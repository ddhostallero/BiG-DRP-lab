from utils.tuple_dataset import TupleIndexDataset
from utils.utils import mkdir, reindex_tuples_v2, moving_average, reset_seed, create_fold_mask
from utils.network_gen import create_mutation_target_network, append_to_mutation_network
from utils.data_initializer import initialize, load_mutation, load_drug_targets

from dgl.dataloading import MultiLayerFullNeighborSampler
import torch
from torch.utils.data import DataLoader, TensorDataset
from muttar_drp.trainer import Trainer
import numpy as np
import pandas as pd

def fold_validation(hyperparams, seed, network, val_network, 
    train_data, val_data, cell_lines, val_cell_lines,
    drug_feats, tuning, epoch, final=False):

    reset_seed(seed)
    train_loader = DataLoader(train_data, batch_size=hyperparams['batch_size'], shuffle=True, drop_last=True)
    val_loader = DataLoader(val_data, batch_size=hyperparams['batch_size'], shuffle=False)

    n_genes = cell_lines.shape[1]
    n_drug_feats = drug_feats.shape[1]

    trainer = Trainer(n_genes, cell_lines, drug_feats, network, hyperparams)


    graph_sampler = MultiLayerFullNeighborSampler(2)
    val_network.ndata['features'] = {
        'drug': drug_feats, 
        'cell_line': torch.cat([cell_lines, val_cell_lines], axis=0),
        'gene': torch.arange(val_network.num_nodes('gene'))}
    _,_, blocks = graph_sampler.sample_blocks(
                val_network, {'drug': range(len(drug_feats)), 
                'cell_line': range(len(cell_lines)+len(val_cell_lines))})

    val_error, metric_names = trainer.fit(
            blocks, 
            num_epoch=epoch, 
            train_loader=train_loader, 
            val_loader=val_loader,
            tuning=tuning)
    if final:
        return val_error, trainer, metric_names
    else:
        return val_error, None, metric_names

def create_dataset(tuples, val_tuples, train_x, val_x, 
    train_y, val_y, drug_feats, mutations, drug_targets,
    c_map, d_map, percentile):

    mutations = mutations.loc[mutations.index.isin(c_map.index)]
    mut_genes = mutations.columns
    mutations['new_index'] = mutations.index
    mutations.index = mutations['new_index'].replace(c_map) # replace with the new indexing
    mutations = mutations.loc[c_map.values, mut_genes]
    
    g_map = pd.Series(range(len(mut_genes)), index=mut_genes)
    mutations.columns = range(len(mut_genes)) # replace with integers for graph

    
    # we only create nodes that do not exist in mutation graph
    not_in_mut = list(set(drug_targets['gene'].unique()) - set(g_map.index)) 
    i = len(g_map)
    for gene in not_in_mut:
        g_map[gene] = i
        i += 1 
    drug_targets['gene'] = drug_targets['gene'].replace(g_map)
    drug_targets['drug'] = drug_targets['drug_name'].replace(d_map)
    network = create_mutation_target_network(tuples, mutations, drug_targets, percentile)

    train_data = TupleIndexDataset( 
        tuples,
        torch.FloatTensor(train_y))

    val_mut = mutations.loc[mutations.index.isin(val_tuples['cell_line'].unique())]
    val_network = append_to_mutation_network(network, val_mut)

    val_data = TupleIndexDataset(
        val_tuples,
        torch.FloatTensor(val_y))

    cell_lines = torch.FloatTensor(train_x)
    drug_feats = torch.FloatTensor(drug_feats.values)
    val_cell_lines = torch.FloatTensor(val_x)

    return network, val_network, train_data, val_data, cell_lines, val_cell_lines, drug_feats

def nested_cross_validation(FLAGS, drug_feats, cell_lines, labels,
    label_matrix, normalizer, mutations, drug_targets):
    reset_seed(FLAGS.seed)
    hyperparams = {
        'learning_rate': 1e-4,
        'num_epoch': 50,
        'batch_size': 128,
        'common_dim': 512,
        'expr_enc': 1024,
        'mid': 512,
        'drop': 1,
        'binary': FLAGS.response_type == 'binary'}

    label_mask = create_fold_mask(labels)
    label_matrix = label_matrix.replace(np.nan, 0)

    final_metrics = None
    drug_list = list(drug_feats.index)
    label_matrix = label_matrix[drug_list]

    for i in range(5):
        print('==%d=='%i)
        test_fold = i 
        val_fold = (i+1)%5
        train_folds = [x for x in range(5) if (x != test_fold) and (x != val_fold)]

        hp = hyperparams.copy()

        # === find number of epochs ===

        train_tuples = labels.loc[labels['fold'].isin(train_folds)]
        train_samples = list(train_tuples['cell_line'].unique())
        train_x = cell_lines.loc[train_samples].values
        train_y = label_matrix.loc[train_samples].values

        val_tuples = labels.loc[labels['fold'] == val_fold]
        val_samples = list(val_tuples['cell_line'].unique())
        val_x = cell_lines.loc[val_samples].values
        val_y = label_matrix.loc[val_samples].values

        train_tuples,c_map,d_map = reindex_tuples_v2(train_tuples, drug_list, train_samples) # all drugs exist in all folds
        
        start_index = train_tuples['cell_line'].max()+1
        val_tuples,vc_map,_ = reindex_tuples_v2(val_tuples, drug_list, val_samples, start_index) # all drugs exist in all folds
        c_map = pd.concat([c_map, vc_map])

        train_x, val_x = normalizer(train_x, val_x)

        train_network, val_network, train_data, val_data, \
        cl_tensor, vcl_tensor, df_tensor = create_dataset(
            train_tuples,
            val_tuples, 
            train_x, val_x, 
            train_y, val_y, drug_feats, 
            mutations.copy(), drug_targets.copy(),
            c_map, d_map, FLAGS.network_perc)

        val_error,_,_ = fold_validation(hp, FLAGS.seed, train_network, val_network, train_data, 
            val_data, cl_tensor, vcl_tensor, df_tensor, tuning=False, 
            epoch=hp['num_epoch'], final=False)

        average_over = 3
        mov_av = moving_average(val_error[:,0], average_over)
        smooth_val_loss = np.pad(mov_av, average_over//2, mode='edge')
        epoch = np.argmin(smooth_val_loss)
        hp['num_epoch'] = int(max(epoch, 2)) 

        # === actual test fold ===

        train_folds = train_folds + [val_fold]
        train_tuples = labels.loc[labels['fold'].isin(train_folds)]
        train_samples = list(train_tuples['cell_line'].unique())
        train_x = cell_lines.loc[train_samples].values
        train_y = label_matrix.loc[train_samples].values

        test_tuples = labels.loc[labels['fold'] == test_fold]
        test_samples = list(test_tuples['cell_line'].unique())
        test_x = cell_lines.loc[test_samples].values
        test_y = label_matrix.loc[test_samples].values

        train_tuples,c_map,d_map = reindex_tuples_v2(train_tuples, drug_list, train_samples) # all drugs exist in all folds
        
        start_index = train_tuples['cell_line'].max()+1
        test_tuples,tc_map,_ = reindex_tuples_v2(test_tuples, drug_list, test_samples, start_index) # all drugs exist in all folds
        c_map = pd.concat([c_map, tc_map])

        train_x, test_x = normalizer(train_x, test_x)

        train_network, test_network, train_data, test_data, \
        cl_tensor, tcl_tensor, df_tensor = create_dataset(
            train_tuples,
            test_tuples, 
            train_x, test_x, 
            train_y, test_y, drug_feats, 
            mutations.copy(), drug_targets.copy(),
            c_map, d_map, FLAGS.network_perc)

        test_error, trainer, metric_names = fold_validation(hp, FLAGS.seed, train_network, test_network, train_data, 
            test_data, cl_tensor, tcl_tensor, df_tensor, tuning=False, 
            epoch=hp['num_epoch'], final=True) # set final so that the trainer uses all epochs

        if i == 0:
            final_metrics = np.zeros((5, test_error.shape[1]))

        final_metrics[i] = test_error[-1]
        test_metrics = pd.DataFrame(test_error, columns=metric_names)
        test_metrics.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/fold_%d.csv'%i, index=False)

        # drug_enc = trainer.get_drug_encoding().cpu().detach().numpy()
        # pd.DataFrame(drug_enc, index=drug_list).to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/encoding_fold_%d.csv'%i)

        trainer.save_model(FLAGS.outroot + "/results/" + FLAGS.folder, i, hp)

        # save predictions
        # test_data = TensorDataset(torch.FloatTensor(test_x))
        # test_data = DataLoader(test_data, batch_size=hyperparams['batch_size'], shuffle=False)

        test_network.ndata['features'] = {
            'drug': df_tensor, 
            'cell_line': torch.cat([cl_tensor, tcl_tensor], axis=0),
            'gene': torch.ones(test_network.num_nodes('gene'),1)}

        prediction_matrix = trainer.predict_matrix(test_network, torch.tensor(tc_map.values, dtype=torch.long))
        prediction_matrix = pd.DataFrame(prediction_matrix, index=test_samples, columns=drug_list)

        from scipy.stats import spearmanr
        scc = np.zeros(len(drug_list))
        preds = []
        truths = []
        for d, drug in enumerate(drug_list):
            pred = prediction_matrix[drug].dropna()
            truth = label_matrix.loc[test_samples,drug].dropna()
            scc[d] = spearmanr(pred, truth)[0]

            preds += list(pred.values)
            truths += list(truth.values)
        print("SCC drug-wise mean: %.4f"%scc.mean())
        print(spearmanr(preds, truths))

        # remove predictions for non-test data
        test_mask = ((label_mask.loc[test_samples]==test_fold)*1)[drug_list]
        test_mask = test_mask.replace(0, np.nan)
        prediction_matrix = prediction_matrix*test_mask

        prediction_matrix.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/val_prediction_fold_%d.csv'%i)
    
    return final_metrics

def main(FLAGS):

    drug_feats, cell_lines, labels, label_matrix, normalizer = initialize(FLAGS)
    mutations = load_mutation(FLAGS)
    mutations = mutations.loc[cell_lines.index]
    drug_targets = load_drug_targets(FLAGS)
    test_metrics = nested_cross_validation(FLAGS, drug_feats, cell_lines, 
        labels, label_matrix, normalizer, mutations, drug_targets)
    test_metrics = test_metrics.mean(axis=0)

    print("Overall Performance")
    print("MSE: %f"%test_metrics[0])
    print("RMSE: %f"%np.sqrt(test_metrics[0]))
    print("Pearson: %f"%test_metrics[1])
    print("Spearman: %f"%test_metrics[2])
