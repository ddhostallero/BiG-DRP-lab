import argparse
from utils.utils import save_flags, mkdir
from utils.flags import MyFlags
import os
os.environ['NUMEXPR_MAX_THREADS']='6'
import numexpr as ne

def main(FLAGS):

    if FLAGS.algo == 'big':
        import bigdrp.main_cv as main_fxn

    if FLAGS.algo == 'big2':
        if FLAGS.mode == 'tosc':
            import bigdrp2.test_on_sc as main_fxn
        else:
            import bigdrp2.main_cv as main_fxn
    elif FLAGS.algo == 'bigrel':
        import bigdrp_rel.main_cv as main_fxn

    elif FLAGS.algo == 'big+':
        import bigdrp_plus.train_extra as main_fxn
    
    elif FLAGS.algo == 'bigmut':
        import bigdrp_mut.main_cv as main_fxn

    elif FLAGS.algo == 'bigrelmut':
        import bigrel_mut.main_cv as main_fxn

    elif FLAGS.algo == 'muttar':
        import muttar_drp.main_cv as main_fxn

    elif FLAGS.algo in ['mlp', 'mlp.vae_baseline', 'mlp.simclr_baseline']:
        if FLAGS.mode == 'train':
            import mlp.main_cv as main_fxn
        elif FLAGS.mode == 'ray':
            import mlp.ray_cv as main_fxn
        elif FLAGS.mode == 'tosc':
            import mlp.test_on_sc as main_fxn

    elif FLAGS.algo == 'scbig':
        if FLAGS.mode == 'totr':
            import scbig.test_on_train as main_fxn
        else:
            import scbig.main_cv as main_fxn


    elif FLAGS.algo in ['ae.b2b', 'vae.b2b', 'ae.sc2sc', 'vae.sc2sc', 
                        'ae.pca.b2b', 'ae.pca.sc2sc']:
        import autoencoder.ae.main_cv as main_fxn
    elif FLAGS.algo in ['ae.mlp', 'vae.mlp', 'cosvae.mlp']:
        import autoencoder.mlp.main_cv as main_fxn

    # elif FLAGS.algo == 'vae_b2sc':
        # import vae_b2sc.main_cv as main_fxn
    # elif FLAGS.algo == 'cosvae':
        # import cosvae.main_cv as main_fxn


    elif FLAGS.algo == 'adda':
        if FLAGS.mode == 'pretrain_mlp':
            import mlp_adda.main_cv as main_fxn
        elif FLAGS.mode == 'adapt_mlp':
            import mlp_adda.main_adda as main_fxn
        elif FLAGS.mode == 'tosc_mlp':
            import mlp_adda.test_on_sc as main_fxn
        elif FLAGS.mode in ['encode_sc', 'encode_bulk']:
            import mlp_adda.encode_expr as main_fxn

    elif FLAGS.algo == 'sym_ae_adda':
        import sym_ae_adda.main_cv as main_fxn

    elif FLAGS.algo == 'big_adda':
        if FLAGS.mode == 'train':
            import big_adda.main_cv as main_fxn
        elif FLAGS.mode == 'adapt':
            import big_adda.main_adda as main_fxn
        elif FLAGS.mode == 'tosc':
            import big_adda.test_on_sc as main_fxn

    elif FLAGS.algo == 'crisp_resp_grl':
        import crisp_resp_grl.main_cv as main_fxn

    elif FLAGS.algo in ['simclr_b2b', 'simclr_sc2sc']:
        import simclr.simclr.main_cv as main_fxn

    elif FLAGS.algo == 'simclr_mlp':
        if FLAGS.mode == 'tosc':
            import simclr.mlp.test_on_sc as main_fxn
        else:
            import simclr.mlp.main_cv as main_fxn

    main_fxn.main(FLAGS)


if __name__ == '__main__':

    print("started main function")
    parser = argparse.ArgumentParser()

    parser.add_argument("--algo", default="big", help="model/algorithm to use")
    parser.add_argument("--mode", default="train", help="[train], test")
    parser.add_argument("--cv", default="double", 
        help="\"single\" reverts model to the epoch with best val loss (val data not used to train)."+
             "\"double\" re-trains the model on all data (except test set) and optimal epoch")

    parser.add_argument("--split", default="lco", help="leave-cells-out [lco], leave-pairs-out (lpo)")
    parser.add_argument("--dataroot", default="../", help="root directory of the data")
    parser.add_argument("--folder", default="chk", help="directory of the output")
    parser.add_argument("--weight_folder", default="", help="directory of the weights")
    parser.add_argument("--emb_folder", default="", help="directory of the embeddings (for loading only)")
    parser.add_argument("--outroot", default="./", help="root directory of the output")
    parser.add_argument("--seed", default=0, help="seed number for pseudo-random generation", type=int)
    parser.add_argument("--drug_feat", default="desc", help="type of drug feature (morgan, [desc], mixed)")
    parser.add_argument("--network_perc", default=1, help="percentile for network generation", type=float)
    parser.add_argument("--test_set", default="schnepp", help="test set of scBiG")
    parser.add_argument("--train_set", default="hideall", help="set to \"seen\" to include common ccls")
    parser.add_argument("--response_type", default="ln_ic50", help="[ln_ic50], auc, binary")
    parser.add_argument("--comment", default="", help="comment about the experiment")
    parser.add_argument("--gene_list", default="", help="name of the gene list")
    parser.add_argument("--sampler", default="full", help="[full], sample10")
    parser.add_argument("--dataset", default="CCLE", help="the dataset, check flags.py for possible values")


    norm_parser = parser.add_mutually_exclusive_group(required=False)
    norm_parser.add_argument('--normalize_response', dest='normalize_response', action='store_true')
    norm_parser.add_argument('--no-normalize_response', dest='normalize_response', action='store_false')
    parser.set_defaults(normalize_response=True)

    args = parser.parse_args() 
    mkdir(args.outroot + "/results/" + args.folder)
    
    FLAGS = MyFlags(args)
    save_flags(FLAGS)
    main(FLAGS)
