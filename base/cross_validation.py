from utils.utils import mkdir, reindex_tuples, reset_seed, create_fold_mask
from utils.tuple_dataset import TupleMapDataset
import numpy as np
import pandas as pd

class NFoldCrossValidation(object):

    def __init__(self, FLAGS):
        super(NFoldCrossValidation, self).__init__()
        self.FLAGS = FLAGS
        

    def _single_cv(self):

    def _double_cv(self):

    def create_dataset(train_tuples, val_tuples, 
        train_x, val_x, 
        train_y, val_y, 
        drug_feats):
    
        train_data = TupleMapDataset(train_tuples, drug_feats, train_x, train_y)
        val_data = TupleMapDataset(val_tuples, drug_feats, val_x, val_y)

        return train_data, val_data

    def fold_validation(hyperparams, seed, train_data, val_data, tuning, epoch, final=False):

    r"""

        Description
        -----------
        Initializes a `Trainer` object then trains using given data and hyperparameters

        Parameters
        ----------
        hyperparams : dict
            Input feature size; i.e, the number of dimensions of :math:`h_j^{(l)}`.
        seed : int
            Output feature size; i.e., the number of dimensions of :math:`h_i^{(l+1)}`.
        train_data : torch.DataSet
            If True, apply a linear layer. Otherwise, aggregating the messages
            without a weight matrix.
        val_data : torch.DataSet
            Contains
        tuning : bool
            Set to true if we are using RayTune
        epoch : int
            Used as a maximum number of training epochs if ``final`` is False. 
            Otherwise, used as the fixed number of  trainingepochs.
        final : bool, optional
            If True, uses the ``epoch`` parameter as a fixed epoch instead of maximum epoch. 
            Otherwise, uses an early stopping criterion. Default: ``False``.
    """

        reset_seed(self.FLAGS.seed)
        train_loader = DataLoader(train_data, batch_size=hyperparams['batch_size'], shuffle=True, drop_last=True)
        val_loader = DataLoader(val_data, batch_size=hyperparams['batch_size'], shuffle=False)

        trainer = Trainer(hyperparams)
        val_error, metric_names = trainer.fit(
                num_epoch=epoch, 
                train_loader=train_loader, 
                val_loader=val_loader,
                tuning=tuning,
                final=final)
        if final:
            return val_error, trainer, metric_names
        else:
            return val_error, None, metric_names


    def nested_cross_validation(self, FLAGS, drug_feats, cell_lines, labels, label_matrix, normalizer):
        reset_seed(FLAGS.seed)
        hyperparams = {
            'drug_in_dim': drug_feats.shape[1],
            'ccl_in_dim': cell_lines.shape[1],
            'learning_rate': 1e-4,
            'num_epoch': 10,
            'batch_size': 128,
            'gene_l1': 1024,
            'drug_l1': 512,
            'mid': 512,
            'drop': 1}

        label_mask = create_fold_mask(labels, label_matrix)
        label_matrix = label_matrix.replace(np.nan, 0)

        final_metrics = None
        drug_list = list(drug_feats.index)

        for i in range(5):
            print('==%d=='%i)
            test_fold = i 
            val_fold = (i+1)%5
            train_folds = [x for x in range(5) if (x != test_fold) and (x != val_fold)]

            hp = hyperparams.copy()

            # === find number of epochs ===

            train_tuples = labels.loc[labels['fold'].isin(train_folds)]
            train_samples = list(train_tuples['cell_line'].unique())
            train_x = cell_lines.loc[train_samples].values
            train_y = label_matrix.loc[train_samples].values
            train_mask = (label_mask.loc[train_samples].isin(train_folds))*1

            val_tuples = labels.loc[labels['fold'] == val_fold]
            val_samples = list(val_tuples['cell_line'].unique())
            val_x = cell_lines.loc[val_samples].values
            val_y = label_matrix.loc[val_samples].values
            val_mask = ((label_mask.loc[val_samples]==val_fold)*1).values

            train_tuples = train_tuples[['drug', 'cell_line', 'response']]
            train_tuples = reindex_tuples(train_tuples, drug_list, train_samples) # all drugs exist in all folds
            val_tuples = val_tuples[['drug', 'cell_line', 'response']]
            val_tuples = reindex_tuples(val_tuples, drug_list, val_samples) # all drugs exist in all folds

            train_x, val_x = normalizer(train_x, val_x)

            train_data, val_data = create_dataset(
                train_tuples, 
                val_tuples,
                train_x, val_x, 
                train_y, val_y, 
                drug_feats)

            val_error,_,_ = fold_validation(hp, seed=FLAGS.seed, 
                train_data=train_data, 
                val_data=val_data, 
                tuning=False, 
                epoch=hp['num_epoch'], final=False)

            epoch = np.argmin(val_error[:,0])
            hp['num_epoch'] = int(max(epoch, 2)) 

            # === actual test fold ===

            train_folds = train_folds + [val_fold]
            train_tuples = labels.loc[labels['fold'].isin(train_folds)]
            train_samples = list(train_tuples['cell_line'].unique())
            train_x = cell_lines.loc[train_samples].values
            train_y = label_matrix.loc[train_samples].values
            train_mask = (label_mask.loc[train_samples].isin(train_folds))*1

            test_tuples = labels.loc[labels['fold'] == test_fold]
            test_samples = list(test_tuples['cell_line'].unique())
            test_x = cell_lines.loc[test_samples].values
            test_y = label_matrix.loc[test_samples].values
            test_mask = (label_mask.loc[test_samples]==test_fold)*1

            train_tuples = train_tuples[['drug', 'cell_line', 'response']]
            train_tuples = reindex_tuples(train_tuples, drug_list, train_samples) # all drugs exist in all folds
            test_tuples = test_tuples[['drug', 'cell_line', 'response']]
            test_tuples = reindex_tuples(test_tuples, drug_list, test_samples) # all drugs exist in all folds


            train_x, test_x = normalizer(train_x, test_x)
            train_data, test_data = create_dataset(
                train_tuples, 
                test_tuples,
                train_x, test_x, 
                train_y, test_y, 
                drug_feats)

            test_error, trainer, metric_names = fold_validation(hp, seed=FLAGS.seed, 
                train_data=train_data, 
                val_data=test_data, 
                tuning=False, 
                epoch=hp['num_epoch'], final=True) # set final so that the trainer uses all epochs

            if i == 0:
                final_metrics = np.zeros((5, test_error.shape[1]))

            final_metrics[i] = test_error[-1]
            test_metrics = pd.DataFrame(test_error, columns=metric_names)
            test_metrics.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/fold_%d.csv'%i, index=False)

            trainer.save_model(FLAGS.outroot + "/results/" + FLAGS.folder, i, hp)

            # save predictions
            test_data = TensorDataset(torch.FloatTensor(test_x))
            # test_data = TupleMapDataset(test_tuples, drug_feats, test_x, test_y)
            test_data = DataLoader(test_data, batch_size=hyperparams['batch_size'], shuffle=False)

            prediction_matrix = trainer.predict_matrix(test_data, torch.Tensor(drug_feats.values))
            prediction_matrix = pd.DataFrame(prediction_matrix, index=test_samples, columns=drug_list)

            # remove predictions for non-test data
            test_mask = test_mask.replace(0, np.nan)
            prediction_matrix = prediction_matrix*test_mask

            prediction_matrix.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/val_prediction_fold_%d.csv'%i)
        
        return final_metrics