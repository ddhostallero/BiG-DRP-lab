import torch
import time
import numpy as np
from scipy.stats import pearsonr, spearmanr
from sklearn.metrics import roc_auc_score
from collections import deque
import json
import os
from copy import deepcopy

class BaseTrainer:
    def __init__(self, hyp):

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.model = None
        self.binary = hyp['binary']

        if hyp['binary']:
            self.loss = torch.nn.BCELoss()
            self.val_loss = torch.nn.BCELoss(reduction='sum')
        else:
            self.loss = torch.nn.MSELoss()
            self.val_loss = torch.nn.MSELoss(reduction='sum')
        self.optimizer = None

    def train_step(self, train_loader, device):
        self.model.train()
        for (expr, d1, y) in train_loader:

            expr, d1, y = expr.to(device), d1.to(device), y.to(device)
            
            self.optimizer.zero_grad()
            pred = self.model(expr, d1)
            loss = self.loss(pred, y)

            loss.backward()
            self.optimizer.step()

        return [loss.item()]

    def metrics_callback(self, y_true, y_hat):
        if self.binary:
            accuracy = np.mean((y_hat > 0.5)*1 == y_true)
            auroc = roc_auc_score(y_true, y_hat)
            return accuracy, auroc

        else:
            pearson = pearsonr(y_true, y_hat)[0]
            spearman = spearmanr(y_true, y_hat)[0]
            return pearson, spearman

    def validation_step(self, val_loader, device):
        self.model.eval()

        val_loss = 0
        preds = []
        ys = []
        with torch.no_grad():
            for (expr, d1, y) in val_loader:
                ys.append(y)
                expr, d1, y = expr.to(device), d1.to(device), y.to(device)
                pred = self.model(expr, d1)
                preds.append(pred)
                val_loss += self.val_loss(pred, y)

        preds = torch.cat(preds, axis=0).cpu().detach().numpy().reshape(-1)
        val_loss /= len(val_loader.dataset)

        ys = torch.cat(ys, axis=0).reshape(-1)
        
        met1, met2 = self.metrics_callback(ys, preds)
        
        return val_loss.item(), met1, met2

    def fit(self, num_epoch, train_loader, val_loader, tuning=False, maxout=False, revert=False):

        if tuning:
            from ray import tune
        if revert:
            best_model_state = None

        start_time = time.time()

        ret_matrix = np.zeros((num_epoch, 4))
        loss_deque = deque([], maxlen=5)

        if self.binary:
            metric_names = ['val BCE', 'val accuracy', 'val auroc', 'train BCE']
        else:
            metric_names = ['val MSE', 'val pearsonr', 'val spearmanr', 'train MSE']

        best_epoch = num_epoch
        best_loss_avg5 = np.inf
        count = 0

        for epoch in range(num_epoch):
            train_metrics = self.train_step(train_loader, self.device)
            val_metrics = self.validation_step(val_loader, self.device)

            ret_matrix[epoch] = list(val_metrics) + list(train_metrics)

            loss_deque.append(val_metrics[0])
            loss_avg5 = sum(loss_deque)/len(loss_deque)

            if best_loss_avg5 > loss_avg5:
                best_loss_avg5 = loss_avg5
                count = 0
                best_epoch = epoch+1
                if revert:
                    best_model_state = deepcopy(self.model.state_dict())
            else:
                count += 1

            if tuning:

                if count == 0: # Checkpoint if the model is as its "best" yet
                    with tune.checkpoint_dir(step=epoch) as checkpoint_dir:
                        path = os.path.join(checkpoint_dir, "checkpoint")
                        torch.save((self.model.state_dict(), self.optimizer.state_dict()), path)

                tune.report(loss=val_metrics[0],  
                            loss_avg5=loss_avg5,
                            spearman=val_metrics[2])
            else:
                elapsed_time = time.time() - start_time
                start_time = time.time()
                print("%d\t%s:%.4f\t%s:%.4f\t%s:%.4f\t%ds"%(
                epoch+1, 
                metric_names[0], val_metrics[0], 
                metric_names[3], train_metrics[0], 
                metric_names[2], val_metrics[2], int(elapsed_time)))

                if ((not maxout) and (count == 10)):
                    ret_matrix = ret_matrix[:epoch+1]
                    if revert:
                        self.model.load_state_dict(best_model_state)
                    break
                    
        return ret_matrix, metric_names, best_epoch

    def save_model(self, directory, fold_id, hyp):
        torch.save(self.model.state_dict(), directory+'/model_weights_fold_%d'%fold_id)

        x = json.dumps(hyp)
        f = open(directory+"/model_config_fold_%d.txt"%fold_id,"w")
        f.write(x)
        f.close()

    def predict(self, data_loader):
        self.model.eval()

        preds = []
        ys = []
        with torch.no_grad():
            for (expr, d1, y) in data_loader:
                ys.append(y)
                expr, d1, y = expr.to(self.device), d1.to(self.device), y.to(self.device)
                pred = self.model(expr, d1)
                preds.append(pred)

        preds = torch.cat(preds, axis=0).cpu().detach().numpy().reshape(-1)
        ys = torch.cat(ys, axis=0).numpy().reshape(-1)
        return preds, ys

    def predict_matrix(self, data_loader, drug_features):
        self.model.eval()
        
        drug_features = drug_features.to(self.device)

        preds = []
        with torch.no_grad():
            for (x,) in data_loader:
                x = x.to(self.device)
                pred = self.model._predict_response_matrix(x, drug_features)
                preds.append(pred)

        preds = torch.cat(preds, axis=0).cpu().detach().numpy()
        return preds