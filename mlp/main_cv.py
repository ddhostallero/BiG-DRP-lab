from utils.tuple_dataset import TupleMapDatasetV2
from utils.utils import mkdir, reindex_tuples, reset_seed, create_fold_mask
from utils.data_initializer import initialize

import torch
from torch.utils.data import DataLoader, TensorDataset

from mlp.trainer import Trainer
from mlp.model import Model as MLP
from mlp.model_vae import BaselineMLP as VAEBaseline
from mlp.simclr_baseline import BaselineMLP as SimCLRBaseline

import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
import pickle

def fold_validation(hyperparams, seed, train_data, val_data, tuning, epoch, maxout, revert):
    reset_seed(seed)
    train_loader = DataLoader(train_data, batch_size=hyperparams['batch_size'], shuffle=True, drop_last=True)
    val_loader = DataLoader(val_data, batch_size=hyperparams['batch_size'], shuffle=False)

    n_genes = train_data.cell_features.shape[1]
    n_drug_feats = train_data.drug_features.shape[1]

    if hyperparams['arki'] == 'mlp':
        model = MLP(n_genes, n_drug_feats, hyperparams)
    elif hyperparams['arki'] == 'vae_baseline':
        model = VAEBaseline(n_genes, n_drug_feats, hyperparams)
    elif hyperparams['arki'] == 'simclr_baseline':
        model = SimCLRBaseline(n_genes, n_drug_feats, hyperparams)

    trainer = Trainer(model, hyperparams)
    val_error, metric_names, best_epoch = trainer.fit(
            num_epoch=epoch, 
            train_loader=train_loader, 
            val_loader=val_loader,
            tuning=tuning,
            maxout=maxout,
            revert=revert)
    
    return val_error, trainer, metric_names, best_epoch

def create_dataset(labels, cell_lines, drug_feats, train_folds, val_fold, normalizer=None):
    drug_list = drug_feats.index
    
    train_tuples = labels.loc[labels['fold'].isin(train_folds)]
    train_samples = list(train_tuples['cell_line'].unique())
    train_x = cell_lines.loc[train_samples].values

    val_tuples = labels.loc[labels['fold'] == val_fold]
    val_samples = list(val_tuples['cell_line'].unique())
    val_x = cell_lines.loc[val_samples].values

    train_tuples = train_tuples[['drug', 'cell_line', 'response']]
    train_tuples = reindex_tuples(train_tuples, drug_list, train_samples) # all drugs exist in all folds
    val_tuples = val_tuples[['drug', 'cell_line', 'response']]
    val_tuples = reindex_tuples(val_tuples, drug_list, val_samples) # all drugs exist in all folds

    if normalizer is None:
        normalizer = StandardScaler()
        train_x = normalizer.fit_transform(train_x)
    else:
        train_x = normalizer.transform(train_x)
    val_x = normalizer.transform(val_x)

    train_data = TupleMapDatasetV2(train_tuples, drug_feats, train_x)
    val_data = TupleMapDatasetV2(val_tuples, drug_feats, val_x)

    return train_data, val_data, train_samples, val_samples, normalizer

def single_cross_validation(FLAGS, drug_feats, cell_lines, labels, hyperparams):
    reset_seed(FLAGS.seed)
    
    label_mask = create_fold_mask(labels)

    final_metrics = None

    for i in range(5):
        print('==%d=='%i)
        test_fold = i 
        val_fold = (i+1)%5
        train_folds = [x for x in range(5) if (x != test_fold) and (x != val_fold)]

        hp = hyperparams.copy()

        # === Train then revert to best validation state ===

        train_data, val_data, _, val_samples, normalizer = create_dataset(
            labels, cell_lines, drug_feats, train_folds, val_fold, normalizer=None)
        val_error, trainer, metric_names, best_epoch = fold_validation(hp, seed=FLAGS.seed, 
            train_data=train_data, 
            val_data=val_data, 
            tuning=False, 
            epoch=hp['num_epoch'], maxout=False, revert=True)

        hp['num_epoch'] = int(max(best_epoch, 2)) 

        # === Test ===

        train_folds = train_folds + [val_fold]
        _, test_data, _, test_samples, _ = create_dataset(
            labels, cell_lines, drug_feats, train_folds, test_fold, normalizer=normalizer)

        if i == 0:
            final_metrics = np.zeros((5, val_error.shape[1]))

        # save logs
        final_metrics[i] = val_error[-1]
        val_metrics = pd.DataFrame(val_error, columns=metric_names)
        val_metrics.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/fold_%d.csv'%i, index=False)

        # save model
        trainer.save_model(FLAGS.outroot + "/results/" + FLAGS.folder, i, hp)
        pickle.dump(normalizer, open(FLAGS.outroot + "/results/" + FLAGS.folder + "/normalizer_fold_%d.pkl"%i, "wb"))


        # validation predictions
        val_data = TensorDataset(val_data.cell_features)
        val_data = DataLoader(val_data, batch_size=hyperparams['batch_size'], shuffle=False)
        prediction_matrix = trainer.predict_matrix(val_data, torch.Tensor(drug_feats.values))
        prediction_matrix = pd.DataFrame(prediction_matrix, index=val_samples, columns=drug_feats.index)

        # remove predictions for non-val data
        val_mask = ((label_mask.loc[val_samples]==test_fold)*1)[drug_feats.index]
        val_mask = val_mask.replace(0, np.nan)
        prediction_matrix = prediction_matrix*val_mask
        prediction_matrix.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/cv_prediction_valfold_%d.csv'%i)

        # test predictions
        test_data = TensorDataset(test_data.cell_features)
        test_data = DataLoader(test_data, batch_size=hyperparams['batch_size'], shuffle=False)
        prediction_matrix = trainer.predict_matrix(test_data, torch.Tensor(drug_feats.values))
        prediction_matrix = pd.DataFrame(prediction_matrix, index=test_samples, columns=drug_feats.index)

        # remove predictions for non-test data
        test_mask = ((label_mask.loc[test_samples]==test_fold)*1)[drug_feats.index]
        test_mask = test_mask.replace(0, np.nan)
        prediction_matrix = prediction_matrix*test_mask

        prediction_matrix.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/cv_prediction_testfold_%d.csv'%i)
    
    final_metrics = pd.DataFrame(final_metrics, columns=metric_names)
    return final_metrics

def nested_cross_validation(FLAGS, drug_feats, cell_lines, labels, hyperparams):
    reset_seed(FLAGS.seed)
    label_mask = create_fold_mask(labels)

    final_metrics = None

    for i in range(5):
        print('==%d=='%i)
        test_fold = i 
        val_fold = (i+1)%5
        train_folds = [x for x in range(5) if (x != test_fold) and (x != val_fold)]

        hp = hyperparams.copy()

        # === find number of epochs ===

        train_data, val_data, _, _, _ = create_dataset(
            labels, cell_lines, drug_feats, train_folds, val_fold, normalizer=None)
        val_error,_,_,best_epoch = fold_validation(hp, seed=FLAGS.seed, 
            train_data=train_data, 
            val_data=val_data, 
            tuning=False, 
            epoch=hp['num_epoch'], maxout=False, revert=False)

        hp['num_epoch'] = int(max(best_epoch, 2)) 

        # === actual test fold ===

        train_folds = train_folds + [val_fold]
        train_data, test_data, _, test_samples, normalizer = create_dataset(
            labels, cell_lines, drug_feats, train_folds, test_fold, normalizer=None)

        test_error, trainer, metric_names, _ = fold_validation(hp, seed=FLAGS.seed, 
            train_data=train_data, 
            val_data=test_data, 
            tuning=False, 
            epoch=hp['num_epoch'], maxout=True, revert=False) # set final so that the trainer uses all epochs

        if i == 0:
            final_metrics = np.zeros((5, test_error.shape[1]))

        # save logs
        final_metrics[i] = test_error[-1]
        test_metrics = pd.DataFrame(test_error, columns=metric_names)
        test_metrics.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/fold_%d.csv'%i, index=False)

        # save model
        trainer.save_model(FLAGS.outroot + "/results/" + FLAGS.folder, i, hp)
        pickle.dump(normalizer, open(FLAGS.outroot + "/results/" + FLAGS.folder + "/normalizer_fold_%d.pkl"%i, "wb"))

        # save predictions
        test_data = TensorDataset(test_data.cell_features)
        test_data = DataLoader(test_data, batch_size=hyperparams['batch_size'], shuffle=False)
        prediction_matrix = trainer.predict_matrix(test_data, torch.Tensor(drug_feats.values))
        prediction_matrix = pd.DataFrame(prediction_matrix, index=test_samples, columns=drug_feats.index)

        # remove predictions for non-test data
        test_mask = ((label_mask.loc[test_samples]==test_fold)*1)[drug_feats.index]
        test_mask = test_mask.replace(0, np.nan)
        prediction_matrix = prediction_matrix*test_mask

        prediction_matrix.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/cv_prediction_testfold_%d.csv'%i)
    
    final_metrics = pd.DataFrame(final_metrics, columns=metric_names)
    return final_metrics

def get_hyp(FLAGS):
    if FLAGS.algo == 'mlp':
        hyperparams = {
            'arki': 'mlp',
            'learning_rate': 1e-4,
            'num_epoch': 50,
            'batch_size': 128,
            'gene_l1': 1024,
            'drug_l1': 512,
            'mid': 512,
            'drop': 1,
            'binary': FLAGS.response_type=='binary'}

    elif FLAGS.algo == 'mlp.vae_baseline':
        hyperparams = {
            'arki': 'vae_baseline',
            'learning_rate': 1e-4,
            'num_epoch': 50,
            'batch_size': 128,
            'gene_l1': 2048,
            'gene_l2': 1024,
            'gene_l3': 512,
            'drug_l1': 512,
            'mid': 512,
            'drop': 1,
            'binary': FLAGS.response_type=='binary'}

    elif FLAGS.algo == 'mlp.simclr_baseline':
        hyperparams = {
            'arki': 'simclr_baseline',
            'learning_rate': 1e-4,
            'num_epoch': 50,
            'batch_size': 128,
            'gene_l1': 4092,
            'gene_l2': 2048,
            'gene_l3': 1024,
            'drug_l1': 512,
            'mid': 512,
            'binary': FLAGS.response_type=='binary'}        
    return hyperparams

def main(FLAGS):
    hyperparams = get_hyp(FLAGS)
    drug_feats, cell_lines, labels, _, _ = initialize(FLAGS)

    if FLAGS.cv == 'double':
        test_metrics = nested_cross_validation(FLAGS, drug_feats, cell_lines, labels, hyperparams)
        print("Overall Performance (on Test)")
    elif FLAGS.cv == 'single':
        test_metrics = single_cross_validation(FLAGS, drug_feats, cell_lines, labels, hyperparams)
        print("Overall Performance (on Validation)")

    test_metrics = test_metrics.mean(axis=0)
    print(test_metrics.mean())
