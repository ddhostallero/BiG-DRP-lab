from utils.utils import reindex_tuples, save_flags, reset_seed, create_fold_mask, calculate_metrics
from utils.tuple_dataset import TupleMapDataset
from utils.data_initializer import initialize

import torch
from torch.utils.data import DataLoader, TensorDataset
from mlp.trainer import Trainer
import numpy as np
import pandas as pd

import os
import ray
from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import ASHAScheduler
from functools import partial

from mlp.model import Model

def fold_validation(hyperparams, seed, train_data, val_data, tuning, epoch, final=False):
    reset_seed(seed)
    train_loader = DataLoader(train_data, batch_size=hyperparams['batch_size'], shuffle=True, drop_last=True)
    val_loader = DataLoader(val_data, batch_size=hyperparams['batch_size'], shuffle=False)

    n_genes = train_data.cell_features.shape[1]
    n_drug_feats = train_data.drug_features.shape[1]

    trainer = Trainer(n_genes, n_drug_feats, hyperparams)
    val_error, metric_names = trainer.fit(
            num_epoch=epoch, 
            train_loader=train_loader, 
            val_loader=val_loader,
            tuning=tuning,
            final=final)
    if final:
        return val_error, trainer, metric_names
    else:
        return val_error, None, metric_names

def create_datasets(train_tuples, val_tuples, train_x, val_x, 
    train_y, val_y, drug_feats):
    
    train_data = TupleMapDataset(train_tuples, drug_feats, train_x, train_y)
    val_data = TupleMapDataset(val_tuples, drug_feats, val_x, val_y)

    return train_data, val_data

def nested_cross_validation(FLAGS, drug_feats, cell_lines, labels, label_matrix, normalizer):

    outfolder = FLAGS.outroot + "/results/" + FLAGS.folder

    hyperparams = {
        "gene_l1": tune.choice([2048, 1024, 512]),
        "drug_l1": tune.choice([512, 256]),
        "mid": tune.choice([512,256]),
        "drop": tune.choice([0,1]),
        "learning_rate": tune.grid_search([5e-5, 1e-4, 2e-4, 5e-4]),
        "batch_size": 128
    }

    final_metrics = np.zeros((5, 4))
    drug_list = list(drug_feats.index)

    label_mask = create_fold_mask(labels, label_matrix)
    label_matrix = label_matrix.replace(np.nan, 0)

    n_genes = cell_lines.shape[1]
    n_drug_feats = drug_feats.shape[1]

    for i in range(5):
        print("=======Fold %d/5======="%i)

        scheduler = ASHAScheduler(
            time_attr="training_iteration",
            metric="loss",
            mode="min",
            max_t=100,
            grace_period=10,
            reduction_factor=3)

        reporter = CLIReporter(
            max_progress_rows=5,
            print_intermediate_tables=False,
            metric="loss",
            mode="min",
            max_report_frequency=1,
            metric_columns=["loss", "spearman", "loss_avg5"])

        test_fold = i 
        val_fold = (i+1)%5
        train_folds = [x for x in range(5) if (x != test_fold) and (x != val_fold)]

        train_tuples = labels.loc[labels['fold'].isin(train_folds)]
        train_samples = list(train_tuples['cell_line'].unique())
        train_x = cell_lines.loc[train_samples].values
        train_y = label_matrix.loc[train_samples].values

        val_tuples = labels.loc[labels['fold'] == val_fold]
        val_samples = list(val_tuples['cell_line'].unique())
        val_x = cell_lines.loc[val_samples].values
        val_y = label_matrix.loc[val_samples].values

        train_tuples = train_tuples[['drug', 'cell_line', 'response']]
        train_tuples = reindex_tuples(train_tuples, drug_list, train_samples) # all drugs exist in all folds
        val_tuples = val_tuples[['drug', 'cell_line', 'response']]
        val_tuples = reindex_tuples(val_tuples, drug_list, val_samples) # all drugs exist in all folds

        train_x, val_x = normalizer(train_x, val_x)

        train_data, val_data = create_datasets(train_tuples, val_tuples, 
            train_x, val_x, train_y, val_y, drug_feats)

        # hyperparameter search
        result = tune.run(
            tune.with_parameters(fold_validation,
                seed=FLAGS.seed,
                train_data=train_data, val_data=val_data,
                tuning=True, epoch=50),
            name="fold_%s"%(i),
            local_dir=outfolder,
            resources_per_trial={"cpu": 2, "gpu": 0.5},
            config=hyperparams,
            num_samples=3, # performs 5 random arkis * gridsearch
            scheduler=scheduler,
            progress_reporter=reporter,
            keep_checkpoints_num=1,
            checkpoint_score_attr='min-loss',
            # verbose=1,
            reuse_actors=True)


        def test_best_model(best_trial):

            best_trained_model = Model(n_genes, n_drug_feats, best_trial.config)
            device = "cuda:0" if torch.cuda.is_available() else "cpu"
            best_trained_model.to(device)

            checkpoint_path = os.path.join(best_trial.checkpoint.value, "checkpoint")

            model_state, optimizer_state = torch.load(checkpoint_path)
            best_trained_model.load_state_dict(model_state)

            test_tuples = labels.loc[labels['fold'] == test_fold]
            test_samples = list(test_tuples['cell_line'].unique())
            test_x = cell_lines.loc[test_samples].values
            test_y = label_matrix.loc[test_samples]

            _, test_x = normalizer(train_x, test_x)

            test_data = TensorDataset(torch.FloatTensor(test_x))
            test_data = DataLoader(test_data, batch_size=128, shuffle=False)

            prediction_matrix = best_trained_model.predict_matrix(test_data, torch.Tensor(drug_feats.values), device)
            prediction_matrix = pd.DataFrame(prediction_matrix, index=test_samples, columns=drug_list)
            
            # remove predictions for non-test data
            test_mask = (label_mask.loc[test_samples]==test_fold)*1
            test_mask = test_mask.replace(0, np.nan)
            prediction_matrix = prediction_matrix*test_mask

            prediction_matrix.to_csv(outfolder + '/val_prediction_fold_%d.csv'%i)

            return calculate_metrics(prediction_matrix, test_y*test_mask), best_trained_model


        best_trial = result.get_best_trial("loss", "min", "all")
        if ray.util.client.ray.is_connected():
            from ray.util.ml_utils.node import force_on_current_node
            remote_fn = force_on_current_node(ray.remote(test_best_model))
            final_metrics[i], model = ray.get(remote_fn.remote(best_trial))
        else:
            final_metrics[i], model = test_best_model(best_trial)

        model.save_model(outfolder, i)
        print(final_metrics[i])
        print(best_trial.config)


        # delete checkpoints (free up memory)
        for trial in result.trials:
            if trial == best_trial:
                continue
            else:
                # ckpt = result.get_trial_checkpoints_paths(trial, 'loss')

                os.remove(os.path.join(trial.checkpoint.value, "checkpoint"))

                last_ckpt = os.path.join(result.get_last_checkpoint(trial), "checkpoint")
                if os.path.exists(last_ckpt):
                    os.remove(last_ckpt)
                else:
                    print(last_ckpt, "does not exist")
        exit()

    return final_metrics

def main(FLAGS):

    # ray.init(address='auto', _node_ip_address=os.environ["ip_head"].split(":")[0], _redis_password=os.environ["redis_password"])

    drug_feats, cell_lines, labels, label_matrix, normalizer = initialize(FLAGS)
    save_flags(FLAGS)
    test_metrics = nested_cross_validation(FLAGS, drug_feats, cell_lines, labels, label_matrix, normalizer)
    test_metrics = test_metrics.mean(axis=0)

    print("MSE: %f"%test_metrics[0])
    print("RMSE: %f"%test_metrics[1])
    print("Pearson: %f"%test_metrics[2])
    print("Spearman: %f"%test_metrics[3])
