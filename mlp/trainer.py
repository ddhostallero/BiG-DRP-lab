import torch
from base.trainer import BaseTrainer

class Trainer(BaseTrainer):
    def __init__(self, model, hyp, test=False, load_model_path=None):
        super(Trainer, self).__init__(hyp)

        self.model = model.to(self.device)
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=hyp['learning_rate'])