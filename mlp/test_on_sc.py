import torch
from torch.utils.data import TensorDataset, DataLoader
import numpy as np
import pandas as pd
import json

from utils.data_initializer import get_bulk_folds, initialize_singlecell, load_drug_features
from utils.utils import load_hyperparams
from mlp.model import Model as MLP
from mlp.model_vae import BaselineMLP as VAEBaseline
from mlp.simclr_baseline import BaselineMLP as SimCLRBaseline

def load_model(n_genes, n_drug_feats, hyperparams, model_path):
    if hyperparams['arki'] == 'mlp':
        model = MLP(n_genes, n_drug_feats, hyperparams)
    elif hyperparams['arki'] == 'vae_baseline':
        model = VAEBaseline(n_genes, n_drug_feats, hyperparams)
    elif hyperparams['arki'] == 'simclr_baseline':
        model = SimCLRBaseline(n_genes, n_drug_feats, hyperparams)

    # model = Model(n_genes, n_drug_feats, hyperparams)
    model.load_state_dict(torch.load(model_path), strict=True)

    return model

def test_on_sc(FLAGS, sc_expr, sc_mapping, normalizer, folds, drug_feats):
    drug_feats_tensor = torch.FloatTensor(drug_feats.values)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    directory = FLAGS.outroot + "results/" + FLAGS.folder

    n_genes = sc_expr.shape[1]
    n_drug_feats = drug_feats.shape[1]

    for i in range(5):
        print('==%d=='%i)
        test_fold = i 
        train_folds = [x for x in range(5) if (x != test_fold)]

        hyperparams = load_hyperparams(directory+'/model_config_fold_%d.txt'%i)
        model_path = directory + '/model_weights_fold_%d'%i
        model = load_model(n_genes, n_drug_feats, hyperparams, model_path)
        model = model.to(device)

        bulk_train_samples = folds.loc[folds['fold'].isin(train_folds)]['cell_line'].unique()
        # we want to exclude the samples that are in the train set

        matched_train_samples = sc_mapping[sc_mapping.isin(bulk_train_samples)].index 
        not_in_train_samples = sc_mapping[~sc_mapping.isin(bulk_train_samples)].index
        
        # normalize using the test samples (i.e. TestNorm)
        # matched_train_samples do not affect the normalization
        # but are normalized too (just in case we want those predicitons)
        test_expr, matched_train_expr = normalizer(
            sc_expr.loc[not_in_train_samples].values, 
            sc_expr.loc[matched_train_samples].values)
        
        data = TensorDataset(torch.FloatTensor(test_expr))
        data = DataLoader(data, batch_size=hyperparams['batch_size'], shuffle=False)
        prediction_matrix = model.predict_matrix(data, drug_feats_tensor, device=device)
        prediction_matrix = pd.DataFrame(prediction_matrix, index=not_in_train_samples, columns=drug_feats.index)
        prediction_matrix.to_csv(directory + '/sc_test_prediction_fold_%d.csv'%i)

        data = TensorDataset(torch.FloatTensor(matched_train_expr))
        data = DataLoader(data, batch_size=hyperparams['batch_size'], shuffle=False)
        prediction_matrix = model.predict_matrix(data, drug_feats_tensor, device=device)
        prediction_matrix = pd.DataFrame(prediction_matrix, index=matched_train_samples, columns=drug_feats.index)
        prediction_matrix.to_csv(directory + '/sc_match_prediction_fold_%d.csv'%i)

def main(FLAGS):
    
    folds = get_bulk_folds(FLAGS, exclude_missing=False)
    sc_expr, sc_mapping, normalizer = initialize_singlecell(FLAGS)
    drug_feats = load_drug_features(FLAGS)

    test_on_sc(FLAGS, sc_expr, sc_mapping, normalizer, folds, drug_feats)