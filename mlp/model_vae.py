import torch
import torch.nn as nn
import torch.nn.functional as F
import json

class BaselineMLP(nn.Module):
    def __init__(self, n_genes, n_drug_feats, hyp):
        super(BaselineMLP, self).__init__()

        self.hyp = hyp
        self.gene_l1 = nn.Linear(n_genes, hyp['gene_l1'])
        self.gene_l2 = nn.Linear(hyp['gene_l1'], hyp['gene_l2'])
        self.gene_l3 = nn.Linear(hyp['gene_l2'], hyp['gene_l3'])

        self.drug_l1 = nn.Linear(n_drug_feats, hyp['drug_l1'])
        self.mid = nn.Linear(hyp['gene_l3']+hyp['drug_l1'], hyp['mid'])

        if hyp['binary']:
            self.out = nn.Sequential(
                nn.Linear(hyp['mid'], 1),
                nn.Sigmoid()
            )
        else:
            self.out = nn.Linear(hyp['mid'], 1)

        if hyp['drop'] == 0:
            drop=[0,0,0]
        else:
            drop=[0.2,0.2,0.5]

        self.expr_drop = nn.Dropout(drop[0])
        self.in_drop = nn.Dropout(drop[1])
        self.mid_drop = nn.Dropout(drop[2])

    def forward(self, expr, drug):
        expr = F.leaky_relu(self.gene_l1(expr))
        expr = self.expr_drop(expr)

        expr = F.leaky_relu(self.gene_l2(expr))
        expr = self.expr_drop(expr)

        expr = F.leaky_relu(self.gene_l3(expr))
        drug = F.leaky_relu(self.drug_l1(drug))
        x = torch.cat([expr, drug], dim=1)

        x = self.in_drop(x)
        x = F.leaky_relu(self.mid(x))
        x = self.mid_drop(x)
        x = self.out(x)
        return x

    def _predict_response_matrix(self, cell_features, drug_features):
        expr = F.leaky_relu(self.gene_l1(cell_features))
        expr = self.expr_drop(expr)

        expr = F.leaky_relu(self.gene_l2(expr))
        expr = self.expr_drop(expr)
        
        expr_enc = F.leaky_relu(self.gene_l3(expr))
        drug_enc = F.leaky_relu(self.drug_l1(drug_features))

        expr_enc = expr_enc.unsqueeze(1) # (batch, 1, expr_enc_size)
        drug_enc = drug_enc.unsqueeze(0) # (1, n_drugs, drug_enc_size)
        
        expr_enc = expr_enc.repeat(1,drug_enc.shape[1],1) # (batch, n_drugs, expr_enc_size)
        drug_enc = drug_enc.repeat(expr_enc.shape[0],1,1) # (batch, n_drugs, drug_enc_size)
        
        x = torch.cat([expr_enc,drug_enc],-1) # (batch, n_drugs, expr_enc_size+drugs_enc_size)
        x = self.in_drop(x)
        x = F.leaky_relu(self.mid(x)) # (batch, n_drugs, 1)
        x = self.mid_drop(x)
        out = self.out(x) # (batch, n_drugs, 1)
        out = out.view(-1, drug_enc.shape[1]) # (batch, n_drugs)
        return out

    def predict_matrix(self, data_loader, drug_features, device='cuda'):
        self.eval()
        
        drug_features = drug_features.to(device)

        preds = []
        with torch.no_grad():
            for (x,) in data_loader:
                x = x.to(device)
                pred = self._predict_response_matrix(x, drug_features)
                preds.append(pred)

        preds = torch.cat(preds, axis=0).cpu().detach().numpy()
        return preds

    def save_model(self, directory, fold_id):
        torch.save(self.state_dict(), directory+'/model_weights_fold_%d'%fold_id)

        x = json.dumps(self.hyp)
        f = open(directory+"/model_config_fold_%d.txt"%fold_id,"w")
        f.write(x)
        f.close()