import torch
# from bigdrp2.model import BiGDRP
import time
import numpy as np
from scipy.stats import pearsonr, spearmanr
from dgl.dataloading import MultiLayerFullNeighborSampler
import torch.nn.functional as F
from bigdrp2.trainer import Trainer as BaseTrainer

class Trainer(BaseTrainer):

    def fit(self, folder, num_epoch, train_loader, val_loader, tuning=False):
        start_time = time.time()

        ret_matrix = np.zeros((num_epoch, 4))

        if self.binary:
            metric_names = ['val BCE', 'val accuracy', 'val auroc', 'train BCE']
        else:
            metric_names = ['val MSE', 'val pearsonr', 'val spearmanr', 'train MSE']

        best_loss = np.inf
        count = 0

        for epoch in range(num_epoch):
            train_metrics = self.train_step(train_loader, self.device)
            val_metrics = self.validation_step(val_loader, self.device)
            loss = val_metrics[0]

            ret_matrix[epoch] = list(val_metrics) + list(train_metrics)

            elapsed_time = time.time() - start_time
            start_time = time.time()
            print("%d\t%s:%.4f\t%s:%.4f\t%s:%.4f\t%ds"%(
                epoch+1, 
                metric_names[0], val_metrics[0], 
                metric_names[3], train_metrics[0], 
                metric_names[2], val_metrics[2], int(elapsed_time)))

            if best_loss > loss:
                best_loss = loss
                count = 0
            else:
                count += 1

            if tuning:

                if count == 0: # Checkpoint if the model is as its "best" yet
                    torch.save(self.model.state_dict(), folder+'/tmp_best_weights')

                elif count == 10: # use this if we are not using raytune
                    ret_matrix = ret_matrix[:epoch+1]
                    self.model.load_state_dict(torch.load(folder+'/tmp_best_weights'), strict=True)
                    self.model = self.model.to(self.device)
                    break

        return ret_matrix, metric_names


    def train_extra_step(self, train_loader):
        self.model.train()
        print("training BiG-DRP+")
        for (x, d, y) in train_loader:
            x, d, y = x.to(self.device), d.to(self.device), y.to(self.device)
            
            self.optimizer.zero_grad()
            pred = self.model.predict_from_drug_encoding(x, d)
            loss = self.loss(pred, y)
            loss.backward()
            self.optimizer.step()
            
            # print("+ train loss: %.4f"%loss.item())

        return [loss.item()]