from utils.tuple_dataset import TupleMatrixDataset, TupleMapDataset
from utils.utils import mkdir, reindex_tuples, moving_average, reset_seed, create_fold_mask
from utils.network_gen import create_network
from utils.data_initializer import initialize

import torch
from torch.utils.data import DataLoader, TensorDataset
from scbig.trainer import Trainer
from bigdrp2.model import DRPPlus
import numpy as np
import pandas as pd

def fold_validation(hyperparams, seed, network, train_data, val_data, cell_lines, 
    drug_feats, folder, tuning, epoch, final=False):

    reset_seed(seed)
    train_loader = DataLoader(train_data, batch_size=hyperparams['batch_size'], shuffle=True, drop_last=True)
    val_loader = DataLoader(val_data, batch_size=hyperparams['batch_size'], shuffle=False)

    n_genes = cell_lines.shape[1]
    n_drug_feats = drug_feats.shape[1]

    trainer = Trainer(n_genes, cell_lines, drug_feats, network, hyperparams)
    val_error, metric_names = trainer.fit(
            folder=folder,
            num_epoch=epoch, 
            train_loader=train_loader, 
            val_loader=val_loader,
            tuning=tuning)

    return val_error, trainer, metric_names

def create_dataset(tuples, train_x, val_x, 
    train_y, val_y, train_mask, val_mask, drug_feats, percentile):

    network = create_network(tuples, percentile)

    train_data = TupleMatrixDataset( 
        tuples,
        torch.FloatTensor(train_x),
        torch.FloatTensor(train_y))

    val_data = TensorDataset(
        torch.FloatTensor(val_x),
        torch.FloatTensor(val_y),
        torch.FloatTensor(val_mask))

    cell_lines = torch.FloatTensor(train_x)
    drug_feats = torch.FloatTensor(drug_feats.values)

    return network, train_data, val_data, cell_lines, drug_feats

def cross_validation(FLAGS, drug_feats, cell_lines, 
    labels, label_matrix, normalizer, test_expr):
    reset_seed(FLAGS.seed)
    directory = FLAGS.outroot + "/results/" + FLAGS.folder
    hyperparams = {
        'learning_rate': 1e-4,
        'num_epoch': 50,
        'batch_size': 128,
        'common_dim': 512,
        'expr_enc': 1024,
        'conv1': 512,
        'conv2': 512,
        'mid': 512,
        'drop': 1,
        'binary': FLAGS.response_type == 'binary'}

    label_mask = create_fold_mask(labels, label_matrix)
    label_matrix = label_matrix.replace(np.nan, 0)

    final_metrics = None
    drug_list = list(drug_feats.index)

    for i in range(5):
        print('==%d=='%i)
        val_fold = i 
        train_folds = [x for x in range(5) if (x != val_fold)]

        if FLAGS.train_set == 'seen':
            print("training set includes bulk of ccls from test set")
            if labels['fold'].min() == -1:
                train_folds.append(-1)
            else:
                train_folds += [5,6,7]
        elif FLAGS.train_set == 'hide5':
            train_folds += [6,7]
        elif FLAGS.train_set == 'hide6':
            train_folds += [5,7]
        elif FLAGS.train_set == 'hide7':
            train_folds += [5,6]
        # default: hide all

        hp = hyperparams.copy()

        # === find number of epochs ===
        train_tuples = labels.loc[labels['fold'].isin(train_folds)]
        train_samples = list(train_tuples['cell_line'].unique())
        train_x = cell_lines.loc[train_samples].values
        train_y = label_matrix.loc[train_samples].values
        train_mask = (label_mask.loc[train_samples].isin(train_folds))*1

        val_tuples = labels.loc[labels['fold'] == val_fold]
        val_samples = list(val_tuples['cell_line'].unique())
        val_x = cell_lines.loc[val_samples].values
        val_y = label_matrix.loc[val_samples].values
        val_mask = ((label_mask.loc[val_samples]==val_fold)*1).values

        train_tuples = reindex_tuples(train_tuples, drug_list, train_samples) # all drugs exist in all folds
        train_x, val_x = normalizer(train_x, val_x)

        network, train_data, val_data, \
        cl_tensor, df_tensor = create_dataset(
            train_tuples, 
            train_x, val_x, 
            train_y, val_y, 
            train_mask, val_mask, drug_feats, FLAGS.network_perc)

        val_error,trainer,metric_names = fold_validation(hp, FLAGS.seed, network, train_data, 
            val_data, cl_tensor, df_tensor, 
            folder=directory, tuning=True, 
            epoch=hp['num_epoch'], final=True)

        if i == 0:
            final_metrics = np.zeros((5, val_error.shape[1]))

        epoch = np.argmin(val_error[:,0])
        hp['num_epoch'] = int(epoch) 
        final_metrics[i] = val_error[int(epoch)]
        val_metrics = pd.DataFrame(val_error, columns=metric_names)
        val_metrics.to_csv(directory + '/fold_%d.csv'%i, index=False)

        drug_enc = trainer.get_drug_encoding()
        pd.DataFrame(drug_enc.cpu().detach().numpy(), 
            index=drug_list).to_csv(
            directory + '/encoding_fold_%d.csv'%i)

        trainer.save_model(directory+"/big/", i, hp)

        # [BiG] validation predictions
        val_data = TensorDataset(torch.FloatTensor(val_x))
        val_data = DataLoader(val_data, batch_size=hyperparams['batch_size'], shuffle=False)

        prediction_matrix = trainer.predict_matrix(val_data, drug_encoding=drug_enc)
        prediction_matrix = pd.DataFrame(prediction_matrix, index=val_samples, columns=drug_list)
        prediction_matrix.to_csv(directory + '/big/val_prediction_fold_%d.csv'%i)

        # [BiG] test predictions
        test_data = TensorDataset(torch.FloatTensor(test_expr.values))
        test_data = DataLoader(test_data, batch_size=hyperparams['batch_size'], shuffle=False)

        prediction_matrix = trainer.predict_matrix(test_data, drug_encoding=drug_enc)
        prediction_matrix = pd.DataFrame(prediction_matrix, index=test_expr.index, columns=drug_list)
        prediction_matrix.to_csv(directory + '/big/test_prediction_fold_%d.csv'%i)
    
        # BigDRP+
        train_data = TupleMapDataset( 
            train_tuples,
            drug_enc.cpu(),
            torch.FloatTensor(train_x),
            torch.FloatTensor(train_y))
        train_data = DataLoader(train_data, batch_size=hp['batch_size'])
        trainer.train_extra_step(train_data)

        # [BiG+] validation predictions
        prediction_matrix = trainer.predict_matrix(val_data, drug_encoding=drug_enc)
        prediction_matrix = pd.DataFrame(prediction_matrix, index=val_samples, columns=drug_list)
        prediction_matrix.to_csv(directory + '/big/val_prediction_fold_%d.csv'%i)

        # [BiG+] test predictions
        prediction_matrix = trainer.predict_matrix(test_data, drug_encoding=drug_enc)
        prediction_matrix = pd.DataFrame(prediction_matrix, index=test_expr.index, columns=drug_list)
        prediction_matrix.to_csv(directory + '/bigplus/test_prediction_fold_%d.csv'%i)
        
        bigplusmodel = DRPPlus(train_x.shape[1], hp)
        bigplusmodel.load_state_dict(trainer.model.state_dict(), strict=False)
        torch.save(bigplusmodel.state_dict(), directory+"/bigplus/weights_fold_%d"%i)

    return final_metrics


def log2fpkm_to_log2tpm(x):
    x = x.T
    x = np.power(2, x) - 1
    x = ((1e6*x)/x.sum()).T
    tpm = np.log2(x + 1)
    return tpm

def main(FLAGS):

    drug_feats, cell_lines, labels, label_matrix, normalizer = initialize(FLAGS)
    mkdir(FLAGS.outroot + "/results/" + FLAGS.folder + "/big")
    mkdir(FLAGS.outroot + "/results/" + FLAGS.folder + "/bigplus")

    if FLAGS.test_set == 'schnepp':
        gene_list = '../sc-data/scDEAL/scBiG/schnepp/genes.txt'
        test_expr = '../sc-data/scDEAL/scBiG/schnepp/expr.csv'
        folds_list = '../sc-data/scDEAL/scBiG/schnepp/folds.csv'
    elif FLAGS.test_set == 'pc9':
        gene_list = '../sc-data/scDEAL/scBiG/pc9/genes.txt'
        test_expr = '../sc-data/scDEAL/scBiG/pc9/expr.csv'
    elif FLAGS.test_set == 'sharma':
        gene_list = '../sc-data/scDEAL/scBiG/sharma/genes.txt'
        test_expr = '../sc-data/scDEAL/scBiG/sharma/expr.csv'
        cell_lines = log2fpkm_to_log2tpm(cell_lines)
    elif FLAGS.test_set == 'gambardella':
        gene_list = '../sc-data/scDEAL/scBiG/gambardella/genes.txt'
        test_expr = '../sc-data/scDEAL/scBiG/gambardella/expr.csv'
        folds_list = '../sc-data/scDEAL/scBiG/gambardella/folds2.csv'
    elif FLAGS.test_set == 'tcga_hnsc':
        gene_list = '../sc-data/HNSC/genes.txt'
        #gene_list = '../sc-data/HNSC/L1000_genes.txt'
        test_expr = '../sc-data/HNSC/TCGA_HNSC_log2FPKM.csv'
    elif FLAGS.test_set == 'kinker':
        gene_list = '../sc-data/scDEAL/scBiG/kinker/genes.txt'
        test_expr = '../sc-data/pancan/filtered/ccle_kinker_log2tpm.csv'
        folds_list = '../sc-data/scDEAL/scBiG/kinker/folds.csv'

    if FLAGS.test_set in ['schnepp', 'gambardella', 'kinker']:
        # replace the folds with the ones that exclude common cell lines
        folds = pd.read_csv(folds_list, index_col=0, squeeze=True)
        labels['fold'] = labels['cell_line'].replace(folds)

    # use only common genes
    genes = [line.strip() for line in open(gene_list)]
    cell_lines = cell_lines[genes]

    # load test data
    if FLAGS.test_set == 'kinker':
        from tqdm import tqdm
        chunksize = 100
        data = []
        for chunk in tqdm(pd.read_csv(test_expr, chunksize=chunksize, index_col=0)):
            data.append(chunk[genes])
        test_expr = pd.concat(data)
    else:
        test_expr = pd.read_csv(test_expr, index_col=0)
        test_expr = test_expr[genes].copy()

    test_expr = (test_expr - test_expr.mean())/test_expr.std()
    test_expr = test_expr.replace(np.nan, 0) # if std is 0, set to mean (0)

    test_metrics = cross_validation(
        FLAGS, drug_feats, cell_lines, 
        labels, label_matrix, normalizer,
        test_expr)
    test_metrics = test_metrics.mean(axis=0)

    print("Overall Performance")
    print("MSE: %f"%test_metrics[0])
    print("RMSE: %f"%np.sqrt(test_metrics[0]))
    print("Pearson: %f"%test_metrics[1])
    print("Spearman: %f"%test_metrics[2])
