from utils.data_initializer import initialize
from utils.utils import mkdir, reindex_tuples, create_fold_mask, reset_seed
from utils.tuple_dataset import TupleMapDataset
import torch
from torch.utils.data import DataLoader, TensorDataset
from bigdrp2.model import DRPPlus
import numpy as np
import pandas as pd

def predict_matrix(model, data_loader, drug_encoding, device):
    """
    returns a prediction matrix of (N, n_drugs)
    """

    model.eval()

    preds = []
    drug_encoding = drug_encoding

    with torch.no_grad():
        for (x,) in data_loader:
            x = x.to(device)
            pred = model.predict_response_matrix(x, drug_encoding)
            preds.append(pred)

    preds = torch.cat(preds, axis=0).cpu().detach().numpy()
    return preds


def test_on_train(FLAGS, cell_lines, labels, label_matrix, normalizer):

    hyperparams = {
        'learning_rate': 1e-4,
        'num_epoch': 50,
        'batch_size': 128,
        'common_dim': 512,
        'expr_enc': 1024,
        'conv1': 512,
        'conv2': 512,
        'mid': 512,
        'drop': 1,
        'binary': FLAGS.response_type == 'binary'}
    
    label_mask = create_fold_mask(labels, label_matrix)
    label_matrix = label_matrix.replace(np.nan, 0)

    n_genes = cell_lines.shape[1]
    metrics = np.zeros((5, 4))

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    for i in range(5):
        reset_seed(FLAGS.seed)
        model_path = FLAGS.outroot + "results/" + FLAGS.folder + '/weights_fold_%d'%i
        encoding_path = FLAGS.outroot + "results/" + FLAGS.weight_folder + '/encoding_fold_%d.csv'%i

        print("Loading weights from: %s..."%model_path)
        model = DRPPlus(n_genes, hyperparams)
        model.load_state_dict(torch.load(model_path), strict=True)
        model = model.to(device)

        print("Loading encoding from: %s..."%encoding_path)
        drug_encoding = pd.read_csv(encoding_path, index_col=0)
        dr_idx = drug_encoding.index

        val_fold = i 
        train_folds = [x for x in range(5) if (x != val_fold)]

        # also predict for the bulk of the hidden CCLs
        if FLAGS.train_set == 'seen':
            print("training set includes bulk of ccls from test set")
            if labels['fold'].min() == -1:
                train_folds.append(-1)
            else:
                train_folds += [5,6,7]
            val_folds = [val_fold]
        elif FLAGS.train_set == 'hide5':
            train_folds += [6,7]
            val_folds = [val_fold, 5]
        elif FLAGS.train_set == 'hide6':
            train_folds += [5,7]
            val_folds = [val_fold, 6]
        elif FLAGS.train_set == 'hide7':
            train_folds += [5,6]
            val_folds = [val_fold, 7]
        elif FLAGS.train_set == 'hideall':
            val_folds = [val_fold, 5, 6, 7]

        train_tuples = labels.loc[labels['fold'].isin(train_folds)]
        train_samples = list(train_tuples['cell_line'].unique())
        train_x = cell_lines.loc[train_samples].values
        train_mask = (label_mask.loc[train_samples].isin(train_folds))*1

        val_tuples = labels.loc[labels['fold'].isin(val_folds)]
        val_samples = list(val_tuples['cell_line'].unique())
        val_x = cell_lines.loc[val_samples].values
        val_mask = ((label_mask.loc[val_samples].isin(val_folds))*1)

        # train_tuples = reindex_tuples(train_tuples, drug_list, train_samples) # all drugs exist in all folds
        train_x, val_x = normalizer(train_x, val_x)

        # === test on train set ===
        drug_encoding = torch.Tensor(drug_encoding.values).to(device)
        train_data = TensorDataset(torch.FloatTensor(train_x))
        train_data = DataLoader(train_data, batch_size=hyperparams['batch_size'], shuffle=False)
        prediction_matrix = predict_matrix(model, train_data, drug_encoding, device)
        prediction_matrix = pd.DataFrame(prediction_matrix, index=train_samples, columns=dr_idx)
        train_mask = train_mask.replace(0, np.nan)
        prediction_matrix = prediction_matrix*train_mask
        prediction_matrix.to_csv(FLAGS.outroot + "results/" + FLAGS.folder + '/train_prediction_fold_%d.csv'%i)

        # == test on val set == 
        val_data = TensorDataset(torch.FloatTensor(val_x))
        val_data = DataLoader(val_data, batch_size=hyperparams['batch_size'], shuffle=False)
        prediction_matrix = predict_matrix(model, val_data, drug_encoding, device)
        prediction_matrix = pd.DataFrame(prediction_matrix, index=val_samples, columns=dr_idx)
        val_mask = val_mask.replace(0, np.nan)
        prediction_matrix = prediction_matrix*val_mask
        prediction_matrix.to_csv(FLAGS.outroot + "results/" + FLAGS.folder + '/val_prediction_fold_%d.csv'%i)


def main(FLAGS):
    drug_feats, cell_lines, labels, label_matrix, normalizer = initialize(FLAGS)

    if FLAGS.test_set == 'schnepp':
        gene_list = '../sc-data/scDEAL/scBiG/schnepp/genes.txt'
        folds_list = '../sc-data/scDEAL/scBiG/schnepp/folds.csv'
    elif FLAGS.test_set == 'pc9':
        gene_list = '../sc-data/scDEAL/scBiG/pc9/genes.txt'
    elif FLAGS.test_set == 'sharma':
        gene_list = '../sc-data/scDEAL/scBiG/sharma/genes.txt'
        cell_lines = log2fpkm_to_log2tpm(cell_lines)
    elif FLAGS.test_set == 'gambardella':
        gene_list = '../sc-data/scDEAL/scBiG/gambardella/genes.txt'
        folds_list = '../sc-data/scDEAL/scBiG/gambardella/folds2.csv'
    elif FLAGS.test_set == 'tcga_hnsc':
        gene_list = '../sc-data/HNSC/genes.txt'
    elif FLAGS.test_set == 'kinker':
        gene_list = '../sc-data/scDEAL/scBiG/kinker/genes.txt'
        folds_list = '../sc-data/scDEAL/scBiG/kinker/folds.csv'


    if FLAGS.test_set in ['schnepp', 'gambardella', 'kinker']:
        # replace the folds with the ones that exclude common cell lines
        folds = pd.read_csv(folds_list, index_col=0, squeeze=True)
        labels['fold'] = labels['cell_line'].replace(folds)

    # use only common genes
    genes = [line.strip() for line in open(gene_list)]
    cell_lines = cell_lines[genes]


    test_on_train(FLAGS, cell_lines, labels, label_matrix, normalizer)