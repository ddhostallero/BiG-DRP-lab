import pandas as pd
import numpy as np
from scipy.stats import spearmanr
import argparse

def load_predictions(file_prefix):
    preds = []
    for i in range(5):
        pred = pd.read_csv('%s_%d.csv'%(file_prefix, i), index_col=0)
        preds.append(pred)
    return preds

def propagate_labels(ctrp, sc_meta):
    
    sc2bulk = sc_meta['stripped_cell_line_name']
    prop_lab = np.empty(((sc_meta.shape[0], ctrp.shape[1])))
    
    for i, idx in enumerate(sc2bulk.index):
        prop_lab[i] = ctrp.loc[sc2bulk[idx]]        
    prop_lab = pd.DataFrame(prop_lab, index=sc2bulk.index, columns=ctrp.columns)
    
    return prop_lab

def agg_prediction(df, meta, agg='mean'):
    meta = meta.loc[meta.index.isin(df.index)]
    
    ret_val = pd.DataFrame(columns=df.columns)
    for i, x in meta.groupby('stripped_cell_line_name'):
        if agg == 'median':
            ret_val.loc[i] = df.loc[x.index].median()
        elif agg == 'mean':
            ret_val.loc[i] = df.loc[x.index].mean()
        else:
            print('uknown agg parameter')
            return None
    return ret_val

def get_perf(resp, pred):
    drugs = resp.columns
    ccls = pred.index
    perf = pd.DataFrame(index=drugs, columns=['n', 'scc', 'rmse'])
    
    for drug in drugs:
        y_true = resp.loc[ccls, drug].dropna()
        y_hat = pred.loc[y_true.index, drug]
        
        perf.loc[drug] = [len(y_true),
                          spearmanr(y_hat, y_true)[0],
                          np.sqrt(((y_hat-y_true)**2).mean())]
    return perf

def get_perf_all(dr_bulk, dr_prop, sc_pred, sc_meta):
    mean_prediction = agg_prediction(sc_pred, sc_meta, agg='mean')
    median_prediction = agg_prediction(sc_pred, sc_meta, agg='median')

    dr_bulk = dr_bulk.loc[mean_prediction.index]
    mean_perf = get_perf(dr_bulk, mean_prediction)
    median_perf = get_perf(dr_bulk, median_prediction)
    
    dr_prop = dr_prop.loc[sc_pred.index]
    prop_perf = get_perf(dr_prop, sc_pred)
    
    return mean_perf, median_perf, prop_perf

def summarize(perf):
    return [perf.T.xs("scc", level="metric").mean(axis=0).mean(),
            perf.T.xs("scc", level="metric").mean(axis=0).std(),
            perf.T.xs("rmse", level="metric").mean(axis=0).mean(),
            perf.T.xs("rmse", level="metric").mean(axis=0).std()]

normalize_response = True
parser = argparse.ArgumentParser()
parser.add_argument('-f', '--folder', default='results/')
parser.add_argument('-p', '--prefix', default='sc_test_prediction_fold')
parser.add_argument('-o', '--outprefix', default='drugwise_test')
parser.add_argument('-d', '--drugset', default='lists/ctrp_drug_list.txt', help='set of drugs')
args = parser.parse_args() 

# load metadata and labels
drugs = open(args.drugset).read().split('\n')
if drugs[-1] == '': drugs=drugs[:-1]

ccls_with_bulk = pd.read_csv('../drp-data/grl-preprocessed/ctrp_ccle/ccle_tpm.csv',index_col=0).columns
tup = pd.read_csv('../drp-data/grl-preprocessed/drug_response/ctrp_tuple_labels_folds.csv', index_col=0)

ctrp = tup.pivot(index='cell_line', columns='drug', values='auc')
if normalize_response:
    filt = tup.loc[tup['cell_line'].isin(ccls_with_bulk)] # these are the only ones we used for normalization during training
    filt = filt.pivot(index='cell_line', columns='drug', values='auc')
    ctrp = (ctrp - filt.mean())/filt.std()
ctrp = ctrp[drugs]

meta = pd.read_csv('../sc-data/scDEAL/scBiG/kinker/meta.csv', index_col=0)

# load predictions
pred_dir = args.folder + args.prefix
predictions = load_predictions(pred_dir)
all_sc_ccls = set()
for i in range(5):
    all_sc_ccls = all_sc_ccls.union(set(predictions[i].index))

# filter
meta = meta.loc[(meta.index.isin(all_sc_ccls)) & (meta['stripped_cell_line_name'].isin(ctrp.index))]
ctrp = ctrp.loc[ctrp.index.isin(meta['stripped_cell_line_name'])]

for i in range(5):
    predictions[i] = predictions[i].loc[predictions[i].index.isin(meta.index)]

# propagate
dr_prop = propagate_labels(ctrp, meta)

# calculate perfomance metrics
idx = pd.MultiIndex.from_product([range(5), ["n", "scc", "rmse"]], names=["fold", "metric"])
mean_perfs = pd.DataFrame(index=drugs, columns=idx)
median_perfs = pd.DataFrame(index=drugs, columns=idx)
prop_perfs = pd.DataFrame(index=drugs, columns=idx)
for i in range(5):
    mean_perf, median_perf, prop_perf = get_perf_all(ctrp, dr_prop, predictions[i], meta)
    mean_perfs[i] = mean_perf
    median_perfs[i] = median_perf
    prop_perfs[i] = prop_perf

# create summary
idx = pd.MultiIndex.from_product([["scc", "rmse"],["mean", "stdev"]], names=["metric", "stat"])
summary = pd.DataFrame(index=['MeanPred', 'MedianPred', 'Propagate'], columns=idx)
summary.loc['MeanPred'] = summarize(mean_perfs)
summary.loc['MedianPred'] = summarize(median_perfs)
summary.loc['Propagate'] = summarize(prop_perfs)


print(summary)
#save
exwrite = pd.ExcelWriter('%s/%s_performance.xlsx'%(args.folder, args.outprefix))
summary.to_excel(exwrite, sheet_name='summary')
mean_perfs.to_excel(exwrite, sheet_name='MeanPred')
median_perfs.to_excel(exwrite, sheet_name='MedianPred')
prop_perfs.to_excel(exwrite, sheet_name='Propagate')
exwrite.save()