import pandas as pd 
import numpy as np
from sklearn.metrics import roc_auc_score, precision_score, recall_score
from metrics import load_predictions
from scipy.stats import wilcoxon
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file', default='lists/LCO_bin.txt', help='file containing the list of folders to compare')
parser.add_argument('-s', '--split', default='lco', help='[lco], lpo')
parser.add_argument('-o', '--outfolder', default='results/', help='output folder')
parser.add_argument('-d', '--drugset', default='lists/drug_list.txt', help='set of drugs')
parser.add_argument('-p', '--positive', default='sensitive', help='[sensitive], resistant]')
args = parser.parse_args() 


def accuracy(y_true, y_pred):
	return np.mean((y_pred > 0.5)*1 == y_true)


folders = pd.read_csv(args.file, index_col=0, header=None, comment='#')
folders.columns=['method_name']
to_compare = list(folders.index)
print(folders)


### load labels ###

print('loading labels...')

y_tup = pd.read_csv('../drp-data/grl-preprocessed/drug_response/gdsc_tuple_labels_folds.csv', index_col=0)
# y_tup = pd.read_csv('../grl-drp2/mutation/gdsc/gdsc_tuple_labels_folds_with_mut.csv', index_col=0)
if args.split == 'lpo':
	y_tup['fold'] = y_tup['pair_fold']
elif args.split == 'ldo':
	y_tup['fold'] = y_tup['drug_fold']
else:
	y_tup['fold'] = y_tup['cl_fold']

y_tup['response']  = y_tup[args.positive]
y_tup = y_tup.loc[y_tup['fold']>=0]
y_bin = y_tup.pivot(index='cell_line', columns='drug', values='response')

samples = list(y_tup['cell_line'].unique())
drugs = [line.strip() for line in open(args.drugset)]

print("calculating for %d drugs and %d cell lines..."%(len(drugs), len(samples)))

y_tup = y_tup.loc[y_tup['drug'].isin(drugs)]
y_bin = y_bin.loc[samples, drugs]

def create_fold_mask(tuples):
    x = tuples.pivot(index='cell_line', columns='drug', values='fold')
    return x

fold_mask = create_fold_mask(y_tup)
fold_mask = fold_mask.loc[samples, drugs]


### load predictions ###

preds = {}
for i, method in enumerate(to_compare):
	_, df = load_predictions(method, unnormalize=False, split=args.split, fold_mask=fold_mask) # do not normalize here
	preds[method] = df.loc[samples, drugs]

null_mask = (y_bin.isna() - 1).values.nonzero()

print('calculating overall metrics...')

cols = ["method", "auroc (collated)", "auroc (mean)", "auroc (stdev)"]
cols += ["auroc (%d)"%i for i in range(5)]
cols += ["accuracy (collated)", "accuracy (mean)", "accuracy (stdev)"]
cols += ["accuracy (%d)"%i for i in range(5)]


overall = pd.DataFrame(index=to_compare, columns=cols)

for method in to_compare:
	
	row = {
		'method': folders.loc[method, 'method_name'],
		'auroc (collated)': roc_auc_score(y_bin.values[null_mask], preds[method].values[null_mask]),
		'accuracy (collated)': accuracy(y_bin.values[null_mask], preds[method].values[null_mask])
	}

	s = np.zeros(5)
	a = np.zeros(5)
	for i in range(5):
		m  = ((fold_mask == i)*1).values.nonzero()
		s[i] = accuracy(y_bin.values[m], preds[method].values[m])
		row['accuracy (%d)'%i] = s[i]
		a[i] = roc_auc_score(y_bin.values[m], preds[method].values[m])
		row['auroc (%d)'%i] = a[i]

	row['accuracy (mean)'] = s.mean()
	row['accuracy (stdev)'] = s.std()
	row['auroc (mean)'] = a.mean()
	row['auroc (stdev)'] = a.std()
	overall.loc[method] = row

overall = overall.sort_values('auroc (collated)', ascending=False)
print(overall)

exwrite = pd.ExcelWriter('%s/%s_bin_comparison_%d_drugs.xlsx'%(args.outfolder, args.split, len(drugs)))#, engine='xlsxwriter')
drugspec = pd.ExcelWriter('%s/%s_bin_drug_metrics_%d_drugs.xlsx'%(args.outfolder, args.split, len(drugs)))#, engine='xlsxwriter')
overall.to_excel(exwrite, sheet_name='Overall')

def get_metrics(y_true, y_pred):
	y_true = y_true.dropna()
	y_pred = y_pred.loc[y_true.index]

	acc = accuracy(y_true, y_pred)
	auroc = roc_auc_score(y_true, y_pred)
	
	y_pred = (y_pred > 0.5)*1

	if (y_pred.sum() < 1):
		precision = 0
	else:
		precision = precision_score(y_true, y_pred)
	recall = recall_score(y_true, y_pred)

	return [acc, precision, recall, auroc]

def get_per_drug_metric(y_true, y_pred):
	metrics = pd.DataFrame(columns=['accuracy', 'precision', 'recall', 'auroc'])
	for drug in y_true.columns:
		metrics.loc[drug] = get_metrics(y_true[drug], y_pred[drug])
	return metrics

def get_per_drug_fold_metric(y_true, y_pred, fold_mask):
	
	for i in range(5):
		mask = (fold_mask == i).replace(False, np.nan)
		y_true_fold = y_true*mask
		y_pred_fold = y_pred*mask

		if i == 0:
			metrics = get_per_drug_metric(y_true_fold, y_pred_fold)
		else:
			metrics += get_per_drug_metric(y_true_fold, y_pred_fold)

	return metrics/5


def calc_drugwise_metrics(to_compare, mode='collate'):

	pdm = pd.DataFrame(index=to_compare, columns=['accuracy', 'precision', 'recall', 'auroc'])
	pdm_stdev = pd.DataFrame(index=to_compare, columns=['accuracy', 'precision', 'recall', 'auroc'])
	metrics = {}

	for method in to_compare:
		if mode == 'collate':
			metrics[method] = get_per_drug_metric(y_bin, preds[method])
		elif mode == 'folds':
			metrics[method] = get_per_drug_fold_metric(y_bin, preds[method], fold_mask)

		pdm.loc[method] = metrics[method].mean()
		pdm_stdev.loc[method] = metrics[method].std()

	pdm = pdm.sort_values('auroc', ascending=False)
	pdm_stdev = pdm_stdev.loc[pdm.index]
	pdm['method'] = folders.loc[pdm.index, 'method_name']
	pdm_stdev['method'] = folders.loc[pdm.index, 'method_name']

	wilcox = []
	for i, method in enumerate(pdm.index[:-1]):
		print(method, pdm.index[i+1])
		temp = wilcoxon(metrics[method]['auroc'], metrics[pdm.index[i+1]]['auroc'], alternative='greater')[1]
		wilcox.append(temp)

		print("%s vs %s"%(method, pdm.index[i+1]), temp)

	wilcox.append(np.nan)
	pdm['auroc wilcox(>row+1) pval'] = wilcox
	pdm = pdm[['method', 'auroc', 'auroc wilcox(>row+1) pval', 'accuracy', 'precision', 'recall']]
	return pdm, metrics, pdm_stdev


print('calculating drug-wise metrics...')
pdm, metrics, pdm_stdev = calc_drugwise_metrics(to_compare, 'folds')
pdm.to_excel(exwrite, sheet_name='drugwise-mean')
pdm_stdev.to_excel(exwrite, sheet_name='drugwise-std')
to_compare = list(pdm.index)
print(pdm)


## create excel file for drug specific metrics
for col in ['auroc', 'accuracy', 'precision', 'recall']:
	temp_df = pd.DataFrame(columns=to_compare) 
	for method in to_compare:
		temp_df[method] = metrics[method][col]
	temp_df = temp_df.sort_values(to_compare[0], ascending=False)
	temp_df.columns = folders.loc[to_compare, 'method_name']
	temp_df.to_excel(drugspec, sheet_name=col)
drugspec.save()

legend = pd.read_csv('lists/legend.txt', index_col=0)
legend.to_excel(exwrite, sheet_name='legend')
exwrite.save()