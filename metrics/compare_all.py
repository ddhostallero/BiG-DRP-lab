import pandas as pd
import numpy as np
from sklearn.metrics import roc_auc_score
from scipy.stats import pearsonr, spearmanr, wilcoxon
from metrics import load_predictions, get_per_drug_metric, get_per_drug_fold_metric
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file', default='lists/LCO.py', help='file containing the list of folders to compare')
parser.add_argument('-s', '--split', default='lco', help='[lco], lco')
parser.add_argument('-o', '--outfolder', default='results/', help='output folder')
parser.add_argument('-d', '--drugset', default='lists/drug_list.txt', help='set of drugs')
args = parser.parse_args() 


folders = pd.read_csv(args.file,index_col=0,header=None,comment='#')
folders.columns=['method_name']
to_compare = list(folders.index)
print(folders)

print('loading labels...')
# y_tup = pd.read_csv('../drp-data/grl-preprocessed/drug_response/gdsc_tuple_labels_folds.csv', index_col=0)
y_tup = pd.read_csv('../grl-drp2/mutation/gdsc/gdsc_tuple_labels_folds_with_mut.csv', index_col=0)

if args.split == 'lpo':
	y_tup['fold'] = y_tup['pair_fold']
elif args.split == 'ldo':
	y_tup['fold'] = y_tup['drug_fold']
else:
	y_tup['fold'] = y_tup['cl_fold']

y_tup = y_tup.loc[y_tup['fold']>=0]
y = y_tup.pivot(index='cell_line', columns='drug', values='ln_ic50')
y_bin = y_tup.pivot(index='cell_line', columns='drug', values='resistant')

samples = list(y_tup['cell_line'].unique())
drugs = open(args.drugset).read().split('\n')
if drugs[-1] == '': drugs=drugs[:-1]

print("calculating for %d drugs and %d cell lines..."%(len(drugs), len(samples)))

y_tup = y_tup.loc[y_tup['drug'].isin(drugs)]
y = y.loc[samples, drugs]
y_bin = y_bin.loc[samples, drugs]

def create_fold_mask(tuples, label_matrix):
    x = tuples.pivot(index='cell_line', columns='drug', values='fold')
    return x

fold_mask = create_fold_mask(y_tup, y)
fold_mask = fold_mask.loc[samples, drugs]

# print(fold_mask.loc['JHOS-4'])
# print('loading predictions...')

preds_unnorm = {}
preds_norm = {}

for i, method in enumerate(to_compare):
	_, df = load_predictions(method, unnormalize=False, split=args.split, fold_mask=fold_mask) # do not normalize here
	df = df.loc[samples, drugs]
	preds_norm[method] = df
	preds_unnorm[method] = df*y.std() + y.mean()

y0 = y.replace(np.nan, 0)
null_mask = y0.values.nonzero()
y_norm = (y - y.mean())/y.std() # this is pandas dataframe so mean and std are columnwise
# y_norm = y

print('calculating overall metrics...')

cols = ["method", "auroc (collated)", "auroc (mean)", "auroc (stdev)"]
cols += ["auroc (%d)"%i for i in range(5)]
cols += ["spearman (collated)", "spearman (mean)", "spearman (stdev)"]
cols += ["spearman (%d)"%i for i in range(5)]


overall = pd.DataFrame(index=to_compare, columns=cols)
# inflated = pd.DataFrame(index=to_compare, columns=['spearman-fmean', 'spearman-all', 'auroc-all', 'auroc-fmean'])

for method in to_compare:
	
	row = {
		'method': folders.loc[method, 'method_name'],
		'spearman (collated)': spearmanr(y_norm.values[null_mask], preds_norm[method].values[null_mask])[0],
		'auroc (collated)': roc_auc_score(y_bin.values[null_mask], preds_norm[method].values[null_mask])  # normalized
	}

	s = np.zeros(5)
	a = np.zeros(5)
	for i in range(5):
		m  = ((fold_mask == i)*1).values.nonzero()
		s[i] = spearmanr(y_norm.values[m], preds_norm[method].values[m])[0]
		row['spearman (%d)'%i] = s[i]
		a[i] = roc_auc_score(y_bin.values[m], preds_norm[method].values[m])
		row['auroc (%d)'%i] = a[i]

		if 'Jessica' in method:
			print(pearsonr(y_norm.values[m], preds_norm[method].values[m]))


	row['spearman (mean)'] = s.mean()
	row['spearman (stdev)'] = s.std()
	row['auroc (mean)'] = a.mean()
	row['auroc (stdev)'] = a.std()
	overall.loc[method] = row

overall = overall.sort_values('spearman (mean)', ascending=False)
print(overall[['auroc (collated)', 'spearman (collated)', 'auroc (mean)', 'spearman (mean)']])

exwrite = pd.ExcelWriter('%s/%s_comparison_%d_drugs.xlsx'%(args.outfolder, args.split, len(drugs)))#, engine='xlsxwriter')
drugspec = pd.ExcelWriter('%s/%s_drug_metrics_%d_drugs.xlsx'%(args.outfolder, args.split, len(drugs)))#, engine='xlsxwriter')
# overall.to_excel(exwrite, sheet_name='Overall')



cols = ["spearman (collated)", "spearman (mean)"]
cols += ["spearman (%d)"%i for i in range(5)]
inf_per_fold = pd.DataFrame(index=to_compare, columns=cols)
# inflated = pd.DataFrame(index=to_compare, columns=['spearman-fmean', 'spearman-all', 'auroc-all', 'auroc-fmean'])

for method in to_compare:
	all_preds = preds_unnorm[method].values[null_mask]
	
	row = {
		'method': folders.loc[method, 'method_name'],
		'spearman (collated)': spearmanr(y.values[null_mask], preds_unnorm[method].values[null_mask])[0], # unnormalized
	}

	s = np.zeros(5)
	for i in range(5):
		m  = ((fold_mask == i)*1).values.nonzero()
		s[i] = spearmanr(y.values[m], preds_unnorm[method].values[m])[0]
		row['spearman (%d)'%i] = s[i]

	row['spearman (mean)'] = s.mean()
	inf_per_fold.loc[method] = row

inf_per_fold = inf_per_fold.sort_values('spearman (collated)', ascending=False)
print(inf_per_fold)

inf_per_fold.to_excel(exwrite, sheet_name='inflated')


def calc_drugwise_metrics(to_compare, mode='collate'):

	pdm = pd.DataFrame(index=to_compare, columns=['spearman', 'pearson', 'RMSE', 'AUROC'])
	pdm_stdev = pd.DataFrame(index=to_compare, columns=['spearman', 'pearson', 'RMSE', 'AUROC'])
	metrics = {}

	for method in to_compare:
		if mode == 'collate':
			metrics[method] = get_per_drug_metric(preds_norm[method], y_norm, y_bin=y_bin)
		elif mode == 'folds':
			metrics[method] = get_per_drug_fold_metric(preds_norm[method], y_norm, y_bin=y_bin, fold_mask=fold_mask)

		pdm.loc[method] = metrics[method].mean()
		pdm_stdev.loc[method] = metrics[method].std()

	pdm = pdm.sort_values('spearman', ascending=False)
	pdm_stdev = pdm_stdev.loc[pdm.index]
	pdm['method'] = folders.loc[pdm.index, 'method_name']
	pdm_stdev['method'] = folders.loc[pdm.index, 'method_name']

	wilcox = []
	for i, method in enumerate(pdm.index[:-1]):
		print(method, pdm.index[i+1])
		temp = wilcoxon(metrics[method]['spearman'], metrics[pdm.index[i+1]]['spearman'], alternative='greater')[1]
		wilcox.append(temp)

		print("%s vs %s"%(method, pdm.index[i+1]), temp)

	wilcox.append(np.nan)
	pdm['SCC wilcox(>row+1) pval'] = wilcox
	pdm = pdm[['method', 'spearman', 'SCC wilcox(>row+1) pval', 'pearson', 'AUROC', 'RMSE']]
	return pdm, metrics, pdm_stdev


print('calculating drug-wise metrics...')
pdm, metrics, pdm_stdev = calc_drugwise_metrics(to_compare, 'folds')
pdm.to_excel(exwrite, sheet_name='drugwise-mean')
pdm_stdev.to_excel(exwrite, sheet_name='drugwise-std')
to_compare = list(pdm.index)
print(pdm)


## create excel file for drug specific metrics
for col in ['spearman', 'pearson', 'AUROC', 'RMSE']:
	temp_df = pd.DataFrame(columns=to_compare) 
	for method in to_compare:
		temp_df[method] = metrics[method][col]
	temp_df = temp_df.sort_values(to_compare[0], ascending=False)
	temp_df.columns = folders.loc[to_compare, 'method_name']
	temp_df.to_excel(drugspec, sheet_name=col)
drugspec.save()


wilx = np.empty((len(to_compare), len(to_compare)))
wilx_p = np.empty((len(to_compare), len(to_compare)))
wilx[:,:]=np.nan
wilx_p[:,:]=np.nan
for i in range(len(to_compare)):
	for j in range(i+1, len(to_compare)):
		x = wilcoxon(metrics[to_compare[i]]['spearman'], metrics[to_compare[j]]['spearman'], alternative='greater')[1]
		wilx_p[i,j]=x
		x = (x<0.05)*0.25 + (x<0.005)*0.25 + (x<0.0005)*0.25 + (x<0.00005)*0.25
		wilx[i,j]=x


pvals = pd.DataFrame(wilx_p, index=folders.loc[to_compare, 'method_name'], columns=folders.loc[to_compare, 'method_name'])
pvals.to_excel(exwrite, sheet_name='SCC wilcox p-val')

legend = pd.read_csv('lists/legend.txt', index_col=0)
legend.to_excel(exwrite, sheet_name='legend')
exwrite.save()

for i, idx in enumerate(pdm.index):
	temp = {}
	temp['method'] = pdm.loc[idx, 'method']
	temp['auroc (fold)'] = u"%.4f (\u00B1%.4f)"%(overall.loc[idx, 'auroc (mean)'], overall.loc[idx, 'auroc (stdev)'])
	temp['spearman (fold)'] = u"%.4f (\u00B1%.4f)"%(overall.loc[idx, 'spearman (mean)'], overall.loc[idx, 'spearman (stdev)'])
	temp['auroc (collated)'] = "%.4f"%(overall.loc[idx, 'auroc (collated)'])
	temp['spearman (collated)'] = "%.4f"%(overall.loc[idx, 'spearman (collated)'])
	for x in ['spearman', 'pearson', 'AUROC', 'RMSE']:
		temp['%s (drug)'%x] = u"%.4f (\u00B1%.4f)"%(pdm.loc[idx, x], pdm_stdev.loc[idx, x])
	temp['spearman (inflated)'] = "%.4f"%(inf_per_fold.loc[idx, 'spearman (mean)'])
	
	if i == 0:
		cleaned = pd.DataFrame(index=pdm.index, columns=temp.keys())

	cleaned.loc[idx] = temp

# cleaned['auroc (fold.mean)'] = overall.loc[pdm.index, 'auroc (mean)'].values
# cleaned['auroc (fold.std)'] = overall.loc[pdm.index, 'auroc (stdev)'].values
# cleaned['spearman (fold.mean)'] = overall.loc[pdm.index, 'spearman (mean)'].values
# cleaned['spearman (fold.std)'] = overall.loc[pdm.index, 'spearman (stdev)'].values
# cleaned['auroc (collated)'] = overall.loc[pdm.index, 'auroc (collated)'].values
# cleaned['spearman (collated)'] = overall.loc[pdm.index, 'spearman (collated)'].values
# cleaned['spearman (drug.mean)'] = pdm.loc[pdm.index, 'spearman'].values
# cleaned['spearman (drug.std)'] = pdm_stdev.loc[pdm.index, 'spearman'].values
# cleaned['pearson (drug.mean)'] = pdm.loc[pdm.index, 'pearson'].values
# cleaned['pearson (drug.std)'] = pdm_stdev.loc[pdm.index, 'pearson'].values
# cleaned['auroc (drug.mean)'] = pdm.loc[pdm.index, 'auroc'].values
# cleaned['auroc (drug.std)'] = pdm_stdev.loc[pdm.index, 'auroc'].values
# cleaned['RMSE (drug.mean)'] = pdm.loc[pdm.index, 'RMSE'].values
# cleaned['RMSE (drug.std)'] = pdm_stdev.loc[pdm.index, 'RMSE'].values
# cleaned['R2 (drug.mean)'] = pdm.loc[pdm.index, 'r2_score'].values
# cleaned['spearman (inflated)'] = inf_per_fold.loc[pdm.index, 'spearman (mean)'].values

cleaned.to_csv('%s/%s_mini_%d_drugs.csv'%(args.outfolder, args.split, len(drugs)))
# plt.show() 