import pandas as pd
import numpy as np
from scipy.stats import spearmanr, wilcoxon
from metrics import load_predictions, get_per_drug_metric, get_per_drug_fold_metric
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file', default='lists/ctrp_LCO.py', help='file containing the list of folders to compare')
parser.add_argument('-s', '--split', default='lco', help='[lco], lpo')
parser.add_argument('-o', '--outfolder', default='results/', help='output folder')
parser.add_argument('-d', '--drugset', default='lists/ctrp2022_drug_list.txt', help='set of drugs')
parser.add_argument('-p', '--prefix', default='cv_prediction_testfold', help='prefix of the prediction filenames')
args = parser.parse_args() 

normalize_response = True

folders = pd.read_csv(args.file,index_col=0,header=None,comment='#')
folders.columns=['method_name']
to_compare = list(folders.index)
print(folders)

print('loading labels...')


# y_tup = pd.read_csv('../drp-data/grl-preprocessed/drug_response/ctrp_tuple_labels_folds.csv', index_col=0)
# y_tup = pd.read_csv('../drp-data/grl-preprocessed/drug_response/crispr_ctrp_ccle_tuple_labels_folds.csv', index_col=0)
# y_tup = pd.read_csv('../drp-data/grl-preprocessed/drug_response/prot_ctrp_ccle_tuple_labels_folds.csv', index_col=0)

y_tup = pd.read_csv('../drp-data/DRP2022_preprocessed/drug_response/ctrp_tuple_labels_folds.csv', index_col=0)
# y_tup = pd.read_csv('../drp-data/DRP2022_preprocessed/drug_response/crispr_ctrp_ccle_tuple_labels_folds.csv', index_col=0)
# y_tup = pd.read_csv('../drp-data/DRP2022_preprocessed/drug_response/prot_ctrp_ccle_tuple_labels_folds.csv', index_col=0)

if args.split == 'lpo':
	y_tup['fold'] = y_tup['pair_fold']
elif args.split == 'ldo':
	y_tup['fold'] = y_tup['drug_fold']
else:
	y_tup['fold'] = y_tup['cl_fold']

y_tup = y_tup.loc[y_tup['fold']>=0]
y = y_tup.pivot(index='cell_line', columns='drug', values='auc')

samples = list(y_tup['cell_line'].unique())
drugs = open(args.drugset).read().split('\n')
if drugs[-1] == '': drugs=drugs[:-1]

print("calculating for %d drugs and %d cell lines..."%(len(drugs), len(samples)))

y_tup = y_tup.loc[y_tup['drug'].isin(drugs)]
y = y.loc[samples, drugs]
y_bin = pd.DataFrame(np.ones(y.shape), index=samples, columns=drugs)

def create_fold_mask(tuples, label_matrix):
    x = tuples.pivot(index='cell_line', columns='drug', values='fold')
    return x

fold_mask = create_fold_mask(y_tup, y)
fold_mask = fold_mask.loc[samples, drugs]

preds_unnorm = {}
preds_norm = {}

for i, method in enumerate(to_compare):
	_, df = load_predictions(method, unnormalize=False, split=args.split, fold_mask=fold_mask, prefix=args.prefix) # do not normalize here
	df = df.loc[samples, drugs]
	preds_norm[method] = df

	if normalize_response:
		preds_unnorm[method] = df*y.std() + y.mean()
	else: # "unnormalized version is the original version"
		preds_unnorm[method] = df 

y0 = y.replace(np.nan, 0)
null_mask = y0.values.nonzero()

if normalize_response:
    y_norm = (y - y.mean())/y.std() # this is pandas dataframe so mean and std are columnwise
else:
    y_norm = y

print('calculating overall metrics...')

cols = ["method", "spearman (collated)", "spearman (mean)", "spearman (stdev)", "RMSE (mean)", "RMSE (stdev)"]
cols += ["spearman (%d)"%i for i in range(5)]

overall = pd.DataFrame(index=to_compare, columns=cols)

for method in to_compare:
	
	row = {
		'method': folders.loc[method, 'method_name'],
		'spearman (collated)': spearmanr(y_norm.values[null_mask], preds_norm[method].values[null_mask])[0],
	}

	s = np.zeros(5)
	rmse = np.zeros(5)
	for i in range(5):
		m  = ((fold_mask == i)*1).values.nonzero()
		s[i] = spearmanr(y_norm.values[m], preds_norm[method].values[m])[0]
		row['spearman (%d)'%i] = s[i]
		rmse[i] = np.sqrt(((y_norm.values[m]-preds_norm[method].values[m])**2).mean())

	row['spearman (mean)'] = s.mean()
	row['spearman (stdev)'] = s.std()
	row['RMSE (mean)'] = rmse.mean()
	row['RMSE (stdev)'] = rmse.std()
	overall.loc[method] = row

overall = overall.sort_values('spearman (mean)', ascending=False)
print(overall[['spearman (collated)', 'spearman (mean)', 'spearman (stdev)']])

exwrite = pd.ExcelWriter('%s/%s_comparison_%d_drugs.xlsx'%(args.outfolder, args.split, len(drugs)))#, engine='xlsxwriter')
drugspec = pd.ExcelWriter('%s/%s_drug_metrics_%d_drugs.xlsx'%(args.outfolder, args.split, len(drugs)))#, engine='xlsxwriter')

cols = ["spearman (collated)", "spearman (mean)", "spearman (stdev)"]
cols += ["spearman (%d)"%i for i in range(5)]
cols += ["MSE (%d)"%i for i in range(5)]
inf_per_fold = pd.DataFrame(index=to_compare, columns=cols)

for method in to_compare:
	all_preds = preds_unnorm[method].values[null_mask]
	
	row = {
		'method': folders.loc[method, 'method_name'],
		'spearman (collated)': spearmanr(y.values[null_mask], preds_unnorm[method].values[null_mask])[0], # unnormalized
	}

	s = np.zeros(5)
	for i in range(5):
		m  = ((fold_mask == i)*1).values.nonzero()
		s[i] = spearmanr(y.values[m], preds_unnorm[method].values[m])[0]
		row['spearman (%d)'%i] = s[i]
		row['MSE (%d)'%i] = ((y.values[m]-preds_unnorm[method].values[m])**2).mean()

	row['spearman (mean)'] = s.mean()
	row['spearman (stdev)'] = s.std()
	inf_per_fold.loc[method] = row

inf_per_fold = inf_per_fold.sort_values('spearman (collated)', ascending=False)
print(inf_per_fold)

inf_per_fold.to_excel(exwrite, sheet_name='inflated')


def calc_drugwise_metrics(to_compare, mode):

	pdm = pd.DataFrame(index=to_compare, columns=['spearman', 'pearson', 'RMSE'])
	pdm_stdev = pd.DataFrame(index=to_compare, columns=['spearman', 'pearson', 'RMSE'])
	metrics = {}

	for method in to_compare:
		if mode == 'collate':
			metrics[method] = get_per_drug_metric(preds_norm[method], y_norm, y_bin=None)
		elif mode == 'folds':
			metrics[method] = get_per_drug_fold_metric(preds_norm[method], y_norm, fold_mask, y_bin=None)

		pdm.loc[method] = metrics[method].mean()
		pdm_stdev.loc[method] = metrics[method].std()

	pdm = pdm.sort_values('spearman', ascending=False)
	pdm_stdev = pdm_stdev.loc[pdm.index]
	pdm['method'] = folders.loc[pdm.index, 'method_name']
	pdm_stdev['method'] = folders.loc[pdm.index, 'method_name']

	wilcox = []
	for i, method in enumerate(pdm.index[:-1]):
		print(method, pdm.index[i+1])
		try:
			temp = wilcoxon(metrics[method]['spearman'], metrics[pdm.index[i+1]]['spearman'], alternative='greater')[1]
		except:
			temp = np.nan
		wilcox.append(temp)

		print("%s vs %s"%(method, pdm.index[i+1]), temp)

	wilcox.append(np.nan)
	pdm['SCC wilcox(>row+1) pval'] = wilcox
	pdm = pdm[['method', 'spearman', 'SCC wilcox(>row+1) pval', 'pearson', 'RMSE']]
	return pdm, metrics, pdm_stdev


print('calculating drug-wise metrics...')
pdm, metrics, pdm_stdev = calc_drugwise_metrics(to_compare, 'folds')
pdm.to_excel(exwrite, sheet_name='drugwise-mean')
pdm_stdev.to_excel(exwrite, sheet_name='drugwise-std')
to_compare = list(pdm.index)
print(pdm)


## create excel file for drug specific metrics
for col in ['spearman', 'pearson', 'RMSE']:
	temp_df = pd.DataFrame(columns=to_compare) 
	for method in to_compare:
		temp_df[method] = metrics[method][col]
	temp_df = temp_df.sort_values(to_compare[0], ascending=False)
	temp_df.columns = folders.loc[to_compare, 'method_name']
	temp_df.to_excel(drugspec, sheet_name=col)
drugspec.save()

legend = pd.read_csv('lists/legend.txt', index_col=0)
legend.to_excel(exwrite, sheet_name='legend')
exwrite.save()

for i, idx in enumerate(pdm.index):
	temp = {}
	temp['method'] = pdm.loc[idx, 'method']
	temp['spearman (fold)'] = u"%.4f (\u00B1%.4f)"%(overall.loc[idx, 'spearman (mean)'], overall.loc[idx, 'spearman (stdev)'])
	temp['spearman (collated)'] = "%.4f"%(overall.loc[idx, 'spearman (collated)'])
	for x in ['spearman', 'pearson', 'RMSE']:
		temp['%s (drug)'%x] = u"%.4f (\u00B1%.4f)"%(pdm.loc[idx, x], pdm_stdev.loc[idx, x])
	temp['spearman (inflated)'] = "%.4f (\u00B1%.4f)"%(inf_per_fold.loc[idx, 'spearman (mean)'], inf_per_fold.loc[idx, 'spearman (stdev)'])
	temp['RMSE (fold)'] = "%.4f (\u00B1%.4f)"%(overall.loc[idx, 'RMSE (mean)'], overall.loc[idx, 'RMSE (stdev)'])
	
	if i == 0:
		cleaned = pd.DataFrame(index=pdm.index, columns=temp.keys())

	cleaned.loc[idx] = temp


# cleaned = pd.DataFrame(index=pdm.index)
# cleaned['spearman (fold.mean)'] = overall.loc[pdm.index, 'spearman (mean)'].values
# cleaned['spearman (fold.std)'] = overall.loc[pdm.index, 'spearman (stdev)'].values
# cleaned['spearman (collated)'] = overall.loc[pdm.index, 'spearman (collated)'].values
# cleaned['spearman (drug.mean)'] = pdm.loc[pdm.index, 'spearman'].values
# cleaned['spearman (drug.std)'] = pdm_stdev.loc[pdm.index, 'spearman'].values
# cleaned['pearson (drug.mean)'] = pdm.loc[pdm.index, 'pearson'].values
# cleaned['pearson (drug.std)'] = pdm_stdev.loc[pdm.index, 'pearson'].values
# cleaned['RMSE (drug.mean)'] = pdm.loc[pdm.index, 'RMSE'].values
# cleaned['RMSE (drug.std)'] = pdm_stdev.loc[pdm.index, 'RMSE'].values
# cleaned['R2 (drug.mean)'] = pdm.loc[pdm.index, 'r2_score'].values
# cleaned['spearman (inflated)'] = inf_per_fold.loc[pdm.index, 'spearman (mean)'].values

cleaned.to_csv('%s/%s_mini_%d_drugs.csv'%(args.outfolder, args.split, len(drugs)))