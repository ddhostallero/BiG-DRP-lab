import pandas as pd 


data = 'test'
folders = {
	'results/new_arki/baseline_mlp/': 'MLP',
	'results/new_arki/bigdrp/': 'BiG-DRP',
	'results/new_arki/vae_mlp/mlp/no_reparam': 'VAE+MLP',
	'results/new_arki/vae_mlp/mlp/': 'VAE(r)+MLP',
	'results/new_arki/ae_mlp/mlp/': 'AE+MLP',
	'results/new_arki/b2sc_mlp/mlp/': 'B2SC+MLP',
	'results/new_arki/cosvae20_mlp/mlp/sc_enc': 'CosVAE(SC)+MLP',
	'results/new_arki/cosvae20_mlp/mlp/': 'CosVAE(B)+MLP',
}

# folders = {
# 	'results/new_arki/baseline_mlp/test_tpm/': 'MLP',
# 	'results/new_arki/bigdrp/test_tpm/': 'BiG-DRP',
# 	'results/new_arki/vae_mlp/mlp/test_tpm/': 'VAE+MLP',
# 	'results/new_arki/ae_mlp/mlp/test_tpm/': 'AE+MLP',
# }

idx = pd.MultiIndex.from_product([["MeanPred", "Propagate"], ["SCC", "RMSE"]], names=["scheme", "metric"])
summary = pd.DataFrame(index=folders.values(), columns=idx)
temp = pd.Series(index=folders.values())

for key, value in folders.items():
	x = pd.read_excel("%s/drugwise_%s_performance.xlsx"%(key, data), sheet_name='summary', index_col=0, header=[0,1])
	temp[value] = x.loc['MeanPred', ('scc', 'mean')]

	summary.loc[value, ("MeanPred", "SCC")] = u"%.4f (\u00B1%.4f)"%(
		x.loc['MeanPred', ('scc', 'mean')],
		x.loc['MeanPred', ('scc', 'stdev')])
	summary.loc[value, ("MeanPred", "RMSE")] = u"%.4f (\u00B1%.4f)"%(
		x.loc['MeanPred', ('rmse', 'mean')],
		x.loc['MeanPred', ('rmse', 'stdev')])

	summary.loc[value, ("Propagate", "SCC")] = u"%.4f (\u00B1%.4f)"%(
		x.loc['Propagate', ('scc', 'mean')],
		x.loc['Propagate', ('scc', 'stdev')])
	summary.loc[value, ("Propagate", "RMSE")] = u"%.4f (\u00B1%.4f)"%(
		x.loc['Propagate', ('rmse', 'mean')],
		x.loc['Propagate', ('rmse', 'stdev')])

order = temp.sort_values(ascending=False).index
print(summary.loc[order])