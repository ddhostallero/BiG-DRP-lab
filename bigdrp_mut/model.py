import torch
import torch.nn as nn
import torch.nn.functional as F
import dgl
from dgl import DGLGraph
from dgl.nn.pytorch import HeteroGraphConv, GraphConv, GATConv


class BiGDRPmut(nn.Module):
    def __init__(self, n_genes, n_cl_feats, n_drug_feats, n_gene_nodes, rel_names, hyp):
        super(BiGDRPmut, self).__init__()

        # hgcn1 = {}
        # hgcn2 = {}
    
        # for rel in rel_names:
        #     if rel in ['has_mutation', 'is_mutated']:
        #         hgcn1[rel] = dgl.nn.GATConv(in_feats=hyp['common_dim'], out_feats=hyp['common_dim'], num_heads=1, activation=torch.squeeze)
        #         hgcn2[rel] = dgl.nn.GATConv(in_feats=hyp['common_dim'], out_feats=hyp['common_dim'], num_heads=1, activation=torch.squeeze)
        #     else:
        #         hgcn1[rel] = dgl.nn.GraphConv(in_feats=hyp['common_dim'], out_feats=hyp['common_dim'])
        #         hgcn2[rel] = dgl.nn.GraphConv(in_feats=hyp['common_dim'], out_feats=hyp['common_dim'])

        # self.conv1 = HeteroGraphConv(hgcn1)
        # self.conv2 = HeteroGraphConv(hgcn2)

        self.conv1 = HeteroGraphConv(
            {rel: dgl.nn.GraphConv(in_feats=hyp['common_dim'], out_feats=hyp['common_dim']) for rel in rel_names})
        self.conv2 = HeteroGraphConv(
            {rel: dgl.nn.GraphConv(in_feats=hyp['common_dim'], out_feats=hyp['common_dim']) for rel in rel_names})

        self.gene_node_embeddings = nn.Embedding(n_gene_nodes, hyp['common_dim'])

        self.drug_l1 = nn.Linear(n_drug_feats, hyp['common_dim'])
        self.cell_l1 = nn.Linear(n_cl_feats, hyp['expr_enc'])
        self.cell_l2 = nn.Linear(hyp['expr_enc'], hyp['common_dim'])

        self.mid = nn.Linear(2*hyp['common_dim'], hyp['mid'])
        # self.out = nn.Linear(hyp['mid'], 1)

        if hyp['binary']:
            self.out = nn.Sequential(
                nn.Linear(hyp['mid'], 1),
                nn.Sigmoid()
            )
        else:
            self.out = nn.Linear(hyp['mid'], 1)


        if hyp['drop'] == 0:
            drop=[0,0]
        else:
            drop=[0.2,0.5]

        self.in_drop = nn.Dropout(drop[0])
        self.mid_drop = nn.Dropout(drop[1])
        self.alpha = 0.5
        

    def forward(self, blocks, drug_features, cell_features_in_network, cell_index, drug_index, gene_index):
        
        cell_enc = F.leaky_relu(self.cell_l1(cell_features_in_network))
        cell_enc = F.leaky_relu(self.cell_l2(cell_enc))        
        drug_enc = F.leaky_relu(self.drug_l1(drug_features))

        node_features = {'drug': drug_enc, 'cell_line': cell_enc, 'gene':self.gene_node_embeddings(gene_index)}
        
        for k, v in node_features.items():
            blocks[0].dstnodes[k].data['h0'] = v[blocks[0].dstnodes(k)]

        h1 = self.conv1(blocks[0], node_features)
        h1 = {k: F.leaky_relu(v + self.alpha*blocks[0].dstnodes[k].data['h0']) for k, v in h1.items()}

        for k, v in h1.items():
            blocks[1].dstnodes[k].data['h1'] = v[blocks[1].dstnodes(k)]

        h2 = self.conv2(blocks[1], h1)
        h2 = {k: F.leaky_relu(h2[k] + self.alpha*blocks[1].dstnodes[k].data['h1']) for k in ['cell_line', 'drug']}

        expr_enc = h2['cell_line'][cell_index]
        drug_enc = h2['drug'][drug_index]

        x = torch.cat([expr_enc,drug_enc],-1) # (batch, expr_enc_size+drugs_enc_size)
        x = self.in_drop(x)
        x = F.leaky_relu(self.mid(x)) 
        x = self.mid_drop(x)
        out = self.out(x)
        return out

    def embed_all(self, network, drug_features, cell_features_in_network):
        cell_enc = F.leaky_relu(self.cell_l1(cell_features_in_network))
        cell_enc = F.leaky_relu(self.cell_l2(cell_enc))        
        drug_enc = F.leaky_relu(self.drug_l1(drug_features))

        node_features = {'drug': drug_enc, 'cell_line': cell_enc, 'gene':self.gene_node_embeddings.weight}

        h1 = self.conv1(network, node_features)
        h1 = {k: F.leaky_relu(v + self.alpha*node_features[k]) for k, v in h1.items()}

        h2 = self.conv2(network, h1)
        h2 = {k: F.leaky_relu(v + self.alpha*h1[k]) for k, v in h2.items()}
        
        return h2

    def predict_from_embedding(self, expr_enc, drug_enc):

        expr_enc = expr_enc.unsqueeze(1) # (batch, 1, expr_enc_size)
        drug_enc = drug_enc.unsqueeze(0) # (1, n_drugs, drug_enc_size)

        expr_enc = expr_enc.repeat(1,drug_enc.shape[1],1) # (n_samples, n_drugs, expr_enc_size)
        drug_enc = drug_enc.repeat(expr_enc.shape[0],1,1) # (n_samples, n_drugs, drug_enc_size)

        x = torch.cat([expr_enc,drug_enc],-1) # (batch, n_drugs, expr_enc_size+drugs_enc_size)
        x = self.in_drop(x)
        x = F.leaky_relu(self.mid(x)) # (batch, n_drugs, 1)
        x = self.mid_drop(x)
        out = self.out(x) # (batch, n_drugs, 1)
        out = out.view(-1, drug_enc.shape[1])
        return out