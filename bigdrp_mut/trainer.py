import torch
from bigdrp_mut.model import BiGDRPmut
import time
import numpy as np
from collections import deque
from dgl.dataloading import MultiLayerFullNeighborSampler,NeighborSampler
from base.trainer import BaseTrainer

class Trainer(BaseTrainer):
    def __init__(self, n_genes, cell_feats, drug_feats, network, hyp, test=False, load_model_path=None):
        super(Trainer, self).__init__(hyp)

        self.cell_feats = cell_feats
        self.drug_feats = drug_feats
        self.network = network

        if load_model_path is not None:
            self.model = ModelHead(n_genes, hyp)
            self.model.load_state_dict(torch.load(load_model_path), strict=False)
            self.model = self.model.to(self.device)

        if not test:
            self.model = BiGDRPmut(n_genes, self.cell_feats.shape[1], drug_feats.shape[1], 
                network.num_nodes('gene'), network.etypes, hyp)
            
            self.mse_loss = torch.nn.MSELoss()
            params = self.model.parameters()
            self.optimizer = torch.optim.Adam(params, lr=hyp['learning_rate'])

            graph_sampler = MultiLayerFullNeighborSampler(2)
            self.network.ndata['features'] = {
                'drug': self.drug_feats, 
                'cell_line': self.cell_feats,
                'gene': torch.arange(network.num_nodes('gene'))}#self.model.gene_node_embeddings.weight}
            _,_, blocks = graph_sampler.sample_blocks(
                self.network, {'drug': range(len(drug_feats)), 'cell_line': range(len(cell_feats))})

            self.blocks = blocks
            self.train_sampler = NeighborSampler(fanouts=[{
                    ('cell_line', 'has_mutation', 'gene'): 10,
                    ('gene', 'is_mutated', 'cell_line'): 10,
                    ('cell_line', 'is_sensitive', 'drug'): 10,
                    ('drug', 'is_effective', 'cell_line'): 10,
                    ('cell_line', 'is_resistant', 'drug'): 10,
                    ('drug', 'is_ineffective', 'cell_line'): 10}]*2,)

            self.model = self.model.to(self.device)


    def sampled_train_step(self, train_loader, device):
        self.model.train()
        _loss = 0
        for (x1, d1, y) in train_loader:
            x1u = torch.unique(x1)
            d1u = torch.unique(d1)
            _,_,blocks = self.train_sampler.sample_blocks(self.network, {'cell_line': x1u, 'drug': d1u})
                
            temp = {k:i for i, k in enumerate(blocks[-1].dstnodes['cell_line'].data['_ID'].numpy())}
            x1 = torch.LongTensor([temp[i] for i in x1.numpy()])
            temp = {k:i for i, k in enumerate(blocks[-1].dstnodes['drug'].data['_ID'].numpy())}
            d1 = torch.LongTensor([temp[i] for i in d1.numpy()])

            blocks = [b.to(device) for b in blocks]
            cell_feats = blocks[0].ndata['features']['cell_line']
            drug_feats = blocks[0].ndata['features']['drug']
            gene_index = blocks[0].ndata['features']['gene']
            x1, d1, y = x1.to(device), d1.to(device), y.to(device)

            self.optimizer.zero_grad()
            pred = self.model(blocks, drug_feats, cell_feats, x1, d1, gene_index)
            loss = self.loss(pred, y)
            _loss += loss
            loss.backward()
            self.optimizer.step()

        return [_loss.item()/len(train_loader)]


    def train_step(self, train_loader, device):

        blocks = [b.to(device) for b in self.blocks]
        cell_feats = blocks[0].ndata['features']['cell_line']
        drug_feats = blocks[0].ndata['features']['drug']
        gene_index = blocks[0].ndata['features']['gene']

        _loss = 0
        self.model.train()
        for (x1, d1, y) in train_loader:
            x1, d1, y = x1.to(device), d1.to(device), y.to(device)
            self.optimizer.zero_grad()
            pred = self.model(blocks, drug_feats, cell_feats, x1, d1, gene_index)
            loss = self.loss(pred, y)
            _loss += loss

            loss.backward()
            self.optimizer.step()

        return [_loss.item()/len(train_loader)]

    def validation_step(self, val_blocks, val_loader, device):

        blocks = [b.to(device) for b in val_blocks]
        cell_feats = blocks[0].ndata['features']['cell_line']
        drug_feats = blocks[0].ndata['features']['drug']
        gene_index = blocks[0].ndata['features']['gene']

        self.model.eval()
        val_loss = 0
        preds = []
        ys = []
        with torch.no_grad():
            for (x1, d1, y) in val_loader:
                ys.append(y)
                x1, d1, y = x1.to(device), d1.to(device), y.to(device)
                pred = self.model(blocks, drug_feats, cell_feats, x1, d1, gene_index)
                preds.append(pred)
                val_loss += self.val_loss(pred, y) #((pred - y)**2).sum()


        preds = torch.cat(preds, axis=0).cpu().detach().numpy().reshape(-1)
        ys = torch.cat(ys, axis=0).reshape(-1)
        met1, met2 = self.metrics_callback(ys, preds)

        return val_loss.item()/len(ys), met1, met2


    def predict_matrix(self, network, cell_index):
        """
        returns a prediction matrix of (N, n_drugs)
        """

        cell_index = cell_index.unsqueeze(1).to(self.device)
        network = network.to(self.device)

        self.model.eval()
        preds = []

        with torch.no_grad():
            all_embeddings = self.model.embed_all(network, network.ndata['features']['drug'], network.ndata['features']['cell_line'])
            drug_emb = all_embeddings['drug']

            for i in cell_index:
                expr_emb = all_embeddings['cell_line'][i]
                pred = self.model.predict_from_embedding(expr_emb, drug_emb)
                preds.append(pred)

        preds = torch.cat(preds, axis=0).cpu().detach().numpy()
        return preds


    def fit(self, val_blocks,
        num_epoch, train_loader, val_loader, tuning=False, final=False):

        start_time = time.time()

        ret_matrix = np.zeros((num_epoch, 4))
        loss_deque = deque([], maxlen=5)

        if self.binary:
            metric_names = ['val BCE', 'val accuracy', 'val auroc', 'train BCE']
        else:
            metric_names = ['val MSE', 'val pearsonr', 'val spearmanr', 'train MSE']

        best_loss_avg5 = np.inf
        count = 0

        for epoch in range(num_epoch):
            train_metrics = self.sampled_train_step(train_loader, self.device)
            # train_metrics = self.train_step(train_loader, self.device)
            val_metrics = self.validation_step(val_blocks, val_loader, self.device)

            ret_matrix[epoch] = list(val_metrics) + list(train_metrics)

            loss_deque.append(val_metrics[0])
            loss_avg5 = sum(loss_deque)/len(loss_deque)

            if best_loss_avg5 > loss_avg5:
                best_loss_avg5 = loss_avg5
                count = 0
            else:
                count += 1

            elapsed_time = time.time() - start_time
            start_time = time.time()
            print("%d\t%s:%.4f\t%s:%.4f\t%s:%.4f\t%ds"%(
                epoch+1, 
                metric_names[0], val_metrics[0], 
                metric_names[3], train_metrics[0], 
                metric_names[2], val_metrics[2], int(elapsed_time)))
            
            if count == 10 and not final:
                ret_matrix = ret_matrix[:epoch+1]
                break            

        return ret_matrix, metric_names