import torch
from base.trainer import BaseTrainer

class Trainer(BaseTrainer):
    def __init__(self, model, hyp, test=False, load_model_path=None):
        super(Trainer, self).__init__(hyp)

        self.model = model.to(self.device)

        # Freeze encoder parameters
        # for name, param in self.model.encoder.named_parameters(): # could be AE, but the name is just VAE
        #     param.requires_grad = False

        # print("Training the following parameters:")
        # for name, param in self.model.named_parameters():
        #     if param.requires_grad:
        #         print(name, "requires grad")
        params =[
                {'params': self.model.predictor.parameters()},
                {'params': self.model.encoder.parameters(), 'lr': hyp['learning_rate']*0.05}]

        self.optimizer = torch.optim.Adam(params, lr=hyp['learning_rate'])


    def train_step(self, train_loader, device):
        self.model.train()
        for (expr, d1, y) in train_loader:

            expr, d1, y = expr.to(device), d1.to(device), y.to(device)
            
            self.optimizer.zero_grad()
            pred = self.model(expr, d1, reparametrize=True)
            loss = self.loss(pred, y)

            loss.backward()
            self.optimizer.step()

        return [loss.item()]

    def validation_step(self, val_loader, device):
        self.model.eval()

        val_loss = 0
        preds = []
        ys = []
        with torch.no_grad():
            for (expr, d1, y) in val_loader:
                ys.append(y)
                expr, d1, y = expr.to(device), d1.to(device), y.to(device)
                pred = self.model(expr, d1, reparametrize=False)
                preds.append(pred)
                val_loss += self.val_loss(pred, y)

        preds = torch.cat(preds, axis=0).cpu().detach().numpy().reshape(-1)
        val_loss /= len(val_loader.dataset)

        ys = torch.cat(ys, axis=0).reshape(-1)
        
        met1, met2 = self.metrics_callback(ys, preds)
        
        return val_loss.item(), met1, met2

    def predict(self, data_loader):
        self.model.eval()

        preds = []
        ys = []
        with torch.no_grad():
            for (expr, d1, y) in data_loader:
                ys.append(y)
                expr, d1, y = expr.to(self.device), d1.to(self.device), y.to(self.device)
                pred = self.model(expr, d1, reparametrize=False)
                preds.append(pred)

        preds = torch.cat(preds, axis=0).cpu().detach().numpy().reshape(-1)
        ys = torch.cat(ys, axis=0).numpy().reshape(-1)
        return preds, ys

    def predict_matrix(self, data_loader, drug_features):
        self.model.eval()
        
        drug_features = drug_features.to(self.device)

        preds = []
        with torch.no_grad():
            for (x,) in data_loader:
                x = x.to(self.device)
                pred = self.model._predict_response_matrix(x, drug_features, reparametrize=False, bulk=True) # use mu for test
                preds.append(pred)

        preds = torch.cat(preds, axis=0).cpu().detach().numpy()
        return preds