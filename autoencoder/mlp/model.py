
import torch
import torch.nn as nn
import torch.nn.functional as F
import json

class Predictor(nn.Module):
  def __init__(self, n_drug_feats, hyp):
    super(Predictor, self).__init__()
    self.drug_l1 = nn.Linear(n_drug_feats, hyp['drug_l1'])
    self.mid = nn.Linear(hyp['gene_l3']+hyp['drug_l1'], hyp['mid'])
    
    if hyp['binary']:
        self.out = nn.Sequential(
                nn.Linear(hyp['mid'], 1),
                nn.Sigmoid())
    else:
        self.out = nn.Linear(hyp['mid'], 1)

    if hyp['drop'] == 0:
        drop=[0,0]
    else:
        drop=[0.2,0.5]

    self.in_drop = nn.Dropout(drop[0])
    self.mid_drop = nn.Dropout(drop[1])

  def forward(self, expr_enc, drug):
    drug = F.leaky_relu(self.drug_l1(drug))

    x = torch.cat([expr_enc, drug], dim=1)
    x = self.in_drop(x)
    x = F.leaky_relu(self.mid(x))
    x = self.mid_drop(x)
    x = self.out(x)
    return x
  
  def _predict_response_matrix(self, expr_enc, drug_features):
    drug_enc = F.leaky_relu(self.drug_l1(drug_features))

    expr_enc = expr_enc.unsqueeze(1) # (batch, 1, expr_enc_size)
    drug_enc = drug_enc.unsqueeze(0) # (1, n_drugs, drug_enc_size)
    
    expr_enc = expr_enc.repeat(1,drug_enc.shape[1],1) # (batch, n_drugs, expr_enc_size)
    drug_enc = drug_enc.repeat(expr_enc.shape[0],1,1) # (batch, n_drugs, drug_enc_size)
    
    x = torch.cat([expr_enc,drug_enc],-1) # (batch, n_drugs, expr_enc_size+drugs_enc_size)
    x = self.in_drop(x)
    x = F.leaky_relu(self.mid(x)) # (batch, n_drugs, 1)
    x = self.mid_drop(x)
    out = self.out(x) # (batch, n_drugs, 1)
    out = out.view(-1, drug_enc.shape[1]) # (batch, n_drugs)
    return out

class Model(nn.Module):
  def __init__(self, n_drug_feats, encoder, hyp):
    super(Model, self).__init__()

    self.hyp = hyp
    self.encoder = encoder
    self.predictor = Predictor(n_drug_feats, hyp)

  def forward(self, expr, drug, reparametrize):
    expr_enc = self.encoder.encode(expr, reparametrize)
    return self.predictor(expr_enc, drug)

  def _predict_response_matrix(self, cell_features, drug_features, reparametrize, bulk):
    expr_enc = self.encoder.encode(cell_features, reparametrize)
    return self.predictor._predict_response_matrix(expr_enc, drug_features)

  def predict_matrix(self, data_loader, drug_features, 
      device='cuda', reparametrize=False, bulk=True):
      self.eval()
      
      drug_features = drug_features.to(device)

      preds = []
      with torch.no_grad():
          for (x,) in data_loader:
              x = x.to(device)
              pred = self._predict_response_matrix(x, drug_features, reparametrize, bulk)
              preds.append(pred)

      preds = torch.cat(preds, axis=0).cpu().detach().numpy()
      return preds

  def save_model(self, directory, fold_id):
    torch.save(self.state_dict(), directory+'/model_weights_fold_%d'%fold_id)

    x = json.dumps(self.hyp)
    f = open(directory+"/model_config_fold_%d.txt"%fold_id,"w")
    f.write(x)
    f.close()
