from utils.tuple_dataset import TupleMapDatasetV2
from utils.utils import reindex_tuples, reset_seed, create_fold_mask, load_hyperparams
from utils.data_initializer import initialize

import torch
from torch.utils.data import DataLoader, TensorDataset
import numpy as np
import pandas as pd
import pickle

from autoencoder.mlp.trainer import Trainer
from autoencoder.ae.model import AE
from autoencoder.vae.model import VAE
from autoencoder.cosvae.model import CosVAE
from autoencoder.mlp.model import Model

def load_enc(n_genes, hyperparams, enc_dir):
    if hyperparams['enc_type'] == 'ae':
        enc = AE(n_genes, hyperparams)
    elif hyperparams['enc_type'] == 'cosvae':
        enc = CosVAE(n_genes, hyperparams)
    else: # vae is default
        enc = VAE(n_genes, hyperparams)

    enc.load_state_dict(torch.load(enc_dir), strict=True)
    return enc

def fold_validation(hyperparams, seed, train_data, val_data, tuning, epoch, enc_dir, maxout=False, revert=False):
    reset_seed(seed)
    train_loader = DataLoader(train_data, batch_size=hyperparams['batch_size'], shuffle=True, drop_last=True)
    val_loader = DataLoader(val_data, batch_size=hyperparams['batch_size'], shuffle=False)

    n_genes = train_data.cell_features.shape[1]
    n_drug_feats = train_data.drug_features.shape[1]
    enc = load_enc(n_genes, hyperparams, enc_dir)
    model = Model(n_drug_feats, enc, hyperparams)
    trainer = Trainer(model, hyperparams)

    val_error, metric_names, best_epoch = trainer.fit(
            num_epoch=epoch, 
            train_loader=train_loader, 
            val_loader=val_loader,
            tuning=tuning,
            maxout=maxout,
            revert=revert)
    
    return val_error, trainer, metric_names, best_epoch

def create_dataset(labels, cell_lines, drug_feats, train_folds, val_fold, normalizer):
    drug_list = drug_feats.index
    
    train_tuples = labels.loc[labels['fold'].isin(train_folds)]
    train_samples = list(train_tuples['cell_line'].unique())
    train_x = cell_lines.loc[train_samples].values

    val_tuples = labels.loc[labels['fold'] == val_fold]
    val_samples = list(val_tuples['cell_line'].unique())
    val_x = cell_lines.loc[val_samples].values

    train_tuples = train_tuples[['drug', 'cell_line', 'response']]
    train_tuples = reindex_tuples(train_tuples, drug_list, train_samples) # all drugs exist in all folds
    val_tuples = val_tuples[['drug', 'cell_line', 'response']]
    val_tuples = reindex_tuples(val_tuples, drug_list, val_samples) # all drugs exist in all folds

    train_x = normalizer.transform(train_x)
    val_x = normalizer.transform(val_x)

    train_data = TupleMapDatasetV2(train_tuples, drug_feats, train_x)
    val_data = TupleMapDatasetV2(val_tuples, drug_feats, val_x)

    return train_data, val_data, train_samples, val_samples

def nested_cross_validation(FLAGS, drug_feats, cell_lines, labels, hyperparams):
    reset_seed(FLAGS.seed)
    label_mask = create_fold_mask(labels)
    final_metrics = None

    for i in range(5):
        print('==%d=='%i)
        test_fold = i 
        val_fold = (i+1)%5
        train_folds = [x for x in range(5) if (x != test_fold) and (x != val_fold)]

        enc_dir = FLAGS.outroot + FLAGS.weight_folder + "/model_weights_fold_%d"%i
        norm_dir = FLAGS.outroot + FLAGS.weight_folder + "/normalizer_fold_%d.pkl"%i
        hyp_dir = FLAGS.outroot + FLAGS.weight_folder + "/model_config_fold_%d.txt"%i

        hp = load_hyperparams(hyp_dir)
        for key, value in hyperparams.items():
            hp[key] = value # overwrite if needed

        normalizer = pickle.load(open(norm_dir, 'rb'))

        # === find number of epochs ===

        train_data, val_data, _, _ = create_dataset(
            labels, cell_lines, drug_feats, train_folds, val_fold, normalizer)

        val_error,_,_,best_epoch = fold_validation(hp, seed=FLAGS.seed, 
            train_data=train_data, 
            val_data=val_data, 
            tuning=False, 
            enc_dir=enc_dir, epoch=hp['num_epoch'], maxout=False)

        hp['num_epoch'] = int(max(best_epoch, 2)) 

        # === actual test fold ===

        train_folds = train_folds + [val_fold]
        train_data, test_data, _, test_samples = create_dataset(
            labels, cell_lines, drug_feats, train_folds, test_fold, normalizer)

        test_error, trainer, metric_names, _ = fold_validation(hp, seed=FLAGS.seed, 
            train_data=train_data, 
            val_data=test_data, 
            tuning=False, 
            epoch=hp['num_epoch'], 
            enc_dir=enc_dir, maxout=True) # set final so that the trainer uses all epochs

        if i == 0:
            final_metrics = np.zeros((5, test_error.shape[1]))

        # save logs
        final_metrics[i] = test_error[-1]
        test_metrics = pd.DataFrame(test_error, columns=metric_names)
        test_metrics.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/fold_%d.csv'%i, index=False)

        # save model
        trainer.save_model(FLAGS.outroot + "/results/" + FLAGS.folder, i, hp)

        # save predictions
        test_data = TensorDataset(test_data.cell_features)
        test_data = DataLoader(test_data, batch_size=hyperparams['batch_size'], shuffle=False)
        prediction_matrix = trainer.predict_matrix(test_data, torch.Tensor(drug_feats.values))
        prediction_matrix = pd.DataFrame(prediction_matrix, index=test_samples, columns=drug_feats.index)

        # remove predictions for non-test data
        test_mask = ((label_mask.loc[test_samples]==test_fold)*1)[drug_feats.index]
        test_mask = test_mask.replace(0, np.nan)
        prediction_matrix = prediction_matrix*test_mask

        prediction_matrix.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/cv_prediction_testfold_%d.csv'%i)
    
    final_metrics = pd.DataFrame(final_metrics, columns=metric_names)
    return final_metrics

def single_cross_validation(FLAGS, drug_feats, cell_lines, labels, hyperparams):
    reset_seed(FLAGS.seed)
    label_mask = create_fold_mask(labels)
    final_metrics = None

    for i in range(5):
        print('==%d=='%i)
        test_fold = i 
        val_fold = (i+1)%5
        train_folds = [x for x in range(5) if (x != test_fold) and (x != val_fold)]

        enc_dir = FLAGS.outroot + FLAGS.weight_folder + "/model_weights_fold_%d"%i
        norm_dir = FLAGS.outroot + FLAGS.weight_folder + "/normalizer_fold_%d.pkl"%i
        hyp_dir = FLAGS.outroot + FLAGS.weight_folder + "/model_config_fold_%d.txt"%i

        hp = load_hyperparams(hyp_dir)
        for key, value in hyperparams.items():
            hp[key] = value # overwrite if needed

        normalizer = pickle.load(open(norm_dir, 'rb'))

        # === Train then revert to best validation state ===

        train_data, val_data, _, val_samples = create_dataset(
            labels, cell_lines, drug_feats, train_folds, val_fold, normalizer)

        val_error, trainer, metric_names, best_epoch = fold_validation(hp, seed=FLAGS.seed, 
            train_data=train_data, 
            val_data=val_data, 
            tuning=False, 
            enc_dir=enc_dir, 
            epoch=hp['num_epoch'], maxout=False, revert=True)

        hp['num_epoch'] = best_epoch

        # === Test ===

        train_folds = train_folds + [val_fold]
        _, test_data, _, test_samples = create_dataset(
            labels, cell_lines, drug_feats, train_folds, test_fold, normalizer=normalizer)

        if i == 0:
            final_metrics = np.zeros((5, val_error.shape[1]))

        # save logs
        final_metrics[i] = val_error[-1]
        val_metrics = pd.DataFrame(val_error, columns=metric_names)
        val_metrics.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/fold_%d.csv'%i, index=False)

        # save model
        trainer.save_model(FLAGS.outroot + "/results/" + FLAGS.folder, i, hp)

        # validation predictions
        val_data = TensorDataset(val_data.cell_features)
        val_data = DataLoader(val_data, batch_size=hyperparams['batch_size'], shuffle=False)
        prediction_matrix = trainer.predict_matrix(val_data, torch.Tensor(drug_feats.values))
        prediction_matrix = pd.DataFrame(prediction_matrix, index=val_samples, columns=drug_feats.index)

        # remove predictions for non-val data
        val_mask = ((label_mask.loc[val_samples]==test_fold)*1)[drug_feats.index]
        val_mask = val_mask.replace(0, np.nan)
        prediction_matrix = prediction_matrix*val_mask
        prediction_matrix.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/cv_prediction_valfold_%d.csv'%i)

        # test predictions
        test_data = TensorDataset(test_data.cell_features)
        test_data = DataLoader(test_data, batch_size=hyperparams['batch_size'], shuffle=False)
        prediction_matrix = trainer.predict_matrix(test_data, torch.Tensor(drug_feats.values))
        prediction_matrix = pd.DataFrame(prediction_matrix, index=test_samples, columns=drug_feats.index)

        # remove predictions for non-test data
        test_mask = ((label_mask.loc[test_samples]==test_fold)*1)[drug_feats.index]
        test_mask = test_mask.replace(0, np.nan)
        prediction_matrix = prediction_matrix*test_mask

        prediction_matrix.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/cv_prediction_testfold_%d.csv'%i)
        trainer = None # release allocation
    
    final_metrics = pd.DataFrame(final_metrics, columns=metric_names)
    return final_metrics

def main(FLAGS):
    hyperparams = {
        'learning_rate': 1e-4,
        'num_epoch': 50,
        'batch_size': 128,
        'drug_l1': 512,
        'mid': 512,
        'drop': 1,
        'binary': FLAGS.response_type=='binary'}

    drug_feats, cell_lines, labels, _, _ = initialize(FLAGS)

    if FLAGS.cv == 'double':
        test_metrics = nested_cross_validation(FLAGS, drug_feats, cell_lines, labels, hyperparams)
        print("Overall Performance (on Test)")
    elif FLAGS.cv == 'single':
        test_metrics = single_cross_validation(FLAGS, drug_feats, cell_lines, labels, hyperparams)
        print("Overall Performance (on Validation)")

    test_metrics = test_metrics.mean(axis=0)
    print("Overall Performance")
    print(test_metrics)