import torch
import torch.nn as nn
import torch.nn.functional as F
import json

from vae_b2b.model import Encoder as VAE_Encoder
from vae_b2b.model import Decoder as VAE_Decoder
from ae_b2b.model import Encoder as AE_Encoder
from ae_b2b.model import Decoder as AE_Decoder


class CosVAE(nn.Module):
  def __init__(self, in_dim, hyp):
    super(CosVAE, self).__init__()

    self.hyp = hyp
    self.bulk_encoder = VAE_Encoder(in_dim, hyp)
    self.bulk_decoder = VAE_Decoder(hyp, in_dim)
    self.sc_encoder = AE_Encoder(in_dim, hyp)
    self.sc_decoder = AE_Decoder(hyp, in_dim)

  def forward(self, x_bulk, x_sc):

    # bulk
    mu, logvar = self.bulk_encoder(x_bulk)
    z_bulk = self.reparametrize(mu, logvar)
    x_bulk_hat = self.bulk_decoder(z_bulk)
    
    # sc
    z_sc = self.sc_encoder(x_sc)
    x_sc_hat = self.sc_decoder(z_sc)

    return mu, logvar, z_bulk, x_bulk_hat, z_sc, x_sc_hat

  def reparametrize(self, mu, logvar):
    std = torch.exp(0.5*logvar)
    eps = torch.randn_like(std)
    return mu + (eps*std) 

  def encode(self, x, reparametrize=True, bulk=True):
    if bulk:
      mu, logvar = self.bulk_encoder(x)

      if reparametrize:
        z = self.reparametrize(mu, logvar)
      else:
        z = mu
    else:
      z = self.sc_encoder(x)
    return z

  def save_model(self, directory, fold_id):
        torch.save(self.state_dict(), directory+'/model_weights_fold_%d'%fold_id)

        x = json.dumps(self.hyp)
        f = open(directory+"/model_config_fold_%d.txt"%fold_id,"w")
        f.write(x)
        f.close()