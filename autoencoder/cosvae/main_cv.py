from utils.utils import mkdir, reindex_tuples, reset_seed, create_fold_mask
from utils.data_initializer import initialize_ccl, initialize_singlecell

import torch
from torch.utils.data import DataLoader, Dataset
from cosvae.trainer import Trainer
from cosvae.model import CosVAE as Model
import numpy as np
import pandas as pd

class MixedDataset(Dataset):
    def __init__(self, bulk_expr, sc_expr, sc2b_mapping, true_pair_rate=0.5):
        self.sc_expr = torch.FloatTensor(sc_expr)
        self.bulk_expr = torch.FloatTensor(bulk_expr)
        self.sc2b_mapping = sc2b_mapping
        self.tpr = true_pair_rate

    def __len__(self):
        return max(len(self.sc_expr), len(self.bulk_expr))

    def __getitem__(self, idx):
        idx_sc = idx
        idx_bulk = idx%len(self.bulk_expr)

        # if possible use a matching ccl with probability self.tpr
        to_pair = np.random.choice([0, 1], p=[1-self.tpr, self.tpr])
        if to_pair == 1 and self.sc2b_mapping[idx_sc] >= 0:
            idx_bulk = self.sc2b_mapping[idx_sc]

        if self.sc2b_mapping[idx_sc] == idx_bulk:
            paired = 1
        else:
            paired = -1

        return [self.bulk_expr[idx_bulk],
                self.sc_expr[idx_sc],
                paired]

class MixedValDataset(MixedDataset):
    def __init__(self, bulk_expr, sc_expr, sc2b_mapping):
        super(MixedValDataset, self).__init__(bulk_expr, sc_expr, sc2b_mapping)

    def __getitem__(self, idx):
        idx_sc = idx
        idx_bulk = idx%len(self.bulk_expr)

        # remove element of randomness
        if idx_sc%2 == 1 and self.sc2b_mapping[idx_sc] >= 0:
            idx_bulk = self.sc2b_mapping[idx_sc]
            #print(idx_bulk)

        if self.sc2b_mapping[idx_sc] == idx_bulk:
            paired = 1
        else:
            paired = -1
        #print("here desu yo")

        return [self.bulk_expr[idx_bulk],
                self.sc_expr[idx_sc],
                paired]

def fold_validation(hyperparams, seed, train_data, val_data, folder, tuning, epoch):
    reset_seed(seed)
    train_loader = DataLoader(train_data, batch_size=hyperparams['batch_size'], shuffle=True, drop_last=True)
    val_loader = DataLoader(val_data, batch_size=hyperparams['batch_size'], shuffle=False)

    n_genes = hyperparams['n_genes']
    model = Model(n_genes, hyperparams)
    trainer = Trainer(model, hyperparams)

    val_error, metric_names = trainer.fit(
            folder=folder,
            num_epoch=epoch, 
            train_loader=train_loader, 
            val_loader=val_loader,
            tuning=tuning)
    
    return val_error, trainer, metric_names

def create_mapper(bulk_samples, sc_samples, sc_mapping):
    idx = pd.Series(range(len(bulk_samples)), index=bulk_samples)
    sc2b = np.zeros(len(sc_samples), dtype=int)
    for i, sc in enumerate(sc_samples):
        if sc_mapping[sc] not in bulk_samples:
            sc2b[i] = -1
        else:
            sc2b[i] = idx[sc_mapping[sc]]
    return sc2b

def create_dataset(folds, cell_lines, train_folds, val_fold, normalizer, sc_expr, sc_mapping, hidden_fold=None):

    if hidden_fold is None:
        hidden_fold = -100 # just make sure this does not exist as a fold

    #bulk
    train_samples = list(folds.loc[folds['fold'].isin(train_folds)]['cell_line'].unique())
    val_samples = list(folds.loc[folds['fold'] == val_fold]['cell_line'].unique())
    hidden_samples = list(folds.loc[folds['fold'] == hidden_fold]['cell_line'].unique()) # test fold

    # anything that is not validation, test, or (labeled) training data is an "extra sample"
    extra_samples = list(cell_lines.loc[~cell_lines.index.isin(train_samples + val_samples + hidden_samples)].index)

    #sc
    sc_train_samples = list(sc_mapping[sc_mapping.isin(train_samples)].index) # paired
    sc_val_samples = list(sc_mapping[sc_mapping.isin(val_samples)].index) # paired
    sc_hidden_samples = list(sc_mapping[sc_mapping.isin(hidden_samples)].index) # test_fold

    # anything that is not validation, test, or (labeled) training data is an "extra sample"
    sc_extra_samples = list(sc_expr.loc[~sc_expr.index.isin(sc_train_samples + sc_val_samples + sc_hidden_samples)].index)


    print("Train samples: %d (bulk), %d (SC)"%(len(train_samples), len(sc_train_samples)))
    print("Val samples: %d (bulk), %d (SC)"%(len(val_samples), len(sc_val_samples)))
    print("Extra samples: %d (bulk), %d (SC)"%(len(extra_samples), len(sc_extra_samples)))

    train_x = cell_lines.loc[train_samples].values
    val_x = cell_lines.loc[val_samples].values
    extra_x = cell_lines.loc[extra_samples].values

    sc_train_x = sc_expr.loc[sc_train_samples].values
    sc_val_x = sc_expr.loc[sc_val_samples].values
    sc_extra_x = sc_expr.loc[sc_extra_samples].values

    # normalize
    train_x, val_x = normalizer(train_x, val_x)
    _, extra_x = normalizer(train_x, extra_x) # we only normalize using the train set (fairness for non-VAE models)
    train_x = np.concatenate([train_x, extra_x], axis=0)

    sc_train_x, sc_val_x = normalizer(sc_train_x, sc_val_x)
    _, sc_extra_x = normalizer(sc_train_x, sc_extra_x) # we only normalize using the train set (fairness for non-VAE models)
    sc_train_x = np.concatenate([sc_train_x, sc_extra_x], axis=0)

    # mapper
    train_sc2b = create_mapper(train_samples+extra_samples, sc_train_samples+sc_extra_samples, sc_mapping)
    val_sc2b = create_mapper(val_samples, sc_val_samples, sc_mapping)

    train_data = MixedDataset(train_x, sc_train_x, train_sc2b, true_pair_rate=0.50) # train on all SC
    val_data = MixedValDataset(val_x, sc_val_x, val_sc2b) # evaluate on all SC

    return train_data, val_data

def nested_cross_validation(FLAGS, cell_lines, folds, normalizer, sc_expr, sc_mapping):
    reset_seed(FLAGS.seed)
    hyperparams = {
        'n_genes': cell_lines.shape[1],
        'learning_rate': 1e-4,
        'num_epoch': 200,
        'batch_size': 128,
        'gene_l1': 2048,
        'gene_l2': 1024,
        'gene_l3': 512,
        'drug_l1': 512,
        'mid': 512,
        'drop': 1,
        'binary': FLAGS.response_type=='binary'}

    directory = FLAGS.outroot + "/results/" + FLAGS.folder
    # label_mask = create_fold_mask(labels)

    final_metrics = None

    for i in range(5):
        print('==%d=='%i)
        test_fold = i 
        val_fold = (i+1)%5
        train_folds = [x for x in range(5) if (x != test_fold) and (x != val_fold)]

        hp = hyperparams.copy()

        # === Train-Val (revert to best val) ===

        train_data, val_data = create_dataset(
            folds, cell_lines, train_folds, val_fold, 
            normalizer, sc_expr, sc_mapping, hidden_fold=test_fold)
        
        val_error, trainer, metric_names = fold_validation(hp, seed=FLAGS.seed, 
            train_data=train_data, 
            val_data=val_data, 
            folder=directory, tuning=True, 
            epoch=hp['num_epoch'])#, final=False)

        # save logs
        test_metrics = pd.DataFrame(val_error, columns=metric_names)
        test_metrics.to_csv(directory + '/vae_fold_%d.csv'%i, index=False)

        # save model
        trainer.model.save_model(directory, i)

        # test model
        _, test_data = create_dataset(
            folds, cell_lines, train_folds, test_fold, 
            normalizer, sc_expr, sc_mapping)

        train_data = DataLoader(train_data, batch_size=hyperparams['batch_size'], shuffle=False)
        train_metrics = trainer.validation_step(train_data, trainer.device)

        test_data = DataLoader(test_data, batch_size=hyperparams['batch_size'], shuffle=False)
        test_metrics = trainer.validation_step(test_data, trainer.device)

        if i == 0:
            final_metrics = np.zeros((5, 15))

        final_metrics[i,:5] = val_error[-1, :5]
        final_metrics[i,5:10] = train_metrics
        final_metrics[i,10:] = test_metrics
    

    metric_names += ['test KLD', 'test bulk_recons', 'test sc_recons', 'test cos', 'test loss']
    final_metrics = pd.DataFrame(final_metrics, columns=metric_names)

    final_metrics.to_csv(directory + '/final_metrics.csv')
    return final_metrics

def main(FLAGS):

    gene_list = '../sc-data/scDEAL/scBiG/kinker/genes.txt'
    genes = [line.strip() for line in open(gene_list)]

    cell_lines, folds, normalizer = initialize_ccl(FLAGS, exclude_missing=False)
    sc_expr, sc_mapping, _ = initialize_singlecell(FLAGS, exclude_no_mapping=False)

    # paired data
    sc_with_bulk = sc_mapping[sc_mapping.isin(cell_lines.index)].index
    bulk_with_sc = list(sc_mapping[sc_with_bulk].unique())
    paired_folds = folds.loc[folds['cell_line'].isin(bulk_with_sc)]

    print('Paired cell lines per fold:')
    print(folds.groupby('fold').size())

    # unpaired data
    sc_without_bulk = sc_mapping[~sc_mapping.index.isin(sc_with_bulk)]
    
    # filter genes
    cell_lines = cell_lines[genes]
    sc_expr = sc_expr[genes]


    n = len(set(bulk_with_sc) - set(folds['cell_line'].index))
    print("Extra paired samples (no DR): %d"%(n))
    n = len(set(cell_lines.index) - set(folds['cell_line'].index))
    print("Extra unpaired bulk samples (no DR): %d"%(n))
    n = len(sc_with_bulk)
    print("Extra unpaired cells (no bulk):%d"%n)


    test_metrics = nested_cross_validation(FLAGS, cell_lines, folds, normalizer, sc_expr, sc_mapping)
    test_metrics = test_metrics.mean(axis=0)

    print("Overall Performance")
    print(test_metrics)
