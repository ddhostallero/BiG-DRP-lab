import torch
import time
import numpy as np
from scipy.stats import pearsonr, spearmanr
from collections import deque
import json
import os
from copy import deepcopy


class Trainer():
    def __init__(self, model, hyp):

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.model = model.to(self.device)
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=hyp['learning_rate'])
        self.recon_criterion = torch.nn.MSELoss()
        self.latent_similarity_criterion = torch.nn.CosineEmbeddingLoss(margin=0.2)
        self.scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            self.optimizer, patience=5, min_lr=1e-7, factor=0.5) # if 1 epoch no improvement, reduce

    def loss(self, mu, logvar, z_bulk, x_bulk_hat, z_sc, x_sc_hat, x_bulk, x_sc, paired):
        kld_loss = -0.5 * torch.mean(1 + logvar - mu.pow(2) - logvar.exp())
        bulk_recons_loss = self.recon_criterion(x_bulk_hat, x_bulk)
        sc_recons_loss = self.recon_criterion(x_sc_hat, x_sc)
        #print(z_bulk.shape, z_sc.shape, paired.shape)
        cosine_loss = self.latent_similarity_criterion(z_bulk, z_sc, paired)
        loss = kld_loss + bulk_recons_loss + sc_recons_loss + cosine_loss

        return kld_loss, bulk_recons_loss, sc_recons_loss, cosine_loss, loss

    def train_step(self, train_loader, device):
        self.model.train()
        for (bulk, sc, paired) in train_loader:
            # sc and bulk are randomly sampled, paired or unpaired
            bulk, sc, paired = bulk.to(device), sc.to(device), paired.to(device)
            self.optimizer.zero_grad()
            mu, logvar, z_bulk, x_bulk_hat, z_sc, x_sc_hat = self.model(bulk, sc)
            #outs = self.model(bulk, sc)
            ret_val = self.loss(mu, logvar, z_bulk, x_bulk_hat, z_sc, x_sc_hat, bulk, sc, paired)
            loss = ret_val[-1]
            loss.backward()
            self.optimizer.step()

        ret_val = [x.item() for x in ret_val]
        return ret_val

    def validation_step(self, val_loader, device):
        self.model.eval()

        _loss = 0
        _kld_loss = 0 
        _bulk_recons_loss = 0
        _sc_recons_loss = 0
        _cosine_loss = 0

        with torch.no_grad():
            for (bulk, sc, paired) in val_loader:
                batch_size = bulk.shape[0]

                bulk, sc, paired = bulk.to(device), sc.to(device), paired.to(device)
                mu, logvar, z_bulk, x_bulk_hat, z_sc, x_sc_hat = self.model(bulk, sc)
                kld_loss, bulk_recons_loss, sc_recons_loss, cosine_loss, loss =\
                    self.loss(mu, logvar, z_bulk, x_bulk_hat, z_sc, x_sc_hat, bulk, sc, paired)

                _loss += loss*batch_size
                _kld_loss += kld_loss*batch_size
                _bulk_recons_loss += bulk_recons_loss*batch_size
                _sc_recons_loss += sc_recons_loss*batch_size
                _cosine_loss += cosine_loss*batch_size

        _loss /= len(val_loader.dataset)
        _kld_loss /= len(val_loader.dataset)
        _bulk_recons_loss /= len(val_loader.dataset)
        _sc_recons_loss /= len(val_loader.dataset)
        _cosine_loss /= len(val_loader.dataset)

        return _kld_loss.item(), _bulk_recons_loss.item(), _sc_recons_loss.item(), _cosine_loss.item(), _loss.item()



    def fit(self, folder, num_epoch, train_loader, val_loader, tuning=False):
        start_time = time.time()

        ret_matrix = np.zeros((num_epoch, 10))
        metric_names = ['val KLD', 'val bulk_recons', 'val sc_recons', 'val cos', 'val loss',
                        'train KLD', 'train bulk_recons', 'train sc_recons', 'train cos','train loss',]

        best_model_state = None
        best_loss = np.inf
        count = 0

        for epoch in range(num_epoch):
            train_metrics = self.train_step(train_loader, self.device)
            val_metrics = self.validation_step(val_loader, self.device)
            loss = val_metrics[2]
            self.scheduler.step(loss)

            ret_matrix[epoch] = list(val_metrics) + list(train_metrics)

            elapsed_time = time.time() - start_time
            start_time = time.time()
            print("%d\t%s:%.4f\t%s:%.4f\t%s:%.4f\t%s:%.4f\t%s:%.4f\t%s:%.4f\t%ds"%(
                epoch+1, 
                metric_names[0], val_metrics[0], 
                metric_names[1], val_metrics[1],  
                metric_names[2], val_metrics[2], 
                metric_names[3], val_metrics[3],
                metric_names[4], val_metrics[4], 
                metric_names[9], train_metrics[4], int(elapsed_time)))

            if best_loss > loss:
                best_loss = loss
                count = 0
            else:
                count += 1

            if tuning:

                if count == 0: # Checkpoint if the model is as its "best" yet
                    best_model_state = deepcopy(self.model.state_dict())

                elif count == 15: # use this if we are not using raytune
                    ret_matrix = ret_matrix[:epoch+1]
                    self.model.load_state_dict(best_model_state)
                    break

        return ret_matrix, metric_names
