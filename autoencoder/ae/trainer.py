import torch
import time
import numpy as np
from scipy.stats import pearsonr, spearmanr
from collections import deque
import json
import os
from copy import deepcopy
from utils.utils import rowwise_scc

class Trainer():
    def __init__(self, model, hyp):

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.model = model.to(self.device)
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=hyp['learning_rate'])
        self.recon_criterion = torch.nn.MSELoss()
        self.scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            self.optimizer, patience=10, min_lr=1e-7, factor=0.5) # if 1 epoch no improvement, reduce

    def train_step(self, train_loader, device):
        self.model.train()
        for (expr,) in train_loader:
            expr = expr.to(device)

            self.optimizer.zero_grad()
            x_hat = self.model(expr)
            loss = self.recon_criterion(x_hat, expr)
            loss.backward()
            self.optimizer.step()

        scc = rowwise_scc(x_hat.cpu().detach().numpy(), expr.cpu().detach().numpy())

        return [loss.item(), scc.mean()]

    def validation_step(self, val_loader, device):
        self.model.eval()

        _loss = 0
        _scc = 0

        with torch.no_grad():
            for (expr,) in val_loader:
                expr = expr.to(device)
                x_hat = self.model(expr)
                loss = self.recon_criterion(x_hat, expr)
                _loss += loss*expr.shape[0]

                scc = rowwise_scc(x_hat.cpu().detach().numpy(), expr.cpu().detach().numpy())
                _scc += scc.sum()

        _loss /= len(val_loader.dataset)
        _scc /= len(val_loader.dataset)
        return [_loss.item(), _scc]


    def fit(self, folder, num_epoch, train_loader, val_loader, tuning=False):
        start_time = time.time()

        ret_matrix = np.zeros((num_epoch, 4))
        metric_names = ['val loss', 'val scc', 'train loss', 'train scc']

        best_epoch = num_epoch
        best_model_state = None
        best_loss = np.inf
        count = 0

        for epoch in range(num_epoch):
            train_metrics = self.train_step(train_loader, self.device)
            val_metrics = self.validation_step(val_loader, self.device)
            loss = val_metrics[0]
            self.scheduler.step(loss)

            ret_matrix[epoch] = list(val_metrics) + list(train_metrics)

            elapsed_time = time.time() - start_time
            start_time = time.time()
            print("%d\t%s:%.4f\t%s:%.4f\t%s:%.4f\t%s:%.4f\t%ds"%(
                epoch+1, 
                metric_names[0], val_metrics[0], 
                metric_names[1], val_metrics[1], 
                metric_names[2], train_metrics[0],
                metric_names[3], train_metrics[1], int(elapsed_time)))

            if best_loss > loss:
                best_loss = loss
                best_epoch = epoch+1
                count = 0
            else:
                count += 1

            if tuning:

                if count == 0: # Checkpoint if the model is as its "best" yet
                    # torch.save(self.model.state_dict(), folder+'/tmp_best_weights')
                    best_model_state = deepcopy(self.model.state_dict())

                elif count == 15: # use this if we are not using raytune
                    ret_matrix = ret_matrix[:epoch+1]
                    # self.model.load_state_dict(torch.load(folder+'/tmp_best_weights'), strict=True)
                    # self.model = self.model.to(self.device)
                    self.model.load_state_dict(best_model_state)
                    break

        return ret_matrix, metric_names, best_epoch