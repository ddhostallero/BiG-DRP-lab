import torch
import torch.nn as nn
import torch.nn.functional as F
import json


class AE(nn.Module):
  def __init__(self, in_dim, hyp):
    super(AE, self).__init__()

    self.hyp = hyp
    self.encoder = Encoder(in_dim, hyp)
    self.decoder = Decoder(hyp, in_dim)

  def forward(self, x):
    z = self.encoder(x)
    x_hat = self.decoder(z)
    return x_hat

  def encode(self, x, reparametrize=None):
    return self.encoder(x)

  def save_model(self, directory, fold_id):
        torch.save(self.state_dict(), directory+'/model_weights_fold_%d'%fold_id)

        x = json.dumps(self.hyp)
        f = open(directory+"/model_config_fold_%d.txt"%fold_id,"w")
        f.write(x)
        f.close()

class Encoder(nn.Module):
  def __init__(self, in_dim, hyp):
    super(Encoder, self).__init__()

    self.layer1 = nn.Linear(in_dim, hyp['gene_l1'])
    self.layer2 = nn.Linear(hyp['gene_l1'], hyp['gene_l2'])
    self.layer3 = nn.Linear(hyp['gene_l2'], hyp['gene_l3'])
    # self.dropout = nn.Dropout(0.2)

  def forward(self, x):
    x = F.leaky_relu(self.layer1(x))
    # x = self.dropout(x)

    x = F.leaky_relu(self.layer2(x))
    # x = self.dropout(x)

    z = self.layer3(x) # no activation    
    return z

class Decoder(nn.Module):
  def __init__(self, hyp, out_dim):
    super(Decoder, self).__init__()

    self.layer1 = nn.Linear(hyp['gene_l3'], hyp['gene_l2'])
    self.layer2 = nn.Linear(hyp['gene_l2'], hyp['gene_l1'])
    self.layer3 = nn.Linear(hyp['gene_l1'], out_dim)
    # self.dropout = nn.Dropout(0.2)

  def forward(self, x):
    x = F.leaky_relu(self.layer1(x))
    # x = self.dropout(x)

    x = F.leaky_relu(self.layer2(x))    
    # x = self.dropout(x)

    x = self.layer3(x)
    return x