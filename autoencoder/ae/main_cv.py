from utils.tuple_dataset import TupleMapDatasetV2
from utils.utils import mkdir, reindex_tuples, reset_seed, create_fold_mask
from utils.data_initializer import initialize_ccl, initialize_singlecell_folds


import torch
import pickle
from torch.utils.data import DataLoader, TensorDataset
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler


def fold_validation(hyperparams, seed, train_data, val_data, folder, tuning, epoch):
    reset_seed(seed)
    train_loader = DataLoader(train_data, batch_size=hyperparams['batch_size'], shuffle=True, drop_last=True)
    val_loader = DataLoader(val_data, batch_size=hyperparams['batch_size'], shuffle=False)

    n_genes = hyperparams['n_genes']
    if hyperparams['enc_type'] == 'ae':
        from autoencoder.ae.model import AE as Model
        from autoencoder.ae.trainer import Trainer
    elif hyperparams['enc_type'] == 'vae':
        from autoencoder.vae.model import VAE as Model
        from autoencoder.vae.trainer import Trainer

    model = Model(n_genes, hyperparams)
    trainer = Trainer(model, hyperparams)

    val_error, metric_names, best_epoch = trainer.fit(
            folder=folder,
            num_epoch=epoch, 
            train_loader=train_loader, 
            val_loader=val_loader,
            tuning=tuning)
    
    return val_error, trainer, metric_names, best_epoch

def create_dataset_bulk(folds, cell_lines, train_folds, val_fold, normalizer):    

    train_samples = list(folds.loc[folds['fold'].isin(train_folds)]['cell_line'].unique())
    val_samples = list(folds.loc[folds['fold'] == val_fold]['cell_line'].unique())
    extra_samples = list(cell_lines.loc[~cell_lines.index.isin(folds['cell_line'].unique())].index)

    train_x = cell_lines.loc[train_samples].values
    val_x = cell_lines.loc[val_samples].values
    extra_x = cell_lines.loc[extra_samples].values

    normalizer = StandardScaler()
    train_x = normalizer.fit_transform(train_x)
    val_x = normalizer.transform(val_x)
    extra_x = normalizer.transform(extra_x)

    train_x = np.concatenate([train_x, extra_x], axis=0)
    train_data = TensorDataset(torch.FloatTensor(train_x))
    val_data = TensorDataset(torch.FloatTensor(val_x))

    return train_data, val_data, train_samples, val_samples, extra_samples, normalizer

def create_dataset_sc(folds, expr, split, current_fold, normalizer, mode='val'):
    suffix = "_bulk_%s_%d"%(split, current_fold)
    train_samples = folds.loc[folds['train'+suffix]==1].index
    val_samples = folds.loc[folds[mode+suffix]==1].index

    train_x = expr.loc[train_samples].values
    val_x = expr.loc[val_samples].values

    normalizer = StandardScaler()
    train_x = normalizer.fit_transform(train_x)
    val_x = normalizer.transform(val_x)

    train_data = TensorDataset(torch.FloatTensor(train_x))
    val_data = TensorDataset(torch.FloatTensor(val_x))

    return train_data, val_data, train_samples, val_samples, normalizer

def single_cross_validation(FLAGS, expr, folds):
    reset_seed(FLAGS.seed)
    hyperparams = {
        'enc_type': FLAGS.algo.split('.')[0],
        'n_genes': expr.shape[1],
        'learning_rate': 1e-4,
        'num_epoch': 1000,
        'batch_size': 128,
        'gene_l1': 4092,
        'gene_l2': 2048,
        'gene_l3': 1024}

    if 'pca' in FLAGS.algo:
        hyperparams['gene_l1'] = 64
        hyperparams['gene_l2'] = 32
        hyperparams['gene_l3'] = 16

    directory = FLAGS.outroot + "/results/" + FLAGS.folder
    final_metrics = None

    for i in range(5):
        print('==%d=='%i)
        test_fold = i 
        val_fold = (i+1)%5
        train_folds = [x for x in range(5) if (x != test_fold) and (x != val_fold)]

        hp = hyperparams.copy()

        # === Train-Val (revert to best val) ===

        if 'b2b' in FLAGS.algo:
            train_data, val_data, _, _, _, normalizer = create_dataset_bulk(folds, expr, train_folds, val_fold, normalizer=None)
        elif 'sc2sc' in FLAGS.algo:
            train_data, val_data, _, _, normalizer = create_dataset_sc(folds, expr, FLAGS.split, i, normalizer=None, mode='val')
        
        val_error, trainer, metric_names, best_epoch = fold_validation(hp, seed=FLAGS.seed, 
            train_data=train_data, 
            val_data=val_data, 
            folder=directory, tuning=True, 
            epoch=hp['num_epoch'])

        # save logs
        test_metrics = pd.DataFrame(val_error, columns=metric_names)
        test_metrics.to_csv(directory + '/fold_%d.csv'%i, index=False)

        # save model
        hyperparams['best_epoch'] = best_epoch
        trainer.model.hyp = hyperparams
        trainer.model.save_model(directory, i)
        pickle.dump(normalizer, open(directory + "/normalizer_fold_%d.pkl"%i, "wb"))

        # test model
        if 'b2b' in FLAGS.algo:
            _, test_data, _, _, _, _ = create_dataset_bulk(folds, expr, train_folds, test_fold, normalizer)
        elif 'sc2sc' in FLAGS.algo:
            _, test_data, _, _, _ = create_dataset_sc(folds, expr, FLAGS.split, i, normalizer, mode='test')

        train_data = DataLoader(train_data, batch_size=hyperparams['batch_size'], shuffle=False)
        train_metrics = trainer.validation_step(train_data, trainer.device)

        test_data = DataLoader(test_data, batch_size=hyperparams['batch_size'], shuffle=False)
        test_metrics = trainer.validation_step(test_data, trainer.device)

        if i == 0:
            n_metrics = len(metric_names)//2
            final_metrics = np.zeros((5, 3*n_metrics))

        final_metrics[i,:n_metrics] = val_error[-1, :n_metrics]
        final_metrics[i,n_metrics:2*n_metrics] = train_metrics
        final_metrics[i,2*n_metrics:] = test_metrics
        trainer = None
    
    metric_names += ["test " + m.split(" ")[1] for m in metric_names[:n_metrics]]
    final_metrics = pd.DataFrame(final_metrics, columns=metric_names)

    final_metrics.to_csv(directory + '/final_metrics.csv')
    return final_metrics

def main(FLAGS):

    if 'b2b' in FLAGS.algo:
        expr, folds, _ = initialize_ccl(FLAGS)
    elif 'sc2sc' in FLAGS.algo:
        expr, folds, _ = initialize_singlecell_folds(FLAGS)

    if 'pca' in FLAGS.algo:
        from sklearn.decomposition import PCA 
        pca = PCA(n_components=50)
        temp = pca.fit_transform(expr.values)
        expr = pd.DataFrame(temp, index=expr.index)

    test_metrics = single_cross_validation(FLAGS, expr, folds)
    test_metrics = test_metrics.mean(axis=0)

    print("Overall Performance")
    print(test_metrics)
