import torch
from bigdrp_rel.model import BiGDRPRel
import time
import numpy as np
from scipy.stats import pearsonr, spearmanr
from dgl.dataloading import MultiLayerFullNeighborSampler
import torch.nn.functional as F
from base.trainer import BaseTrainer

class Trainer(BaseTrainer):
    def __init__(self, n_genes, cell_feats, drug_feats, network, hyp, test=False, load_model_path=None):
        super(Trainer, self).__init__(hyp)

        self.cell_feats = cell_feats
        self.drug_feats = drug_feats
        self.network = network

        if load_model_path is not None:
            self.model = ModelHead(n_genes, hyp)
            self.model.load_state_dict(torch.load(load_model_path), strict=False)
            self.model = self.model.to(self.device)

        if not test:
            self.model = BiGDRPRel(n_genes, self.cell_feats.shape[1], drug_feats.shape[1], hyp).to(self.device)
            params = self.model.parameters()
            self.optimizer = torch.optim.Adam(params, lr=hyp['learning_rate'])

            
            self.network.ndata['global_index'] = torch.arange(network.num_nodes())
            # local_index = [i for i in range(network.num_nodes) if i < len(drug_feats) else i - len(drug_feats)]
            # self.network.ndata['local_index'] = torch.LongTensor(local_index)

            graph_sampler = MultiLayerFullNeighborSampler(2)
            _,_, blocks = graph_sampler.sample_blocks(self.network, range(len(drug_feats)))
            # self.blocks = [b.to(self.device) for b in blocks]
           
            # make sure they are aligned correctly
            idx = blocks[0].ndata['global_index']['_N']
            # print(idx)
            ccl_idx = idx[len(drug_feats):] - len(drug_feats)
            drug_idx = idx[:len(drug_feats)]

            if max(drug_idx) >= len(drug_feats):
                print("Assumption that first %d nodes are drugs is not satisfied")
                exit()

            self.cell_feats = self.cell_feats[ccl_idx].to(self.device)
            self.drug_feats = self.drug_feats[drug_idx].to(self.device)
            self.blocks = [b.to(self.device) for b in blocks]


    def train_step(self, train_loader, device):
        # trains on tuples
        self.model.train()
        for (x, d1, y, w) in train_loader:
            x, d1, y = x.to(device), d1.to(device), y.to(device)
            w = w.to(device)

            self.optimizer.zero_grad()
            pred = self.model(self.blocks, self.drug_feats, self.cell_feats, x, d1)
            loss = self.loss(pred, y)

            loss.backward()
            self.optimizer.step()

        return [loss.item()]

    def validation_step(self, val_loader, device):
        """
        Creates a matrix of predictions with shape (n_cell_lines, n_drugs) and calculates the metrics
        """

        self.model.eval()

        preds = []
        ys = []
        with torch.no_grad():
            for (x, y, mask) in val_loader:
                x, y, mask = x.to(device), y.to(device), mask.to(device)
                pred = self.model.predict_all(self.blocks, self.drug_feats, self.cell_feats, x)

                mask = mask.cpu().detach().numpy().nonzero()
                ys.append(y.cpu().detach().numpy()[mask])
                preds.append(pred.cpu().detach().numpy()[mask])

        preds = np.concatenate(preds, axis=0)
        ys = np.concatenate(ys, axis=0)
        val_loss = self.val_loss(torch.Tensor(preds), torch.Tensor(ys))
        met1, met2 = self.metrics_callback(ys, preds)

        return val_loss.item()/len(ys), met1, met2

    def get_drug_encoding(self):
        """
        returns the tensor of drug encodings (by GraphConv])
        """
        self.model.eval()
        with torch.no_grad():
            drug_encoding = self.model.get_drug_encoding(self.blocks, self.drug_feats, self.cell_feats)
        return drug_encoding

    def predict_matrix(self, data_loader, drug_encoding=None):
        """
        returns a prediction matrix of (N, n_drugs)
        """

        self.model.eval()

        preds = []
        if drug_encoding is None:
            drug_encoding = self.get_drug_encoding() # get the encoding first so that we don't have top run the conv every time
        else:
            drug_encoding = drug_encoding.to(self.device)

        with torch.no_grad():
            for (x,) in data_loader:
                x = x.to(self.device)
                pred = self.model.predict_response_matrix(x, drug_encoding)
                preds.append(pred)

        preds = torch.cat(preds, axis=0).cpu().detach().numpy()
        return preds
