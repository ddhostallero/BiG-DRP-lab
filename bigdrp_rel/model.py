import torch
import torch.nn as nn
import torch.nn.functional as F
import dgl
from dgl import DGLGraph
from dgl.nn.pytorch import RelGraphConv

class DRPPlus(nn.Module):
    def __init__(self, n_genes, hyp):
        super(DRPPlus, self).__init__()

        self.expr_l1 = nn.Linear(n_genes, hyp['expr_enc'])
        self.mid = nn.Linear(hyp['expr_enc'] + hyp['common_dim'], hyp['mid'])
        
        if hyp['binary']:
            self.out = nn.Sequential(
                nn.Linear(hyp['mid'], 1),
                nn.Sigmoid()
            )
        else:
            self.out = nn.Linear(hyp['mid'], 1)
        
        if hyp['drop'] == 0:
            drop=[0,0]
        else:
            drop=[0.2,0.5]

        self.in_drop = nn.Dropout(drop[0])
        self.mid_drop = nn.Dropout(drop[1])
        self.alpha = 0.5

    def forward(self, cell_features, drug_enc):
        expr_enc = F.leaky_relu(self.expr_l1(cell_features))
        
        x = torch.cat([expr_enc,drug_enc],-1) # (batch, expr_enc_size+drugs_enc_size)
        x = self.in_drop(x)
        x = F.leaky_relu(self.mid(x)) # (batch, n_drugs, 1)
        x = self.mid_drop(x)
        out = self.out(x) # (batch, n_drugs, 1)
 
        return out
        
    def predict_response_matrix(self, cell_features, drug_enc):

        expr_enc = F.leaky_relu(self.expr_l1(cell_features))
        expr_enc = expr_enc.unsqueeze(1) # (batch, 1, expr_enc_size)
        drug_enc = drug_enc.unsqueeze(0) # (1, n_drugs, drug_enc_size)
        
        expr_enc = expr_enc.repeat(1,drug_enc.shape[1],1) # (batch, n_drugs, expr_enc_size)
        drug_enc = drug_enc.repeat(expr_enc.shape[0],1,1) # (batch, n_drugs, drug_enc_size)
        
        x = torch.cat([expr_enc,drug_enc],-1) # (batch, n_drugs, expr_enc_size+drugs_enc_size)
        x = self.in_drop(x)
        x = F.leaky_relu(self.mid(x)) # (batch, n_drugs, 1)
        x = self.mid_drop(x)
        out = self.out(x) # (batch, n_drugs, 1)
        out = out.view(-1, drug_enc.shape[1]) # (batch, n_drugs)
        return out

    def predict_matrix(self, data_loader, drug_encoding, device):
        self.eval()

        preds = []
        drug_encoding = drug_encoding.to(device)

        with torch.no_grad():
            for (x,) in data_loader:
                x = x.to(device)
                pred = self.predict_response_matrix(x, drug_encoding)
                preds.append(pred)

        preds = torch.cat(preds, axis=0).cpu().detach().numpy()
        return preds


class BiGDRPRel(nn.Module):
    def __init__(self, n_genes, n_cl_feats, n_drug_feats, hyp):
        super(BiGDRPRel, self).__init__()

        self.conv1 = RelGraphConv(in_feat=hyp['common_dim'], out_feat=hyp['common_dim'], 
            num_rels=2, self_loop=True, activation=F.leaky_relu)
        self.conv2 = RelGraphConv(in_feat=hyp['common_dim'], out_feat=hyp['common_dim'], 
            num_rels=2, self_loop=True, activation=F.leaky_relu)

        self.drug_l1 = nn.Linear(n_drug_feats, hyp['common_dim'])
        self.cell_l1 = nn.Linear(n_cl_feats, hyp['common_dim'])
        self.expr_l1 = nn.Linear(n_genes, hyp['expr_enc'])
        self.mid = nn.Linear(hyp['expr_enc'] + hyp['common_dim'], hyp['mid'])
        # self.out = nn.Linear(hyp['mid'], 1)

        if hyp['binary']:
            self.out = nn.Sequential(
                nn.Linear(hyp['mid'], 1),
                nn.Sigmoid()
            )
        else:
            self.out = nn.Linear(hyp['mid'], 1)

        if hyp['drop'] == 0:
            drop=[0,0]
        else:
            drop=[0.2,0.5]

        self.in_drop = nn.Dropout(drop[0])
        self.mid_drop = nn.Dropout(drop[1])
        self.alpha = 0.5
        

    def forward(self, blocks, drug_features, cell_features_in_network, cell_features, drug_index):
        # predict starting from the original features

        drug_enc = self.get_drug_encoding(blocks, drug_features, cell_features_in_network)        
        drug_enc = drug_enc[drug_index]
        out = self.predict_from_drug_encoding(cell_features, drug_enc)
        return out

    def get_drug_encoding(self, blocks, drug_features, cell_features_in_network):
        # graph convolutions

        drug_enc = F.leaky_relu(self.drug_l1(drug_features))
        cell_enc = F.leaky_relu(self.cell_l1(cell_features_in_network))

        node_features = torch.cat([drug_enc, cell_enc], axis=0) # assumes first n_drugs nodes in blocks[0] are drugs        
        h1 = self.conv1(blocks[0], node_features, blocks[0].edata['response_type'])
        # h1 = {k: F.leaky_relu(v + self.alpha*node_features[k]) for k, v in h1.items()}
        
        h2 = self.conv2(blocks[1], h1, blocks[1].edata['response_type'])
        # h2['drug'] = F.leaky_relu(h2['drug'] + self.alpha*h1['drug'])
        
        drug_enc = h2[:len(drug_features)]
        return drug_enc

    def predict_all(self, blocks, drug_features, cell_features_in_network, cell_features):
        # predict for all (cell,drug) pairs

        drug_enc = self.get_drug_encoding(blocks, drug_features, cell_features_in_network)
        out = self.predict_response_matrix(cell_features, drug_enc)
        return out

    def predict_response_matrix(self, cell_features, drug_enc):

        expr_enc = F.leaky_relu(self.expr_l1(cell_features))
        expr_enc = expr_enc.unsqueeze(1) # (batch, 1, expr_enc_size)
        drug_enc = drug_enc.unsqueeze(0) # (1, n_drugs, drug_enc_size)
        
        expr_enc = expr_enc.repeat(1,drug_enc.shape[1],1) # (batch, n_drugs, expr_enc_size)
        drug_enc = drug_enc.repeat(expr_enc.shape[0],1,1) # (batch, n_drugs, drug_enc_size)
        
        x = torch.cat([expr_enc,drug_enc],-1) # (batch, n_drugs, expr_enc_size+drugs_enc_size)
        x = self.in_drop(x)
        x = F.leaky_relu(self.mid(x)) # (batch, n_drugs, 1)
        x = self.mid_drop(x)
        out = self.out(x) # (batch, n_drugs, 1)
        out = out.view(-1, drug_enc.shape[1]) # (batch, n_drugs)
        return out

    def predict_from_drug_encoding(self, cell_features, drug_enc):

        expr_enc = F.leaky_relu(self.expr_l1(cell_features))
        x = torch.cat([expr_enc,drug_enc],-1) # (batch, expr_enc_size+drugs_enc_size)
        x = self.in_drop(x)
        x = F.leaky_relu(self.mid(x)) # (batch, n_drugs, 1)
        x = self.mid_drop(x)
        out = self.out(x) # (batch, n_drugs, 1)

        return out