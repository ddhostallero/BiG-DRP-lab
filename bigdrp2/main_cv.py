from utils.tuple_dataset import TupleMatrixDataset
from utils.utils import mkdir, reindex_tuples, moving_average, reset_seed, create_fold_mask
from utils.network_gen import create_network
from utils.data_initializer import initialize

import torch
from torch.utils.data import DataLoader, TensorDataset
from bigdrp2.trainer import Trainer
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
import pickle

def fold_validation(hyperparams, seed, network, train_data, val_data, cell_lines, 
    drug_feats, tuning, epoch, maxout, revert):

    r"""

    Description
    -----------
    Initializes a `Trainer` object then trains using given data and hyperparameters

    Parameters
    ----------
    hyperparams : dict
        Input feature size; i.e, the number of dimensions of :math:`h_j^{(l)}`.
    seed : int
        Output feature size; i.e., the number of dimensions of :math:`h_i^{(l+1)}`.
    network : pandas.DataFrame
        How to apply the normalizer. If is `'right'`, divide the aggregated messages

        where the :math:`c_{ji}` in the paper is applied.
    train_data : torch.DataSet
        If True, apply a linear layer. Otherwise, aggregating the messages
        without a weight matrix.
    val_data : torch.DataSet
        Contains
    cell_lines : torch.Tensor
        Tensor that contains the gene expressions. Must be in shape (n_cell_lines, n_genes)
    drug_feats : torch.Tensor
        Tensor that contains the drug features. Must be in shape (n_drugs, n_features)
    tuning : bool
        Set to true if we are using RayTune
    epoch : int
        Used as a maximum number of training epochs if ``final`` is False. 
        Otherwise, used as the fixed number of  trainingepochs.
    maxout : bool, optional
        If True, uses the ``epoch`` parameter as a fixed epoch instead of maximum epoch. 
        Otherwise, uses an early stopping criterion. Default: ``False``.
    """

    reset_seed(seed)
    train_loader = DataLoader(train_data, batch_size=hyperparams['batch_size'], shuffle=True, drop_last=True)
    val_loader = DataLoader(val_data, batch_size=hyperparams['batch_size'], shuffle=False)

    n_genes = cell_lines.shape[1]
    n_drug_feats = drug_feats.shape[1]

    trainer = Trainer(n_genes, cell_lines, drug_feats, network, hyperparams)
    val_error, metric_names, best_epoch = trainer.fit(
            num_epoch=epoch, 
            train_loader=train_loader, 
            val_loader=val_loader,
            tuning=tuning,
            maxout=maxout,
            revert=revert)
    return val_error, trainer, metric_names, best_epoch

def create_dataset(tuples, train_x, val_x, 
    train_y, val_y, train_mask, val_mask, drug_feats, percentile):

    r"""

        Description
        -----------
        Creates the training and validation/testing dataset and the training bipartite graph

        Parameters
        ----------
        tuples : pandas.DataFrame
            DataFrame containing the index of the drug index and the cell line index of the training pairs
        train_x : matrix
            Gene expressions of the training cell lines. 
        val_x : matrix
            Gene expressions of the validation cell lines
        train_y : matrix
            Training labels. Must be in shape (n_cell_lines, n_drugs)
        val_y : matrix
            Validation labels.Must be in shape (n_cell_lines, n_drugs)
        val_mask : matrix
            Matrix indicating that the (i,j)th cell in the ``val_y`` is part of the validation set
        drug_feats : matrix
            Drug features
        percentile :
            Percentile used in creating thresholds for the bipartite graph
    """

    network = create_network(tuples, percentile)

    train_data = TupleMatrixDataset( 
        tuples,
        torch.FloatTensor(train_x),
        torch.FloatTensor(train_y))

    val_data = TensorDataset(
        torch.FloatTensor(val_x),
        torch.FloatTensor(val_y),
        torch.FloatTensor(val_mask))

    cell_lines = torch.FloatTensor(train_x)
    drug_feats = torch.FloatTensor(drug_feats.values)

    return network, train_data, val_data, cell_lines, drug_feats

def test(trainer, test_x, test_mask, test_samples, drug_enc, drug_list):
    test_data = TensorDataset(torch.FloatTensor(test_x))
    test_data = DataLoader(test_data, batch_size=128, shuffle=False)

    prediction_matrix = trainer.predict_matrix(test_data, drug_encoding=torch.Tensor(drug_enc))
    prediction_matrix = pd.DataFrame(prediction_matrix, index=test_samples, columns=drug_list)

    # remove predictions for non-test data
    test_mask = test_mask.replace(0, np.nan)
    prediction_matrix = prediction_matrix*test_mask

    return prediction_matrix

def filter_folds(folds, labels, cell_lines, label_matrix, label_mask):
    tuples = labels.loc[labels['fold'].isin(folds)]
    samples = list(tuples['cell_line'].unique())
    x = cell_lines.loc[samples].values
    y = label_matrix.loc[samples].values
    mask = (label_mask.loc[samples].isin(folds))*1
    return tuples, samples, x, y, mask

def single_cross_validation(FLAGS, drug_feats, cell_lines, labels, label_matrix, hyperparams):
    reset_seed(FLAGS.seed)
    label_mask = create_fold_mask(labels, label_matrix)
    label_matrix = label_matrix.replace(np.nan, 0)

    final_metrics = None
    drug_list = list(drug_feats.index)

    for i in range(5):
        print('==%d=='%i)
        test_fold = i 
        val_fold = (i+1)%5
        train_folds = [x for x in range(5) if (x != test_fold) and (x != val_fold)]

        hp = hyperparams.copy()

        # === train and revert on best validation ===

        train_tuples, train_samples, train_x, train_y, train_mask =\
            filter_folds(train_folds, labels, cell_lines, label_matrix, label_mask)

        val_tuples, val_samples, val_x, val_y, val_mask =\
            filter_folds([val_fold], labels, cell_lines, label_matrix, label_mask)

        train_tuples = reindex_tuples(train_tuples, drug_list, train_samples) # all drugs exist in all folds
        ss = StandardScaler()
        train_x = ss.fit_transform(train_x)
        val_x = ss.transform(val_x)

        network, train_data, val_data, \
        cl_tensor, df_tensor = create_dataset(
            train_tuples, 
            train_x, val_x, 
            train_y, val_y, 
            train_mask, val_mask.values, 
            drug_feats, FLAGS.network_perc)

        val_error,trainer,metric_names,best_epoch = fold_validation(hp, FLAGS.seed, network, train_data, 
            val_data, cl_tensor, df_tensor, tuning=False, 
            epoch=hp['num_epoch'], maxout=False, revert=True)

        if i == 0:
            final_metrics = np.zeros((5, val_error.shape[1]))
        final_metrics[i] = val_error[-1]
        val_metrics = pd.DataFrame(val_error, columns=metric_names)
        val_metrics.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/fold_%d.csv'%i, index=False)
        hp['num_epoch'] = best_epoch
        trainer.save_model(FLAGS.outroot + "/results/" + FLAGS.folder, i, hp)
        pickle.dump(ss, open(FLAGS.outroot + "/results/" + FLAGS.folder + "/normalizer_fold_%d.pkl"%i, "wb"))

        drug_enc = trainer.get_drug_encoding().cpu().detach().numpy()
        pd.DataFrame(drug_enc, index=drug_list).to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/encoding_fold_%d.csv'%i)

        # test
        test_tuples, test_samples, test_x, test_y, test_mask =\
            filter_folds([test_fold], labels, cell_lines, label_matrix, label_mask)

        test_x = ss.transform(test_x)

        # save predictions
        prediction_matrix = test(trainer, test_x, test_mask, test_samples, drug_enc, drug_list)
        prediction_matrix.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/cv_prediction_testfold_%d.csv'%i)

        prediction_matrix = test(trainer, val_x, val_mask, val_samples, drug_enc, drug_list)
        prediction_matrix.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/cv_prediction_valfold_%d.csv'%i)
    
    final_metrics = pd.DataFrame(final_metrics, columns=metric_names)
    return final_metrics

def nested_cross_validation(FLAGS, drug_feats, cell_lines, labels, label_matrix, hyperparams):
    reset_seed(FLAGS.seed)

    label_mask = create_fold_mask(labels, label_matrix)
    label_matrix = label_matrix.replace(np.nan, 0)

    final_metrics = None
    drug_list = list(drug_feats.index)

    for i in range(5):
        print('==%d=='%i)
        test_fold = i 
        val_fold = (i+1)%5
        train_folds = [x for x in range(5) if (x != test_fold) and (x != val_fold)]

        hp = hyperparams.copy()

        # === find number of epochs ===

        train_tuples, train_samples, train_x, train_y, train_mask =\
            filter_folds(train_folds, labels, cell_lines, label_matrix, label_mask)

        val_tuples, val_samples, val_x, val_y, val_mask =\
            filter_folds([val_fold], labels, cell_lines, label_matrix, label_mask)

        train_tuples = reindex_tuples(train_tuples, drug_list, train_samples) # all drugs exist in all folds
        ss = StandardScaler()
        train_x = ss.fit_transform(train_x)
        val_x = ss.transform(val_x)

        network, train_data, val_data, \
        cl_tensor, df_tensor = create_dataset(
            train_tuples, 
            train_x, val_x, 
            train_y, val_y, 
            train_mask, val_mask.values,
            drug_feats, FLAGS.network_perc)

        val_error,_,_,best_epoch = fold_validation(hp, FLAGS.seed, network, train_data, 
            val_data, cl_tensor, df_tensor, tuning=False, 
            epoch=hp['num_epoch'], maxout=False, revert=False)
        hp['num_epoch'] = int(max(best_epoch, 2))

        # === actual test fold ===

        train_folds = train_folds + [val_fold]
        train_tuples, train_samples, train_x, train_y, train_mask =\
            filter_folds(train_folds, labels, cell_lines, label_matrix, label_mask)

        test_tuples, test_samples, test_x, test_y, test_mask =\
            filter_folds([test_fold], labels, cell_lines, label_matrix, label_mask)

        train_tuples = reindex_tuples(train_tuples, drug_list, train_samples) # all drugs exist in all folds
        ss = StandardScaler()
        train_x = ss.fit_transform(train_x)
        test_x = ss.transform(test_x)

        network, train_data, test_data, \
        cl_tensor, df_tensor = create_dataset(
            train_tuples, 
            train_x, test_x, 
            train_y, test_y, 
            train_mask, test_mask.values, drug_feats, FLAGS.network_perc)

        test_error, trainer, metric_names,_ = fold_validation(hp, FLAGS.seed, network, train_data, 
            test_data, cl_tensor, df_tensor, tuning=False, 
            epoch=hp['num_epoch'], maxout=True, revert=False) # set final so that the trainer uses all epochs

        if i == 0:
            final_metrics = np.zeros((5, test_error.shape[1]))

        final_metrics[i] = test_error[-1]
        test_metrics = pd.DataFrame(test_error, columns=metric_names)
        test_metrics.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/fold_%d.csv'%i, index=False)

        drug_enc = trainer.get_drug_encoding().cpu().detach().numpy()
        pd.DataFrame(drug_enc, index=drug_list).to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/encoding_fold_%d.csv'%i)

        trainer.save_model(FLAGS.outroot + "/results/" + FLAGS.folder, i, hp)
        pickle.dump(ss, open(FLAGS.outroot + "/results/" + FLAGS.folder + "/normalizer_fold_%d.pkl"%i, "wb"))

        # save predictions
        prediction_matrix = test(trainer, test_x, test_mask, test_samples, drug_enc, drug_list)
        prediction_matrix.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/cv_prediction_testfold_%d.csv'%i)
    
    final_metrics = pd.DataFrame(final_metrics, columns=metric_names)
    return final_metrics

def main(FLAGS):

    hyperparams = {
        'learning_rate': 1e-4,
        'num_epoch': 50,
        'batch_size': 128,
        'common_dim': 512,
        'expr_enc': 1024,
        'mid': 512,
        'drop': 1,
        'binary': FLAGS.response_type == 'binary'}

    drug_feats, cell_lines, labels, label_matrix, _ = initialize(FLAGS)

    if FLAGS.cv == 'double':
        test_metrics = nested_cross_validation(FLAGS, drug_feats, cell_lines, labels, label_matrix, hyperparams)
        print("Overall Performance (on Test)")
    elif FLAGS.cv == 'single':
        test_metrics = single_cross_validation(FLAGS, drug_feats, cell_lines, labels, label_matrix, hyperparams)
        print("Overall Performance (on Validation)")

    test_metrics = test_metrics.mean(axis=0)
    print(test_metrics.mean())