from utils.tuple_dataset import TupleIndexDataset
from utils.utils import mkdir, reindex_tuples_v2, reset_seed, create_fold_mask
from utils.network_gen import create_crispr_network, append_to_crispr_network
from utils.data_initializer import initialize, load_crispr

from dgl.dataloading import MultiLayerFullNeighborSampler
import torch
from torch.utils.data import DataLoader, TensorDataset
from crisp_resp_grl.trainer import Trainer
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler

def fold_validation(hyperparams, seed, network, val_network, 
    train_data, val_data, train_ccls, train_val_ccls,
    drug_feats, tuning, epoch, final=False):

    reset_seed(seed)
    train_loader = DataLoader(train_data, batch_size=hyperparams['batch_size'], shuffle=True, drop_last=True)
    val_loader = DataLoader(val_data, batch_size=hyperparams['batch_size'], shuffle=False)

    n_genes = train_ccls.shape[1]
    n_drug_feats = drug_feats.shape[1]

    trainer = Trainer(n_genes, train_ccls, drug_feats, network, hyperparams)

    print(network.num_nodes('cell_line'), val_network.num_nodes('cell_line'))
    print(drug_feats.shape, train_val_ccls.shape)

    graph_sampler = MultiLayerFullNeighborSampler(2)
    val_network.ndata['features'] = {
        'drug': drug_feats, 
        'cell_line': train_val_ccls,
        'gene': torch.arange(val_network.num_nodes('gene'))}

    _,_, blocks = graph_sampler.sample_blocks(
                val_network, {'drug': range(len(drug_feats)), 
                'cell_line': range(len(train_val_ccls))})

    val_error, metric_names, best_epoch = trainer.fit(
            blocks, 
            num_epoch=epoch, 
            train_loader=train_loader, 
            val_loader=val_loader,
            tuning=tuning)

    return val_error, trainer, metric_names, best_epoch

def create_dataset(tuples, val_tuples, train_x, val_x, 
    train_y, val_y, drug_feats, gene_ess, c_map, percentile):

    gene_ess = gene_ess.loc[gene_ess.index.isin(c_map.index)].copy()
    genes = gene_ess.columns
    gene_ess['new_index'] = gene_ess.index
    gene_ess.index = gene_ess['new_index'].replace(c_map) # replace with the new indexing
    gene_ess = gene_ess.loc[c_map.values, genes]

    network, crispr_genes = create_crispr_network(tuples, gene_ess, homogeneous=False, percentile=1, 
        ess_threshold=-1, min_ess_edge_per_ccl=1, min_ess_edge_per_gene=2)

    train_data = TupleIndexDataset( 
        tuples,
        torch.FloatTensor(train_y))

    val_only_ccls = set(val_tuples['cell_line'].unique()) - set(tuples['cell_line'].unique())
    val_ess = gene_ess.loc[gene_ess.index.isin(val_only_ccls), crispr_genes]
    val_network = append_to_crispr_network(network, val_ess, homogeneous=False,
        ess_threshold=-1, min_ess_edge_per_ccl=1, max_ess_edge_per_ccl=None)

    val_data = TupleIndexDataset(
        val_tuples,
        torch.FloatTensor(val_y))

    cell_lines = torch.FloatTensor(train_x)
    drug_feats = torch.FloatTensor(drug_feats.values)
    val_cell_lines = torch.FloatTensor(val_x)

    return network, val_network, train_data, val_data, cell_lines, val_cell_lines, drug_feats

def nested_cross_validation(FLAGS, drug_feats, cell_lines, labels,
    label_matrix, normalizer, gene_essentiality):
    reset_seed(FLAGS.seed)

    hyperparams = {
        'learning_rate': 1e-4,
        'num_epoch': 50,
        'batch_size': 128,
        'common_dim': 512,
        'expr_enc': 1024,
        'mid': 512,
        'drop': 1,
        'binary': FLAGS.response_type == 'binary',
        'sampler': FLAGS.sampler}

    label_mask = create_fold_mask(labels)
    label_matrix = label_matrix.replace(np.nan, 0)

    final_metrics = None
    drug_list = list(drug_feats.index)
    label_matrix = label_matrix[drug_list]

    for i in range(5):
        print('==%d=='%i)
        test_fold = i 
        val_fold = (i+1)%5
        train_folds = [x for x in range(5) if (x != test_fold) and (x != val_fold)]

        hp = hyperparams.copy()

        # === find number of epochs ===

        train_tuples = labels.loc[labels['fold'].isin(train_folds)]
        train_samples = list(train_tuples['cell_line'].unique())
        train_x = cell_lines.loc[train_samples].values
        train_y = label_matrix.loc[train_samples].values

        val_tuples = labels.loc[labels['fold'] == val_fold]
        val_samples = list(val_tuples['cell_line'].unique())
        # val_x = cell_lines.loc[val_samples].values
        val_y = label_matrix.loc[val_samples].values

        train_tuples,c_map,_ = reindex_tuples_v2(train_tuples, drug_list, train_samples) # all drugs exist in all folds
        val_tuples,c_map,_ = reindex_tuples_v2(val_tuples, drug_list, val_samples, c_map=c_map) # all drugs exist in all folds
        train_val_x = cell_lines.loc[c_map.index]

        normalizer = StandardScaler()
        train_x = normalizer.fit_transform(train_x)
        train_val_x = normalizer.transform(train_val_x)

        train_network, val_network, train_data, val_data, \
        train_ccls, train_val_ccls, df_tensor = create_dataset(
            train_tuples,
            val_tuples, 
            train_x, train_val_x, 
            train_y, val_y, drug_feats, 
            gene_essentiality.copy(), c_map, FLAGS.network_perc)

        val_error,_,_,best_epoch = fold_validation(hp, FLAGS.seed, train_network, val_network, train_data, 
            val_data, train_ccls, train_val_ccls, df_tensor, tuning=False, 
            epoch=hp['num_epoch'], final=False)

        hp['num_epoch'] = int(max(best_epoch, 2))

        # === actual test fold ===

        train_folds = train_folds + [val_fold]
        train_tuples = labels.loc[labels['fold'].isin(train_folds)]
        train_samples = list(train_tuples['cell_line'].unique())
        train_x = cell_lines.loc[train_samples].values
        train_y = label_matrix.loc[train_samples].values

        test_tuples = labels.loc[labels['fold'] == test_fold]
        test_samples = list(test_tuples['cell_line'].unique())
        # test_x = cell_lines.loc[test_samples].values
        test_y = label_matrix.loc[test_samples].values

        train_tuples,c_map,_ = reindex_tuples_v2(train_tuples, drug_list, train_samples) # all drugs exist in all folds
        test_tuples,c_map,_ = reindex_tuples_v2(test_tuples, drug_list, test_samples, c_map=c_map) # all drugs exist in all folds
        train_test_x = cell_lines.loc[c_map.index]

        normalizer = StandardScaler()
        train_x = normalizer.fit_transform(train_x)
        train_test_x = normalizer.transform(train_test_x)

        train_network, test_network, train_data, test_data, \
        train_ccls, train_test_ccls, df_tensor = create_dataset(
            train_tuples,
            test_tuples, 
            train_x, train_test_x, 
            train_y, test_y, drug_feats, 
            gene_essentiality.copy(), c_map, FLAGS.network_perc)

        test_error, trainer, metric_names, _ = fold_validation(hp, FLAGS.seed, train_network, test_network, train_data, 
            test_data, train_ccls, train_test_ccls, df_tensor, tuning=False, 
            epoch=hp['num_epoch'], final=True) # set final so that the trainer uses all epochs

        if i == 0:
            final_metrics = np.zeros((5, test_error.shape[1]))

        final_metrics[i] = test_error[-1]
        test_metrics = pd.DataFrame(test_error, columns=metric_names)
        test_metrics.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/fold_%d.csv'%i, index=False)

        # drug_enc = trainer.get_drug_encoding().cpu().detach().numpy()
        # pd.DataFrame(drug_enc, index=drug_list).to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/encoding_fold_%d.csv'%i)

        trainer.save_model(FLAGS.outroot + "/results/" + FLAGS.folder, i, hp)

        # save predictions
        # test_data = TensorDataset(torch.FloatTensor(test_x))
        # test_data = DataLoader(test_data, batch_size=hyperparams['batch_size'], shuffle=False)

        test_network.ndata['features'] = {
            'drug': df_tensor, 
            'cell_line': train_test_ccls,
            'gene': torch.ones(test_network.num_nodes('gene'),1)}

        prediction_matrix = trainer.predict_matrix(test_network, torch.tensor(c_map[test_samples].values, dtype=torch.long))
        prediction_matrix = pd.DataFrame(prediction_matrix, index=test_samples, columns=drug_list)

        # remove predictions for non-test data
        test_mask = ((label_mask.loc[test_samples]==test_fold)*1)[drug_list]
        test_mask = test_mask.replace(0, np.nan)
        prediction_matrix = prediction_matrix*test_mask

        prediction_matrix.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/cv_prediction_testfold_%d.csv'%i)
    
    return final_metrics

def main(FLAGS):

    drug_feats, cell_lines, labels, label_matrix, normalizer = initialize(FLAGS)
    gene_essentiality = load_crispr(FLAGS)
    test_metrics = nested_cross_validation(FLAGS, drug_feats, cell_lines, 
        labels, label_matrix, normalizer, gene_essentiality)
    test_metrics = test_metrics.mean(axis=0)