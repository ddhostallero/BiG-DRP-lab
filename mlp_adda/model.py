import torch
import torch.nn as nn
import torch.nn.functional as F
import json

class Discriminator(nn.Module):
    def __init__(self, n_in, hyp):
        super(Discriminator, self).__init__()
        self.disc_l1 = nn.Linear(n_in, hyp['disc_l1'])
        self.disc_l2 = nn.Linear(hyp['disc_l1'], hyp['disc_l2'])
        self.out = nn.Linear(hyp['disc_l2'], 1)

    def forward(self, x):
        x = F.leaky_relu(self.disc_l1(x))
        x = F.leaky_relu(self.disc_l2(x))
        x = self.out(x) # no sigmoid
        return x

class Encoder(nn.Module):
    def __init__(self, gene_expr_dim, hyp):
        super(Encoder, self).__init__()
        self.enc_l1 = nn.Linear(gene_expr_dim, hyp['gene_l1'])
        self.enc_l2 = nn.Linear(hyp['gene_l1'], hyp['gene_l2'])
        self.enc_l3 = nn.Linear(hyp['gene_l2'], hyp['gene_l3'])
        self.drop = nn.Dropout(0.2)
    
    def forward(self, x):
        x = F.leaky_relu(self.enc_l1(x))
        x = self.drop(x)
        x = F.leaky_relu(self.enc_l2(x))
        x = self.drop(x)
        x = torch.tanh(self.enc_l3(x))

        return x
    
    def encode_in_batches(self, dataloader, device='cuda'):
        self.eval()
        
        enc = []
        with torch.no_grad():
            for (x,) in dataloader:
                x = x.to(device)
                enc.append(self(x))

        enc = torch.cat(enc, axis=0).cpu().detach().numpy()
        return enc

class Predictor(nn.Module):
    def __init__(self, expr_enc_dim, drug_feat_dim, hyp):
        super(Predictor, self).__init__()
        self.drug_l1 = nn.Linear(drug_feat_dim, hyp['drug_l1'])

        self.pred_l1 = nn.Linear(expr_enc_dim+hyp['drug_l1'], hyp['pred_l1'])
        self.pred_l2 = nn.Linear(hyp['pred_l1'], hyp['pred_l2'])
        self.out = nn.Linear(hyp['pred_l2'], 1)

    def forward(self, expr_enc, drug):
        drug_enc = F.leaky_relu(self.drug_l1(drug))
        
        x = torch.cat([expr_enc, drug_enc], dim=1)
        x = F.leaky_relu(self.pred_l1(x))
        x = F.leaky_relu(self.pred_l2(x))
        x = self.out(x)

        return x

    def _predict_response_matrix(self, expr_enc, drug):
        drug_enc = F.leaky_relu(self.drug_l1(drug))
        
        expr_enc = expr_enc.unsqueeze(1) # (batch, 1, expr_enc_size)
        drug_enc = drug_enc.unsqueeze(0) # (1, n_drugs, drug_enc_size)

        expr_enc = expr_enc.repeat(1,drug_enc.shape[1],1) # (batch, n_drugs, expr_enc_size)
        drug_enc = drug_enc.repeat(expr_enc.shape[0],1,1) # (batch, n_drugs, drug_enc_size)
        
        x = torch.cat([expr_enc,drug_enc],-1) # (batch, n_drugs, expr_enc_size+drugs_enc_size)
        x = F.leaky_relu(self.pred_l1(x))
        x = F.leaky_relu(self.pred_l2(x))
        x = self.out(x)
        out = x.view(-1, drug_enc.shape[1]) # (batch, n_drugs)
        return out


class FullMLP(nn.Module):
    def __init__(self, gene_expr_dim, drug_feat_dim, hyp):
        super(FullMLP, self).__init__()
        self.hyp = hyp
        self.expr_encoder = Encoder(gene_expr_dim, hyp)
        self.predictor = Predictor(hyp['gene_l3'], drug_feat_dim, hyp)
    
    def forward(self, expr, drug):
        expr_enc = self.expr_encoder(expr)
        out = self.predictor(expr_enc, drug)
        return out

    def _predict_response_matrix(self, expr, drug_features):
        expr = self.expr_encoder(expr)
        return self.predictor._predict_response_matrix(expr, drug_features)

    def predict_matrix(self, data_loader, drug_features, device='cuda'):
        self.eval()
        
        drug_features = drug_features.to(device)

        preds = []
        with torch.no_grad():
            for (x,) in data_loader:
                x = x.to(device)
                pred = self._predict_response_matrix(x, drug_features)
                preds.append(pred)

        preds = torch.cat(preds, axis=0).cpu().detach().numpy()
        return preds

    def save_model(self, directory, fold_id):
        torch.save(self.state_dict(), directory+'/model_weights_fold_%d'%fold_id)

        x = json.dumps(self.hyp)
        f = open(directory+"/model_config_fold_%d.txt"%fold_id,"w")
        f.write(x)
        f.close()