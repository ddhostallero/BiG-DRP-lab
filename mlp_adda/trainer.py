import torch
import time
import numpy as np
from scipy.stats import pearsonr, spearmanr
from sklearn.metrics import roc_auc_score
from collections import deque
import json
from copy import deepcopy

class AdaptationTrainer:
    def __init__(self, orig_model, discriminator, hyp):

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.bce_with_logits_loss = torch.nn.BCEWithLogitsLoss()
        self.val_frequency = hyp['val_frequency']

        self.src_encoder = orig_model.expr_encoder.to(self.device)
        self.tar_encoder = deepcopy(orig_model.expr_encoder).to(self.device)
        self.discriminator = discriminator.to(self.device)
        self.orig_model = orig_model

        self.enc_optimizer = torch.optim.Adam(self.tar_encoder.parameters(), lr=hyp['enc_learning_rate'])
        self.disc_optimizer = torch.optim.Adam(self.discriminator.parameters(), lr=hyp['disc_learning_rate'])

    def validation_step(self, src_val_loader, tar_val_loader, device):
        self.discriminator.eval()
        self.src_encoder.eval()
        self.tar_encoder.eval()

        with torch.no_grad():
            src_dom = []
            for (expr,) in src_val_loader:
                expr = expr.to(device)
                src_dom.append(self.discriminator(self.src_encoder(expr)))
            src_dom = torch.cat(src_dom, axis=0).cpu()

            tar_dom = []
            for (expr,) in tar_val_loader:
                expr = expr.to(device)
                tar_dom.append(self.discriminator(self.tar_encoder(expr)))
            tar_dom = torch.cat(tar_dom, axis=0).cpu()

            pred_dom = torch.cat([src_dom, tar_dom], axis=0)
            true_dom = torch.Tensor([1]*len(src_dom)+[0]*len(tar_dom)).unsqueeze(1)

            # performance of the discriminator
            true_bce = self.bce_with_logits_loss(pred_dom, true_dom)

            # performance of the encoder
            map_loss = self.bce_with_logits_loss(tar_dom, torch.ones((len(tar_dom), 1)))
            loss = true_bce + map_loss

            # low = predictions close to 0.5 (confused); high = predictions close to 0 or 1 (confident)
            # min = 0.301, max = 1.002
            clamped_pred = torch.clamp(torch.sigmoid(pred_dom), 0.01, 0.99)
            confusion = (-0.5*torch.log10(clamped_pred) - 0.5*torch.log10(1-clamped_pred)).mean()
            
            # percentage of target that is classified as source
            false_src_rate = (torch.sigmoid(tar_dom) > 0.5).sum()/len(tar_dom)

            # balanced accuracy = (TPR+TNR)/2
            true_src_rate = (torch.sigmoid(src_dom) > 0.5).sum()/len(src_dom)
            true_tar_rate = (torch.sigmoid(tar_dom) < 0.5).sum()/len(tar_dom)
            balanced_accuracy = (true_src_rate + true_tar_rate)/2

        return loss, map_loss, true_bce, confusion, false_src_rate, balanced_accuracy

    def train_discriminator_step(self, src_expr, tar_expr, device):
        
        src_expr, tar_expr  = src_expr.to(device), tar_expr.to(device)
        
        self.disc_optimizer.zero_grad()
        src_enc = self.src_encoder(src_expr)
        tar_enc = self.tar_encoder(tar_expr)

        encoding = torch.cat([src_enc, tar_enc])
        pred_dom = self.discriminator(encoding)
        true_dom = torch.Tensor([1]*len(src_enc)+[0]*len(tar_enc)).unsqueeze(1).to(device)

        # discriminator wants to predict correctly (1=src, 0=tar)
        disc_loss = self.bce_with_logits_loss(pred_dom, true_dom)
        disc_loss.backward()
        self.disc_optimizer.step() # update only domain classifier

        return disc_loss.item()

    def train_encoder_step(self, tar_expr, device):

        tar_expr = tar_expr.to(device)

        self.enc_optimizer.zero_grad()
        tar_enc = self.tar_encoder(tar_expr)
        tar_pred_dom = self.discriminator(tar_enc)

        # targets must be classified as src (i.e. 1, instead of 0); we dont; care about source
        tar_enc_loss = self.bce_with_logits_loss(tar_pred_dom, 
            torch.Tensor([1]*len(tar_enc)).unsqueeze(1).to(device)) 
        tar_enc_loss.backward()
        self.enc_optimizer.step()

        return tar_enc_loss.item()


    def warmup_discriminator(self, src_train_loader, tar_train_loader, src_val_loader, 
        tar_val_loader, max_warmup_step=1000):
        
        num_batch_src = len(src_train_loader)
        num_batch_tar = len(tar_train_loader)

        src_step = num_batch_src
        tar_step = num_batch_tar
        global_step = 0

        self.flip_train_switch()
        while global_step < max_warmup_step:

            if src_step == num_batch_src:
                src = iter(src_train_loader)
                src_step = 0
            if tar_step == num_batch_tar:
                tar = iter(tar_train_loader)
                tar_step = 0

            (src_expr,) = next(src)
            (tar_expr,) = next(tar)

            train_disc_bce = self.train_discriminator_step(src_expr, tar_expr, self.device)

            global_step += 1
            src_step += 1
            tar_step += 1

            if global_step % 10 == 0:
                val_metrics = self.validation_step(src_val_loader, tar_val_loader, self.device)
                self.flip_train_switch() 

                if val_metrics[5] > 0.8:
                    break
        print("Pretrained discriminator with %d steps to reach %.2f balanced_accuracy"%(global_step, val_metrics[4]))

    def flip_train_switch(self):
        self.tar_encoder.train()
        self.src_encoder.train()
        self.discriminator.train()

    def fit(self, max_global_step, src_train_loader, src_val_loader,
        tar_train_loader, tar_val_loader, tuning=False, maxout=False, revert=False):

        start_time = time.time()

        best_crit = np.inf
        ret_matrix = np.zeros((max_global_step//self.val_frequency + 1, 11))
        metric_names = ['step', 'val-loss', 'val-enc-bce', 'val-disc-bce', 'val-conf', 'val-fsr', 
                        'val-bal-acc', 'train-disc-bce', 'train-adap-bce', 'src-epoch', 'tar-epoch']

        num_batch_src = len(src_train_loader)
        num_batch_tar = len(tar_train_loader)

        print(num_batch_src, num_batch_tar)

        src_step = num_batch_src
        tar_step = num_batch_tar
        global_step = 0
        count = 0
        train_disc_bce = np.nan
        train_adap_bce = np.nan
        best_step = max_global_step
        

        self.warmup_discriminator(src_train_loader, tar_train_loader, src_val_loader, tar_val_loader)

        val_metrics = self.validation_step(src_val_loader, tar_val_loader, self.device)
        ret_row = [0] + list(val_metrics) + [0, 0, 0, 0] 
        ret_matrix[0] = ret_row

        # lambda1 = lambda s: max(0.99**s, 0.1) # lr of disc is higher than in the 1st 230 steps
        # scheduler = torch.optim.lr_scheduler.LambdaLR(self.disc_optimizer, lr_lambda=lambda1)

        train_disc = False 
        train_enc = True # train encoder first, because discriminator is already warmed up

        self.flip_train_switch()
        while global_step < max_global_step:

            if src_step == num_batch_src:
                src = iter(src_train_loader)
                src_step = 0

            if tar_step == num_batch_tar:
                tar = iter(tar_train_loader)
                tar_step = 0

            (src_expr,) = next(src)
            (tar_expr,) = next(tar)

            if train_disc:
                train_disc_bce = self.train_discriminator_step(src_expr, tar_expr, self.device)
            if train_enc:
                train_adap_bce = self.train_encoder_step(tar_expr, self.device)

            global_step += 1
            src_step += 1
            tar_step += 1

            # scheduler.step()

            if global_step % (self.val_frequency//2) == 0:
                # change training flags
                train_disc = not train_disc 
                train_enc = not train_enc

            if global_step % self.val_frequency == 0:
                val_metrics = self.validation_step(src_val_loader, tar_val_loader, self.device)
                self.flip_train_switch() # flip train switch after validation
                ret_row = [global_step] + list(val_metrics) \
                    + [train_disc_bce, train_adap_bce] \
                    + [global_step/num_batch_src, global_step/num_batch_tar]
                ret_matrix[global_step//self.val_frequency] = ret_row

                crit = val_metrics[0]
                if crit < best_crit:
                    best_crit = crit
                    best_step = global_step
                    count = 0
                    if revert:
                        best_model_state = deepcopy(self.tar_encoder.state_dict())
                else:
                    count += 1

                elapsed_time = time.time() - start_time
                start_time = time.time()
                print("%d\t%s:%.3f\t%s:%.4f\t%s:%.3f\t%s:%.3f\t%s:%.3f\t%s:%.3f\t%ds"%(
                    global_step, 
                    metric_names[2], ret_row[2], 
                    metric_names[3], ret_row[3], 
                    metric_names[4], ret_row[4], 
                    metric_names[6], ret_row[6], 
                    metric_names[7], ret_row[7],
                    metric_names[8], ret_row[8], int(elapsed_time)))

                if ((not maxout) and (count == 10)):
                    ret_matrix = ret_matrix[:(global_step//self.val_frequency)+1]
                    if revert:
                        self.tar_encoder.load_state_dict(best_model_state)
                    break

        return ret_matrix, metric_names, best_step

    def save_model(self, directory, fold_id, hyp):
        adapted_model = deepcopy(self.orig_model)
        adapted_model.expr_encoder.load_state_dict(self.tar_encoder.state_dict())

        torch.save(adapted_model.state_dict(), directory+'/model_weights_fold_%d'%fold_id)
        torch.save(self.discriminator.state_dict(), directory+'/disc_weights_fold_%d'%fold_id)

        x = json.dumps(hyp)
        f = open(directory+"/model_config_fold_%d.txt"%fold_id,"w")
        f.write(x)
        f.close()