import torch
from torch.utils.data import TensorDataset, DataLoader
import numpy as np
import pandas as pd
import json
import pickle

from utils.data_initializer import initialize_ccl, initialize_singlecell, load_drug_features
from utils.utils import mkdir
from mlp_adda.model import FullMLP as Model

def load_model(n_genes, n_drug_feats, hyperparams, model_path):
    model = Model(n_genes, n_drug_feats, hyperparams)
    model.load_state_dict(torch.load(model_path), strict=True)

    return model

def load_hyperparams(hyp_dir):
    with open(hyp_dir) as f:
            hyperparams = f.read()
    return json.loads(hyperparams)

def encode_sc(FLAGS, sc_expr, sc_mapping, folds, n_drug_feats):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    directory = FLAGS.outroot + "results/" + FLAGS.folder

    n_genes = sc_expr.shape[1]
    mkdir(directory + '/embedding/')

    for i in range(5):
        print('==%d=='%i)
        test_fold = i 
        train_folds = [x for x in range(5) if (x != test_fold)]

        print('loading...')
        hyperparams = load_hyperparams(directory+'/model_config_fold_%d.txt'%i)
        model_path = directory + '/model_weights_fold_%d'%i
        model = load_model(n_genes, n_drug_feats, hyperparams, model_path)
        model = model.to(device)
        print('finished loading...')

        bulk_train_samples = folds.loc[folds['fold'].isin(train_folds)]['cell_line'].unique()
        # we want to exclude the samples that are in the train set

        matched_train_samples = sc_mapping[sc_mapping.isin(bulk_train_samples)].index 
        not_in_train_samples = sc_mapping[~sc_mapping.isin(bulk_train_samples)].index
        
        # normalizer = pickle.load(open(FLAGS.outroot + "/results/" + FLAGS.folder + "/sc_normalizer_fold_%d.pkl"%i, "rb"))
        normalizer = pickle.load(open(FLAGS.outroot + "/results/reboot/adda/adda/sc_normalizer_fold_%d.pkl"%i, "rb"))
        test_expr = normalizer.transform(sc_expr.loc[not_in_train_samples].values)
        matched_train_expr = normalizer.transform(sc_expr.loc[matched_train_samples].values)

        data = TensorDataset(torch.FloatTensor(test_expr))
        data = DataLoader(data, batch_size=hyperparams['batch_size'], shuffle=False)
        print('encoding...')
        enc = model.expr_encoder.encode_in_batches(data, device=device)
        enc = pd.DataFrame(enc, index=not_in_train_samples)
        enc.to_csv(directory + '/embedding/sc_test_emb_fold_%d.csv'%i)

        data = TensorDataset(torch.FloatTensor(matched_train_expr))
        data = DataLoader(data, batch_size=hyperparams['batch_size'], shuffle=False)
        enc = model.expr_encoder.encode_in_batches(data, device=device)
        enc = pd.DataFrame(enc, index=matched_train_samples)
        enc.to_csv(directory + '/embedding/sc_match_emb_fold_%d.csv'%i)

def encode_bulk(FLAGS, bulk_expr, folds, n_drug_feats):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    directory = FLAGS.outroot + "results/" + FLAGS.folder

    n_genes = bulk_expr.shape[1]
    mkdir(directory + '/embedding/')

    for i in range(5):
        print('==%d=='%i)
        print('loading...')
        hyperparams = load_hyperparams(directory+'/model_config_fold_%d.txt'%i)
        model_path = directory + '/model_weights_fold_%d'%i
        model = load_model(n_genes, n_drug_feats, hyperparams, model_path)
        model = model.to(device)
        print('finished loading...')

        normalizer = pickle.load(open(FLAGS.outroot + "/results/" + FLAGS.folder + "/normalizer_fold_%d.pkl"%i, "rb"))
        expr = normalizer.transform(bulk_expr)

        data = TensorDataset(torch.FloatTensor(expr))
        data = DataLoader(data, batch_size=hyperparams['batch_size'], shuffle=False)
        print('encoding...')
        enc = model.expr_encoder.encode_in_batches(data, device=device)
        enc = pd.DataFrame(enc, index=bulk_expr.index)
        enc.to_csv(directory + '/embedding/emb_fold_%d.csv'%i)

def main(FLAGS):

    gene_list = '../sc-data/scDEAL/scBiG/kinker/genes.txt'
    genes = [line.strip() for line in open(gene_list)] 
    
    bulk_expr, folds, _ = initialize_ccl(FLAGS, exclude_missing=False)
    n_drug_feats = load_drug_features(FLAGS).shape[1]

    if FLAGS.mode == 'encode_sc':
        sc_expr, sc_mapping, _ = initialize_singlecell(FLAGS)
        sc_expr = sc_expr.loc[:, genes]
        encode_sc(FLAGS, sc_expr, sc_mapping, folds, n_drug_feats)
    elif FLAGS.mode == 'encode_bulk':
        bulk_expr = bulk_expr[genes]
        encode_bulk(FLAGS, bulk_expr, folds, n_drug_feats)

    # print("Overall Performance")
    # print(test_metrics)
