import torch
from torch.utils.data import TensorDataset, DataLoader
import numpy as np
import pandas as pd
import json
import pickle

from utils.data_initializer import initialize_ccl, initialize_singlecell, load_drug_features
from mlp_adda.model import FullMLP as Model

def load_model(n_genes, n_drug_feats, hyperparams, model_path):
    model = Model(n_genes, n_drug_feats, hyperparams)
    model.load_state_dict(torch.load(model_path), strict=True)

    return model

def load_hyperparams(hyp_dir):
    with open(hyp_dir) as f:
            hyperparams = f.read()
    return json.loads(hyperparams)

def test_on_sc(FLAGS, sc_expr, sc_mapping, folds, drug_feats):
    drug_feats_tensor = torch.FloatTensor(drug_feats.values)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    directory = FLAGS.outroot + "results/" + FLAGS.folder

    n_genes = sc_expr.shape[1]
    n_drug_feats = drug_feats.shape[1]

    for i in range(5):
        print('==%d=='%i)
        test_fold = i 
        train_folds = [x for x in range(5) if (x != test_fold)]

        print('loading...')
        hyperparams = load_hyperparams(directory+'/model_config_fold_%d.txt'%i)
        model_path = directory + '/model_weights_fold_%d'%i
        model = load_model(n_genes, n_drug_feats, hyperparams, model_path)
        model = model.to(device)
        print('finished loading...')

        bulk_train_samples = folds.loc[folds['fold'].isin(train_folds)]['cell_line'].unique()
        # we want to exclude the samples that are in the train set

        matched_train_samples = sc_mapping[sc_mapping.isin(bulk_train_samples)].index 
        not_in_train_samples = sc_mapping[~sc_mapping.isin(bulk_train_samples)].index
        
        # normalize using the test samples (i.e. TestNorm)
        # matched_train_samples do not affect the normalization
        # but are normalized too (just in case we want those predicitons)
        # test_expr, matched_train_expr = normalizer(
        #     sc_expr.loc[not_in_train_samples].values, 
        #     sc_expr.loc[matched_train_samples].values)
        
        normalizer = pickle.load(open(FLAGS.outroot + "/results/" + FLAGS.folder + "/sc_normalizer_fold_%d.pkl"%i, "rb"))
        # normalizer = pickle.load(open(FLAGS.outroot + "/results/reboot/adda/adda/sc_normalizer_fold_%d.pkl"%i, "rb"))
        test_expr = normalizer.transform(sc_expr.loc[not_in_train_samples].values)
        matched_train_expr = normalizer.transform(sc_expr.loc[matched_train_samples].values)

        data = TensorDataset(torch.FloatTensor(test_expr))
        data = DataLoader(data, batch_size=hyperparams['batch_size'], shuffle=False)
        print('predicting...')
        prediction_matrix = model.predict_matrix(data, drug_feats_tensor, device=device)
        prediction_matrix = pd.DataFrame(prediction_matrix, index=not_in_train_samples, columns=drug_feats.index)
        print('done predicting...')
        prediction_matrix.to_csv(directory + '/sc_test_prediction_fold_%d.csv'%i)

        data = TensorDataset(torch.FloatTensor(matched_train_expr))
        data = DataLoader(data, batch_size=hyperparams['batch_size'], shuffle=False)
        prediction_matrix = model.predict_matrix(data, drug_feats_tensor, device=device)
        prediction_matrix = pd.DataFrame(prediction_matrix, index=matched_train_samples, columns=drug_feats.index)
        prediction_matrix.to_csv(directory + '/sc_match_prediction_fold_%d.csv'%i)

def main(FLAGS):

    gene_list = '../sc-data/scDEAL/scBiG/kinker/genes.txt'
    genes = [line.strip() for line in open(gene_list)] 
    
    _, folds, _ = initialize_ccl(FLAGS, exclude_missing=False)
    sc_expr, sc_mapping, _ = initialize_singlecell(FLAGS)
    drug_feats = load_drug_features(FLAGS)
    sc_expr = sc_expr.loc[:, genes]

    test_on_sc(FLAGS, sc_expr, sc_mapping, folds, drug_feats)

    # print("Overall Performance")
    # print(test_metrics)
