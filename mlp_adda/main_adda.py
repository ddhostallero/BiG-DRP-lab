from utils.tuple_dataset import TupleMapDatasetV2
from utils.utils import mkdir, reindex_tuples, reset_seed, create_fold_mask, load_hyperparams
from utils.data_initializer import initialize_ccl, initialize_singlecell_folds

import torch
from torch.utils.data import DataLoader, TensorDataset
from mlp_adda.trainer import AdaptationTrainer
from mlp_adda.model import FullMLP, Discriminator#, Encoder, Predictor
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
import pickle

def fold_validation(hyperparams, seed, src_train, src_val, tar_train, tar_val, maxout, revert):
    reset_seed(seed)
    src_train_loader = DataLoader(src_train, batch_size=hyperparams['batch_size'], shuffle=True, drop_last=True)
    src_val_loader = DataLoader(src_val, batch_size=hyperparams['batch_size'], shuffle=False)
    tar_train_loader = DataLoader(tar_train, batch_size=hyperparams['batch_size'], shuffle=True, drop_last=True)
    tar_val_loader = DataLoader(tar_val, batch_size=hyperparams['batch_size'], shuffle=False)

    model = FullMLP(hyperparams['n_genes'],hyperparams['n_drug_feats'], hyperparams)
    model.load_state_dict(torch.load(hyperparams['pre_adda_weights']), strict=True)

    discriminator = Discriminator(hyperparams['gene_l3'], hyperparams)

    # pre-adaptation
    # trainer = AdaptationTrainer(src_encoder, tar_encoder, discriminator, hyp)
    trainer = AdaptationTrainer(model, discriminator, hyperparams)


    val_error, metric_names, best_step = trainer.fit(
        max_global_step=hyperparams['max_global_step'], 
        src_train_loader=src_train_loader, src_val_loader=src_val_loader,
        tar_train_loader=tar_train_loader, tar_val_loader=tar_val_loader,
        maxout=maxout, revert=revert)

    # model.expr_encoder.load_state_dict(trainer.tar_encoder.state_dict(), strict=True)

    return val_error, trainer, metric_names, best_step

def create_dataset_bulk(expr, folds, train_folds, test_folds, normalizer,
    include_unmatched_in_train, exclude_fold=[]):

    if len(exclude_fold) > 1:
        folds = folds.loc[~folds['fold'].isin(exclude_fold)]

    train_samples = list(folds.loc[folds['fold'].isin(train_folds), 'cell_line'].unique())
    train_x = expr.loc[train_samples].values

    test_samples = list(folds.loc[folds['fold'].isin(test_folds), 'cell_line'].unique())
    test_x = expr.loc[test_samples].values

    # train_x, test_x = normalizer(train_x, test_x)
    if normalizer is None:
        normalizer = StandardScaler()
        train_x = normalizer.fit_transform(train_x)
    else:
        train_x = normalizer.transform(train_x)
    test_x = normalizer.transform(test_x)

    if include_unmatched_in_train:
        extra_samples = list(set(expr.index) - set(train_samples) - set(test_samples))
        extra_x = expr.loc[extra_samples]
        # _, extra_x = normalizer(train_x, extra_x)
        extra_x = normalizer.transform(extra_x)
        train_x = np.concatenate([train_x, extra_x], axis=0)

    print("Train bulk: %d"%len(train_x))
    train_x = TensorDataset(torch.FloatTensor(train_x))
    test_x = TensorDataset(torch.FloatTensor(test_x))

    return train_x, test_x, train_samples, test_samples, normalizer

def create_dataset_sc(expr, folds, current_fold, normalizer, include_unmatched_in_train, include_val_in_train=False, mode='val'):
    """
        expr: gene expressions
        folds: sc folds
        current_fold: current test fold
        include_unmatched_in_train: include SC without matching bulk in the training set (as long as it has SC GEx)
        include_val_in_train: if True, include validation set in the training set (for double-pass CV)
        mode: either val or test
    """

    if mode == 'val' and include_val_in_train:
        print("BAD PRACTICE DETECTED: you should not include the validation set in the training during hyperparameter search")
        exit()


    test_samples = list(folds.loc[folds['%s_bulk_lco_%d'%(mode, current_fold)]==1].index)
    test_x = expr.loc[test_samples].values

    train_samples = list(folds.loc[folds['train_bulk_lco_%d'%current_fold]==1].index)
    
    if include_unmatched_in_train:
        extra_samples = list(folds.loc[~folds['has_bulk_GEx']].index)
        train_samples += extra_samples

    if include_val_in_train:
        val_samples = set(folds.loc[folds['val_bulk_lco_%d'%current_fold]==1].index)
        train_samples = list(set(train_samples).union(val_samples))

    train_x = expr.loc[train_samples].values
    # train_x, test_x = normalizer(train_x, test_x)

    if normalizer is None:
        normalizer = StandardScaler()
        train_x = normalizer.fit_transform(train_x)
    else:
        train_x = normalizer.transform(train_x)
    test_x = normalizer.transform(test_x)


    train_x = TensorDataset(torch.FloatTensor(train_x))
    test_x = TensorDataset(torch.FloatTensor(test_x))

    return train_x, test_x, train_samples, test_samples, normalizer


def single_cross_validation(FLAGS, bulk_expr, bulk_folds, sc_expr, sc_folds):
    reset_seed(FLAGS.seed)

    base_folder = FLAGS.outroot + "/results/" + FLAGS.folder
    hyperparams = {
        'enc_learning_rate': 5e-5,
        'disc_learning_rate': 1e-4,
        'max_global_step': 300*1000, # 262 iters is 1 epoch of SC, 9 iters is 1 epoch of bulk
        'disc_l1': 256,
        'disc_l2': 128,
        'val_frequency': 100}

    # label_mask = create_fold_mask(labels)

    final_metrics = None

    for i in range(5):
        print('==%d=='%i)
        test_fold = i 
        val_fold = (i+1)%5
        train_folds = [x for x in range(5) if (x != test_fold) and (x != val_fold)]

        hp = load_hyperparams(FLAGS.outroot + FLAGS.weight_folder + "/model_config_fold_%d.txt"%i)
        hp['pre_adda_weights'] = FLAGS.outroot + FLAGS.weight_folder + "/model_weights_fold_%d"%i
        for k, v in hyperparams.items():
            hp[k] = v 

        # load normalizer for bulk
        bulk_normalizer = pickle.load(open(FLAGS.outroot + FLAGS.weight_folder + "/normalizer_fold_%d.pkl"%i, "rb"))

        # === Adapt Model ===

        src_train,src_val,_,_,_ = create_dataset_bulk(bulk_expr, bulk_folds, train_folds, [val_fold],
            normalizer=bulk_normalizer, include_unmatched_in_train=True, exclude_folds=[test_fold])
        tar_train,tar_val,_,_,sc_normalizer = create_dataset_sc(sc_expr, sc_folds, i, normalizer=None,
            include_unmatched_in_train=True, include_val_in_train=False, mode='val')

        val_error,trainer,metric_names,best_step = fold_validation(hp, 
            seed=FLAGS.seed, 
            src_train=src_train, 
            src_val=src_val, 
            tar_train=tar_train, 
            tar_val=tar_val,
            maxout=True,
            revert=False)
        hp['best_step'] = best_step

        if i == 0:
            final_metrics = np.zeros((5, val_error.shape[1]))

        # save logs
        final_metrics[i] = val_error[-1]
        val_metrics = pd.DataFrame(val_error, columns=metric_names)
        val_metrics.to_csv(FLAGS.outroot + "/results/" + FLAGS.folder + '/fold_%d.csv'%i, index=False)

        # save model and normalizer
        trainer.save_model(FLAGS.outroot + "/results/" + FLAGS.folder, i, hp)
        pickle.dump(sc_normalizer, open(FLAGS.outroot + "/results/" + FLAGS.folder + "/sc_normalizer_fold_%d.pkl"%i, "wb"))

    final_metrics = pd.DataFrame(final_metrics, columns=metric_names)
    return final_metrics

def main(FLAGS):
    gene_list = '../sc-data/scDEAL/scBiG/kinker/genes.txt'
    genes = [line.strip() for line in open(gene_list)]

    bulk_expr, bulk_folds, _ = initialize_ccl(FLAGS)
    bulk_expr = bulk_expr[genes]

    sc_expr, sc_folds, _ = initialize_singlecell_folds(FLAGS)
    sc_expr  = sc_expr[genes]


    test_metrics = single_cross_validation(FLAGS, bulk_expr, bulk_folds, sc_expr, sc_folds)

    print("Overall Performance")
    print(test_metrics.mean())
