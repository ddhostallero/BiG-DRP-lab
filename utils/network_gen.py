import pandas as pd
import numpy as np
import dgl
import torch
import copy


def create_network(tuples, percentile=1, relational=False):    

    if 'edge_score' in tuples.columns:
        score_col = 'edge_score'
    else:
        score_col = 'response'

    sen_edges = pd.DataFrame()
    res_edges = pd.DataFrame()
    for drug in tuples['drug'].unique():
        drug_edges = tuples.loc[tuples['drug']==drug]        
        thresh = np.percentile(drug_edges[score_col], percentile)
        sen_edges = pd.concat([sen_edges, drug_edges.loc[drug_edges[score_col]<thresh]])
        thresh = np.percentile(drug_edges[score_col], (100-percentile))
        res_edges = pd.concat([res_edges, drug_edges.loc[drug_edges[score_col]>thresh]])

    print("generated a network with %d sensitive edges and %d resistant edges "%(len(sen_edges), len(res_edges)))
    cl = list(set(sen_edges['cell_line']).union(set(res_edges['cell_line'])))
    print('unique_CLs', len(cl))

    if relational:
        # create a homogeneous graph (in dgl's eyes)
        n_drugs = tuples['drug'].nunique()
        sen_edges['cell_line'] += n_drugs
        res_edges['cell_line'] += n_drugs 
        all_edges = pd.concat([sen_edges, res_edges])
        network = dgl.graph((torch.tensor(all_edges['drug'].values), torch.tensor(all_edges['cell_line'].values)), idtype=torch.int32)
        network = dgl.to_bidirected(network)
        # add edge type id
        edge_types = torch.LongTensor([0]*len(sen_edges) + [1]*len(res_edges) + [0]*len(sen_edges) + [1]*len(res_edges))
        network.edata['response_type'] = edge_types
    else:
        graph_data = {
                ('cell_line', 'is_sensitive', 'drug'): (sen_edges['cell_line'].values, sen_edges['drug'].values),
                ('drug', 'is_effective', 'cell_line'): (sen_edges['drug'].values, sen_edges['cell_line'].values),
                ('cell_line', 'is_resistant', 'drug'): (res_edges['cell_line'].values, res_edges['drug'].values),
                ('drug', 'is_ineffective', 'cell_line'): (res_edges['drug'].values, res_edges['cell_line'].values)}
        network = dgl.heterograph(graph_data)

    print(network)

    if len(cl) != tuples['cell_line'].nunique():
        n_nodes_to_add = tuples['cell_line'].max()-max(cl)
        print('Added %d disconnected CCL nodes for indexing purposes'%n_nodes_to_add)
        if relational:
            network.add_nodes(n_nodes_to_add)
        else:
            network.add_nodes(n_nodes_to_add, ntype='cell_line')
    return network

def append_to_mutation_network(network, mutations, homogeneous=False, gene_offset=0):
    # add the new cell lines as destination nodes only

    mut_edges = mutations.melt(var_name='gene', value_name='mutated', ignore_index=False)
    mut_edges = mut_edges.reset_index()
    mut_edges = mut_edges.loc[mut_edges['mutated'] == 1]
    mut_edges = mut_edges[['new_index', 'gene']]
    mut_edges.columns=['cell_line', 'gene']

    val_network = copy.deepcopy(network)

    if homogeneous:
        mut_edges['cell_line'] += network.num_nodes()
        mut_edges['gene'] += mut_idx_offset
        val_network = dgl.add_nodes(val_network, mut_edges['cell_line'].nunique())
        val_network = dgl.add_edges(val_network, mut_edges['gene'].values, mut_edges['cell_line'].values,
            data={'relation_type': torch.tensor([2]*len(mut_edges))})

    else:
        val_network = dgl.add_nodes(val_network, mut_edges['cell_line'].nunique(), ntype='cell_line')
        val_network = dgl.add_edges(val_network, mut_edges['gene'].values, mut_edges['cell_line'].values, etype='is_mutated')

    print(val_network)

    return val_network

def create_mutation_network(tuples, mutations, percentile=1, homogeneous=False):

    if 'edge_score' in tuples.columns:
        score_col = 'edge_score'
    else:
        score_col = 'response'

    mutations = mutations.loc[mutations.index.isin(tuples['cell_line'].unique())] # so that we don't add ccls that do not exist

    mut_edges = mutations.melt(var_name='gene', value_name='mutated', ignore_index=False)
    mut_edges = mut_edges.reset_index()
    mut_edges = mut_edges.loc[mut_edges['mutated'] == 1]
    mut_edges = mut_edges[['new_index', 'gene']]
    mut_edges.columns=['cell_line', 'gene']

    sen_edges = pd.DataFrame()
    res_edges = pd.DataFrame()
    for drug in tuples['drug'].unique():
        drug_edges = tuples.loc[tuples['drug']==drug]        
        thresh = np.percentile(drug_edges[score_col], percentile)
        sen_edges = pd.concat([sen_edges, drug_edges.loc[drug_edges[score_col]<thresh]])
        thresh = np.percentile(drug_edges[score_col], (100-percentile))
        res_edges = pd.concat([res_edges, drug_edges.loc[drug_edges[score_col]>thresh]])

    print("generated a network with %d mutation edges, %d sensitive \
        edges and %d resistant edges "%(len(mut_edges), len(sen_edges), len(res_edges)))

    if homogeneous:
        graph = None

        # create a homogeneous graph (in dgl's eyes)
        n_drugs = tuples['drug'].nunique()
        n_genes = mut_edges['gene'].nunique()

        mut_edges['gene'] += n_drugs
        sen_edges['cell_line'] += (n_drugs + n_genes)
        res_edges['cell_line'] += (n_drugs + n_genes)


        all_edges = pd.concat([sen_edges, res_edges, mut_edges])
        network = dgl.graph((torch.tensor(all_edges['drug'].values), 
            torch.tensor(all_edges['cell_line'].values)), idtype=torch.int32)
        network = dgl.to_bidirected(network)

        # add edge type id
        edge_types = torch.LongTensor(
            [0]*len(sen_edges) + [1]*len(res_edges) + [2]*len(mut_edges) + \
            [0]*len(sen_edges) + [1]*len(res_edges) + [2]*len(mut_edges))
        network.edata['relation_type'] = edge_types

    else:
        graph_data = {
                ('cell_line', 'has_mutation', 'gene'): (mut_edges['cell_line'].values, mut_edges['gene'].values),
                ('gene', 'is_mutated', 'cell_line'): (mut_edges['gene'].values, mut_edges['cell_line'].values),
                ('cell_line', 'is_sensitive', 'drug'): (sen_edges['cell_line'].values, sen_edges['drug'].values),
                ('drug', 'is_effective', 'cell_line'): (sen_edges['drug'].values, sen_edges['cell_line'].values),
                ('cell_line', 'is_resistant', 'drug'): (res_edges['cell_line'].values, res_edges['drug'].values),
                ('drug', 'is_ineffective', 'cell_line'): (res_edges['drug'].values, res_edges['cell_line'].values)}

        network = dgl.heterograph(graph_data)
    print(network)

    return network

def create_mutation_target_network(tuples, mutations, drug_targets, percentile=1):

    if 'edge_score' in tuples.columns:
        score_col = 'edge_score'
    else:
        score_col = 'response'

    mutations = mutations.loc[mutations.index.isin(tuples['cell_line'].unique())] # so that we don't add ccls that do not exist

    mut_edges = mutations.melt(var_name='gene', value_name='mutated', ignore_index=False)
    mut_edges = mut_edges.reset_index()
    mut_edges = mut_edges.loc[mut_edges['mutated'] == 1]
    mut_edges = mut_edges[['new_index', 'gene']]
    mut_edges.columns=['cell_line', 'gene']

    sen_edges = pd.DataFrame()
    res_edges = pd.DataFrame()
    for drug in tuples['drug'].unique():
        drug_edges = tuples.loc[tuples['drug']==drug]        
        thresh = np.percentile(drug_edges[score_col], percentile)
        sen_edges = pd.concat([sen_edges, drug_edges.loc[drug_edges[score_col]<thresh]])
        thresh = np.percentile(drug_edges[score_col], (100-percentile))
        res_edges = pd.concat([res_edges, drug_edges.loc[drug_edges[score_col]>thresh]])

    print("generated a network with %d mutation edges, %d sensitive \
        edges and %d resistant edges "%(len(mut_edges), len(sen_edges), len(res_edges)))

    graph_data = {
             ('cell_line', 'has_mutation', 'gene'): (mut_edges['cell_line'].values, mut_edges['gene'].values),
             ('gene', 'is_mutated', 'cell_line'): (mut_edges['gene'].values, mut_edges['cell_line'].values),
             ('cell_line', 'is_sensitive', 'drug'): (sen_edges['cell_line'].values, sen_edges['drug'].values),
             ('drug', 'is_effective', 'cell_line'): (sen_edges['drug'].values, sen_edges['cell_line'].values),
             ('cell_line', 'is_resistant', 'drug'): (res_edges['cell_line'].values, res_edges['drug'].values),
             ('drug', 'is_ineffective', 'cell_line'): (res_edges['drug'].values, res_edges['cell_line'].values),
             ('drug', 'targets', 'gene'): (drug_targets['drug'].values, drug_targets['gene'].values),
             ('gene', 'is_targeted_by', 'drug'): (drug_targets['gene'].values, drug_targets['drug'].values)}

    network = dgl.heterograph(graph_data)
    print(network)

    return network

def _get_response_edges(tuples, percentile):

    if 'edge_score' in tuples.columns:
        score_col = 'edge_score'
    else:
        score_col = 'response'

    sen_edges = pd.DataFrame()
    res_edges = pd.DataFrame()
    for drug in tuples['drug'].unique():
        drug_edges = tuples.loc[tuples['drug']==drug]        
        thresh = np.percentile(drug_edges[score_col], percentile)
        sen_edges = pd.concat([sen_edges, drug_edges.loc[drug_edges[score_col]<thresh]])
        thresh = np.percentile(drug_edges[score_col], (100-percentile))
        res_edges = pd.concat([res_edges, drug_edges.loc[drug_edges[score_col]>thresh]])

    return sen_edges, res_edges


def create_crispr_network(tuples, gene_ess, homogeneous=False, percentile=1, 
    ess_threshold=-1, min_ess_edge_per_ccl=1, max_ess_edge_per_ccl=None,
    min_ess_edge_per_gene=2, max_ess_edge_per_gene=np.inf):

    gene_ess = gene_ess.loc[gene_ess.index.isin(tuples['cell_line'].unique())] # so that we don't add ccls that do not exist

    if max_ess_edge_per_ccl is not None:
        gene_ess = gene_ess.apply(pd.Series.nsmallest, axis=1, n=max_ess_edge_per_ccl)

    minimum_crispr_graph = (gene_ess.apply(pd.Series.nsmallest, axis=1, n=min_ess_edge_per_ccl).notna())*1
    gene_ess_bin = 1*((1*(gene_ess < ess_threshold) + minimum_crispr_graph) > 0) 
    
    # remove genes that have < min_ess_edge_per_gene
    gene_ess_bin = gene_ess_bin.loc[:,gene_ess_bin.sum()>=min_ess_edge_per_gene]

    # remove genes the have > max_ess_edge_per_gene
    gene_ess_bin = gene_ess_bin.loc[:,gene_ess_bin.sum()<max_ess_edge_per_gene]
    gene_nodes = list(gene_ess_bin.columns)
    gene_ess_bin.columns = range(len(gene_ess_bin.columns)) # replace with integers for graph

    ess_edges = gene_ess_bin.melt(var_name='gene', value_name='essential', ignore_index=False)
    ess_edges = ess_edges.reset_index()
    ess_edges = ess_edges.loc[ess_edges['essential'] == 1]
    ess_edges = ess_edges[['new_index', 'gene']]
    ess_edges.columns=['cell_line', 'gene']

    res_edges, sen_edges = _get_response_edges(tuples, percentile)

    print("generated a network with %d essential edges, %d sensitive \
        edges and %d resistant edges "%(len(ess_edges), len(sen_edges), len(res_edges)))

    if homogeneous:
        print("Homogenous network track not yet implemented")
        exit()
    else:
        graph_data = {
                ('cell_line', 'dependent_to', 'gene'): (ess_edges['cell_line'].values, ess_edges['gene'].values),
                ('gene', 'is_essential', 'cell_line'): (ess_edges['gene'].values, ess_edges['cell_line'].values),
                ('cell_line', 'is_sensitive', 'drug'): (sen_edges['cell_line'].values, sen_edges['drug'].values),
                ('drug', 'is_effective', 'cell_line'): (sen_edges['drug'].values, sen_edges['cell_line'].values),
                ('cell_line', 'is_resistant', 'drug'): (res_edges['cell_line'].values, res_edges['drug'].values),
                ('drug', 'is_ineffective', 'cell_line'): (res_edges['drug'].values, res_edges['cell_line'].values)}

        network = dgl.heterograph(graph_data)
    print(network)

    return network, gene_nodes


def append_to_crispr_network(network, gene_ess, homogeneous=False,
    ess_threshold=-1, min_ess_edge_per_ccl=1, max_ess_edge_per_ccl=None,):
    # add the new cell lines as destination nodes only

    if max_ess_edge_per_ccl is not None:
        gene_ess = gene_ess.apply(pd.Series.nsmallest, axis=1, n=max_ess_edge_per_ccl)

    minimum_crispr_graph = (gene_ess.apply(pd.Series.nsmallest, axis=1, n=min_ess_edge_per_ccl).notna())*1
    gene_ess_bin = 1*((1*(gene_ess < ess_threshold) + minimum_crispr_graph) > 0) 
    gene_ess_bin.columns = range(len(gene_ess.columns))

    ess_edges = gene_ess_bin.melt(var_name='gene', value_name='essential', ignore_index=False)
    ess_edges = ess_edges.reset_index()
    ess_edges = ess_edges.loc[ess_edges['essential'] == 1]
    ess_edges = ess_edges[['new_index', 'gene']]
    ess_edges.columns=['cell_line', 'gene']

    val_network = copy.deepcopy(network)

    if homogeneous:
        print("Homogenous network track not yet implemented")
        exit()
    else:
        val_network = dgl.add_nodes(val_network, ess_edges['cell_line'].nunique(), ntype='cell_line')
        val_network = dgl.add_edges(val_network, ess_edges['gene'].values, ess_edges['cell_line'].values, etype='is_essential')

    print(val_network)

    return val_network