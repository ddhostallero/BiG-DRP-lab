from utils.utils import mkdir, reset_seed
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
# import utils.constants as c

def standardize(train_x, test_x):
    ss = StandardScaler()
    train_x = ss.fit_transform(train_x)
    test_x = ss.transform(test_x)
    return train_x, test_x
    
def load_mutation(FLAGS):
    mut = pd.read_csv(FLAGS.dataroot + FLAGS._c['MUTATION_FILE'], index_col=0)
    # x = mut.sum()/len(mut)
    # mut = mut.loc[:, x<0.5]
    return mut

def load_crispr(FLAGS, remove_global_essential=True):
    crispr = pd.read_csv(FLAGS.dataroot + FLAGS._c['CRISPR_FILE'], index_col=0).T

    print(crispr.shape)
    if remove_global_essential:
        # common_ess = pd.read_csv(FLAGS.dataroot + FLAGS._c['CRISPR_COMMON_ESSENTIALS_FILE'])['gene'].values
        common_ess = pd.read_csv(FLAGS.dataroot + FLAGS._c['CRISPR_COMMON_ESSENTIALS_FILE'])['ensembl_gene_id'].values
        crispr = crispr.loc[:,~crispr.columns.isin(common_ess)]
    print("after global essentiality removal", crispr.shape)
    return crispr

def load_drug_targets(FLAGS):
    tar = pd.read_csv(FLAGS.dataroot + FLAGS._c['DRUG_TARGET_FILE'], index_col=0)
    tar = tar[['drug_name', 'gene']]
    return tar

def load_gene_list(FLAGS):
    genes = None
    if FLAGS.gene_list != "":
        gene_list = FLAGS.dataroot + FLAGS._c['GENE_LIST']
        genes = [line.strip() for line in open(gene_list)]
    # if FLAGS.gene_list == 'kinker':
    #     gene_list = FLAGS.dataroot + FLAGS._c['KINKER_GENES']
    #     genes = [line.strip() for line in open(gene_list)]
    
    return genes

def load_drug_features(FLAGS):
    if FLAGS.drug_feat == 'desc' or FLAGS.drug_feat == 'mixed':
        DRUG_FEATURE_FILE = FLAGS.dataroot + FLAGS._c['DRUG_DESCRIPTOR_FILE']
        drug_feats = pd.read_csv(DRUG_FEATURE_FILE, index_col=0)

        df = StandardScaler().fit_transform(drug_feats.values) # normalize
        drug_feats = pd.DataFrame(df, index=drug_feats.index, columns=drug_feats.columns)

        if FLAGS.drug_feat == 'mixed':
            DRUG_MFP_FEATURE_FILE = FLAGS.dataroot + FLAGS._c['MORGAN_FP_FILE']
            drug_mfp = pd.read_csv(DRUG_MFP_FEATURE_FILE, index_col=0)
            drug_feats[drug_mfp.columns] = drug_mfp

        valid_cols = drug_feats.columns[~drug_feats.isna().any()] # remove columns with missing data
        drug_feats = drug_feats[valid_cols]
        
    else:
        DRUG_FEATURE_FILE = FLAGS.dataroot + FLAGS._c['MORGAN_FP_FILE']
        drug_feats = pd.read_csv(DRUG_FEATURE_FILE, index_col=0)

    return drug_feats

def initialize(FLAGS, binary=False, multitask=False):

    reset_seed(FLAGS.seed)
    mkdir(FLAGS.outroot + "/results/" + FLAGS.folder)

    LABEL_FILE = FLAGS.dataroot + FLAGS._c['LABEL_FILE']    
    GENE_EXPRESSION_FILE = FLAGS.dataroot + FLAGS._c['GENE_EXPRESSION_FILE']
   
    drug_feats = load_drug_features(FLAGS)
    cell_lines = pd.read_csv(GENE_EXPRESSION_FILE, index_col=0).T # need to normalize
    labels = pd.read_csv(LABEL_FILE)
    labels['cell_line'] = labels['cell_line'].astype(str)

    labels = labels.loc[labels['drug'].isin(drug_feats.index)] # use only cell lines with data
    labels = labels.loc[labels['cell_line'].isin(cell_lines.index)] # use only drugs with data
    cell_lines = cell_lines.loc[cell_lines.index.isin(labels['cell_line'].unique())] # use only cell lines with labels
    cell_lines.index.names = ['cell_line'] # fix melt bug
    drug_feats = drug_feats.loc[drug_feats.index.isin(labels['drug'].unique())]      # use only drugs in labels
    drug_feats.index.names = ['drug'] # fix melt bug

    if FLAGS.response_type != 'binary':
        label_matrix = labels.pivot(index='cell_line', columns='drug', values=FLAGS.response_type)
        label_matrix = label_matrix.loc[cell_lines.index, drug_feats.index]

        if FLAGS.normalize_response:
            ss = StandardScaler() # normalize IC50
            temp = ss.fit_transform(label_matrix.values)
            label_matrix = pd.DataFrame(temp, index=label_matrix.index, columns=label_matrix.columns)
        
            # add a z-scored ln_ic50 column
            x = label_matrix.melt(value_name='response', ignore_index=False).dropna()
            x = x.reset_index()
            labels = labels.merge(x, on=['cell_line', 'drug'])
        else:
            labels['response'] = labels[FLAGS.response_type]
            label_matrix = label_matrix.astype(float)
    
    else: # binary
        labels['response'] = labels['sensitive'].astype(int)
        label_matrix = labels.pivot(index='cell_line', columns='drug', values='sensitive')
        label_matrix = label_matrix[drug_feats.index].astype(float)
        
    if FLAGS.split == 'lpo': # leave pairs out
        labels['fold'] = labels['pair_fold']
    else: # default: leave cell lines out
        labels['fold'] = labels['cl_fold']

    print('tuples per fold:')
    print(labels.groupby('fold').size())

    if FLAGS.response_type == 'binary':
        labels['edge_score'] = labels['ln_ic50']
        labels = labels[['drug', 'cell_line', 'response', 'edge_score', 'fold']]
    else:
        labels = labels[['drug', 'cell_line', 'response', 'fold']]

    genes = load_gene_list(FLAGS)
    if genes is not None:
        cell_lines = cell_lines[genes].copy()

    return drug_feats, cell_lines, labels, label_matrix, standardize


def initialize_ccl(FLAGS, exclude_missing=True):
    reset_seed(FLAGS.seed)
    mkdir(FLAGS.outroot + "/results/" + FLAGS.folder)

    fold = get_bulk_folds(FLAGS, exclude_missing)
    GENE_EXPRESSION_FILE = FLAGS.dataroot + FLAGS._c['GENE_EXPRESSION_FILE']

    cell_lines = pd.read_csv(GENE_EXPRESSION_FILE, index_col=0).T # need to normalize
    print('cell lines per fold:')
    print(fold.groupby('fold').size())
    n_extra = len(cell_lines.loc[~cell_lines.index.isin(fold['cell_line'].unique())])
    print('extra (unlabeled) samples: %d'%n_extra)

    genes = load_gene_list(FLAGS)
    if genes is not None:
        cell_lines = cell_lines[genes].copy()

    return cell_lines, fold, standardize

def get_bulk_folds(FLAGS, exclude_missing=True):
    LABEL_FILE = FLAGS.dataroot + FLAGS._c['LABEL_FILE']
    labels = pd.read_csv(LABEL_FILE)
    labels['cell_line'] = labels['cell_line'].astype(str)

    if FLAGS.split == 'lpo': # leave pairs out
        labels['fold'] = labels['pair_fold']
    else: # default: leave cell lines out
        labels['fold'] = labels['cl_fold']

    if exclude_missing:
        labels = labels.loc[labels['fold'] >= 0]

    fold = labels[['cell_line', 'fold']].drop_duplicates()

    return fold


def initialize_singlecell(FLAGS, exclude_no_mapping=True):
    from tqdm import tqdm

    SC_GENE_EXPRESSION_FILE = FLAGS.dataroot + FLAGS._c['SC_GENE_EXPRESSION_FILE']
    SC_MAPPING_FILE = FLAGS.dataroot + FLAGS._c['SC_META_FILE']


    print("loading SC data...")
    chunksize = 500
    data = []

    for chunk in tqdm(pd.read_csv(SC_GENE_EXPRESSION_FILE, chunksize=chunksize, index_col=0)):
        data.append(chunk)

    sc = pd.concat(data).T
    sc_mapping = pd.read_csv(SC_MAPPING_FILE, index_col=0)

    # only include mapping for those with expression
    sc_mapping = sc_mapping.loc[sc_mapping.index.isin(sc.index)]['DepMap_ID']

    if exclude_no_mapping:
        sc = sc.loc[sc_mapping.index]
    else:
        no_mapping = list(set(sc.index) - set(sc_mapping.index))
        for x in no_mapping:
            sc_mapping[x] = x

    genes = load_gene_list(FLAGS)
    if genes is not None:
        sc = sc[genes].copy()

    return sc, sc_mapping, standardize

def initialize_singlecell_folds(FLAGS):
    from tqdm import tqdm

    SC_GENE_EXPRESSION_FILE = FLAGS.dataroot + FLAGS._c['SC_GENE_EXPRESSION_FILE']
    SC_META_FILE = FLAGS.dataroot + FLAGS._c['SC_META_FILE']

    print("loading SC data...")
    chunksize = 500
    data = []

    for chunk in tqdm(pd.read_csv(SC_GENE_EXPRESSION_FILE, chunksize=chunksize, index_col=0)):
        data.append(chunk)

    sc = pd.concat(data)
    sc_meta_fold = pd.read_csv(SC_META_FILE, index_col=0, low_memory=False)

    # only include mapping for those with expression
    sc_meta_fold = sc_meta_fold.loc[sc_meta_fold.index.isin(sc.index)]#['stripped_cell_line_name']
    sc = sc.loc[sc_meta_fold.index]

    genes = load_gene_list(FLAGS)
    if genes is not None:
        sc = sc[genes].copy()

    return sc, sc_meta_fold, standardize