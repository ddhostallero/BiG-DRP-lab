class MyFlags:
    def __init__(self, flags):

        self.PUBLIC_ATTR = []
        for x in vars(flags):
            setattr(self, x, getattr(flags, x))
            self.PUBLIC_ATTR.append(x)

        self._c = {}

        if flags.dataset == 'GDSC':
            self._c = self._initialize_gdsc()
        elif flags.dataset == 'GDSC_MUT':
            self._c = self._initialize_gdsc_with_mut()
        elif flags.dataset == 'CCLE':
            self._c = self._initialize_ccle()
        elif flags.dataset == 'CCLE_CRISPR.EXPR':
            self._c = self._initialize_ccle_with_crispr(False)
        elif flags.dataset == 'CCLE_CRISPR.CRISPR':
            self._c = self._initialize_ccle_with_crispr(True)
        elif flags.dataset == 'CCLE_PQ.EXPR':
            self._c = self._initialize_ccle_with_pq(False)
        elif flags.dataset == 'CCLE_PQ.PQ':
            self._c = self._initialize_ccle_with_pq(True)
        else:
            print("Unknown dataset")
            exit()

        if flags.gene_list == 'kinker':
            self._c['GENE_LIST'] = '/drp-data/DRP2022_preprocessed/gene_lists/ccle_kinker_genes.txt'
            self._c['SC_GENE_EXPRESSION_FILE'] = '/drp-data/DRP2022_preprocessed/kinker/kinker_log2cpm.csv'
            self._c['SC_META_FILE'] = '/drp-data/DRP2022_preprocessed/kinker/meta_fold.csv'
            # self._c['GENE_LIST'] = '/sc-data/scDEAL/scBiG/kinker/genes.txt'
            # self._c['SC_GENE_EXPRESSION_FILE'] = '/sc-data/pancan/filtered/ccle_kinker_log2cpm.csv'
            # # self._c['SC_GENE_EXPRESSION_FILE'] = '/sc-data/pancan/filtered/ccle_kinker_log2cpm_practice.csv'
            # self._c['SC_MAPPING_FILE'] = '/sc-data/scDEAL/scBiG/kinker/meta.csv'
            # self._c['SC_META_FILE'] = '/sc-data/scDEAL/scBiG/kinker/meta_fold.csv'
        elif flags.gene_list == 'crispr':
            self._c['GENE_LIST'] = '/drp-data/DRP2022_preprocessed/gene_lists/ccle_expr_chronos_genes.txt'
        elif flags.gene_list == 'protein':
            self._c['GENE_LIST'] = '/drp-data/DRP2022_preprocessed/gene_lists/ccle_expr_proteinquant_genes.txt'


    def _initialize_gdsc(self):
        c = {
            'LABEL_FILE': '/drp-data/DRP2022_preprocessed/drug_response/gdsc_tuple_labels_folds.csv',
            'GENE_EXPRESSION_FILE': '/drp-data/DRP2022_preprocessed/sanger/sanger_broad_ccl_log2tpm.csv',
            'DRUG_DESCRIPTOR_FILE': '/drp-data/DRP2022_preprocessed/drug_features/gdsc_drug_descriptors.csv',
            'MORGAN_FP_FILE': '/drp-data/DRP2022_preprocessed/drug_features/gdsc_morgan.csv'}
        return c

    def _initialize_gdsc_mut(self):
        c = {
            'LABEL_FILE': '/grl-drp2/mutation/gdsc/gdsc_tuple_labels_folds_with_mut.csv',
            'GENE_EXPRESSION_FILE': '/drp-data/grl-preprocessed/sanger/sanger_fpkm.csv',
            'DRUG_DESCRIPTOR_FILE': '/drp-data/grl-preprocessed/drug_features/gdsc_250_drug_descriptors.csv',
            'MORGAN_FP_FILE': '/drp-data/grl-preprocessed/drug_features/gdsc_250_morgan.csv',
            'MUTATION_FILE': '/grl-drp2/mutation/gdsc/filtered_mutations.csv',
            'DRUG_TARGET_FILE': '/drp-data/grl-preprocessed/drug_features/gdsc_drug_target_net.csv'}
        return c

    def _initialize_ccle(self):
        c = {
            'LABEL_FILE': '/drp-data/DRP2022_preprocessed/drug_response/ctrp_tuple_labels_folds.csv',
            'GENE_EXPRESSION_FILE': '/drp-data/DRP2022_preprocessed/depmap/ccle_log2tpm.csv',
            'DRUG_DESCRIPTOR_FILE': '/drp-data/DRP2022_preprocessed/drug_features/ctrp_drug_descriptors.csv',
            'MORGAN_FP_FILE': '/drp-data/DRP2022_preprocessed/drug_features/ctrp_morgan.csv'}
        return c

    def _initialize_ccle_with_crispr(self, use_crispr):
        if use_crispr:
            GENE_EXPRESSION_FILE = '/drp-data/DRP2022_preprocessed/depmap/ccle_chronos.csv'
        else:
            GENE_EXPRESSION_FILE = '/drp-data/DRP2022_preprocessed/depmap/ccle_log2tpm.csv'
        
        c = {
            'GENE_EXPRESSION_FILE': GENE_EXPRESSION_FILE,
            'LABEL_FILE': '/drp-data/DRP2022_preprocessed/drug_response/crispr_ctrp_ccle_tuple_labels_folds.csv',
            'DRUG_DESCRIPTOR_FILE': '/drp-data/DRP2022_preprocessed/drug_features/ctrp_drug_descriptors.csv',
            'MORGAN_FP_FILE': '/drp-data/DRP2022_preprocessed/drug_features/ctrp_morgan.csv',
            'CRISPR_FILE': '/drp-data/DRP2022_preprocessed/depmap/ccle_chronos.csv',
            'CRISPR_COMMON_ESSENTIALS_FILE': '/drp-data/DB_DepMap/CRISPR/CRISPR_common_essentials_ensembl.csv'}
        return c

    def _initialize_ccle_with_pq(self, use_pq):
        if use_pq:
            GENE_EXPRESSION_FILE = '/drp-data/DRP2022_preprocessed/protein_quantification/ccle_protein_quantification.csv'
        else:
            GENE_EXPRESSION_FILE = '/drp-data/DRP2022_preprocessed/depmap/ccle_log2tpm.csv'
        
        c = {
            'GENE_EXPRESSION_FILE': GENE_EXPRESSION_FILE,
            'LABEL_FILE': '/drp-data/DRP2022_preprocessed/drug_response/prot_ctrp_ccle_tuple_labels_folds.csv',
            'DRUG_DESCRIPTOR_FILE': '/drp-data/DRP2022_preprocessed/drug_features/ctrp_drug_descriptors.csv',
            'MORGAN_FP_FILE': '/drp-data/DRP2022_preprocessed/drug_features/ctrp_morgan.csv'
        }
        return c