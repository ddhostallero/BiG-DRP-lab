import random
import os
import numpy as np
import pandas as pd
import torch
from scipy.stats import spearmanr, pearsonr
# import utils.constants as c
import dgl
import json

_SEED = 0

def reset_seed(seed=None):
    if seed is not None:
        global _SEED
        _SEED = seed

    torch.manual_seed(_SEED)
    np.random.seed(_SEED)
    random.seed(_SEED)
    dgl.seed(_SEED)
    dgl.random.seed(_SEED)

def create_fold_mask(tuples, label_matrix=None):
    x = tuples.pivot(index='cell_line', columns='drug', values='fold')

    if label_matrix is not None:
        x = x.loc[label_matrix.index, label_matrix.columns]

    return x

def load_hyperparams(hyp_dir):
    with open(hyp_dir) as f:
            hyperparams = f.read()
    return json.loads(hyperparams)
    
def save_flags(FLAGS):

    filename = "%s/results/%s/%s_flags.cfg"%(FLAGS.outroot, FLAGS.folder, FLAGS.mode)

    with open(filename,'w') as f:
        for arg in FLAGS.PUBLIC_ATTR:
            f.write('--%s=%s\n'%(arg, getattr(FLAGS, arg)))

        for key, value in FLAGS._c.items():
            f.write("\n%s=%s"%(key, value))
        
def mkdir(directory):
    directories = directory.split("/")   

    folder = ""
    for d in directories:
        folder += d + '/'
        if not os.path.exists(folder):
            print('creating folder: %s'%folder)
            os.mkdir(folder)


def reindex_tuples(tuples, drugs, cells, start_index=0):
    """
    Transforms strings in the drug and cell line columns to numerical indices
    
    tuples: dataframe with columns: cell_line_name, drug_col, drug_row
    drugs: list of drugs
    cells: list of cell line names
    """
    tuples = tuples.copy()
    for i, drug in enumerate(drugs):
        tuples = tuples.replace(drug, i)
    for i, cell in enumerate(cells):
        tuples = tuples.replace(cell, i+start_index)

    return tuples

def reindex_tuples_v2(tuples, drugs, cells, start_index=0, c_map=None):
    """
    Transforms strings in the drug and cell line columns to numerical indices
    
    tuples: dataframe with columns: cell_line_name, drug_col, drug_row
    drugs: list of drugs
    cells: list of cell line names
    start_index: start of indexing for cell lines
    c_map: initial indexing
    """
    tuples = tuples.copy()

    d_map = pd.Series(np.arange(len(drugs)), index=drugs) 
    tuples['drug_name'] = tuples['drug'].values  
    tuples['drug'] = tuples['drug'].replace(d_map)
    
    if c_map is not None:
        no_map = [i for i in cells if i not in c_map.index] # ccls that don't have exisiting index
        start_index = c_map.max()+1
        for i, ccl in enumerate(no_map):
            c_map[ccl] = i + start_index
    else:
        c_map = pd.Series(np.arange(start_index, start_index+len(cells)), index=cells)    
    
    tuples['ccl_name'] = tuples['cell_line'].values  
    tuples['cell_line'] = tuples['cell_line'].replace(c_map)

    return tuples, c_map, d_map

def moving_average(a, n=3):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


def calculate_metrics(x, y):
    y = y.loc[x.index, x.columns]

    x = x.values.reshape(-1)
    y = y.values.reshape(-1)

    x = x[~np.isnan(x)]
    y = y[~np.isnan(y)]

    print(len(x), "length of x")

    return (
        ((x - y)**2).mean(),
        np.sqrt((x - y)**2).mean(),
        pearsonr(x, y)[0],
        spearmanr(x, y)[0])

def rowwise_pcc(A,B):
    # from here: https://stackoverflow.com/questions/41700840/correlation-of-2-time-dependent-multidimensional-signals-signal-vectors/41703623#41703623
    # Rowwise mean of input arrays & subtract from input arrays themeselves
    A_mA = A - A.mean(1)[:,None]
    B_mB = B - B.mean(1)[:,None]

    # Sum of squares across rows
    ssA = (A_mA**2).sum(1);
    ssB = (B_mB**2).sum(1);

    # Finally get corr coeff
    return np.einsum('ij,ij->i',A_mA,B_mB)/np.sqrt(ssA*ssB)

def rowwise_scc(A, B):
    out = np.zeros(A.shape[0])
    for i in range(len(A)):
        out[i] = spearmanr(A[i], B[i])[0]
    return out